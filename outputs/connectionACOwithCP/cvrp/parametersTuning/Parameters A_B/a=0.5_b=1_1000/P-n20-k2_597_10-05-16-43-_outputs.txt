
Sol ID : 1
Sol costs: 329.3925570717078
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 192.3192539263862 || Demand  = 159.0 || 
Route 2 || Costs = 137.0733031453216 || Demand  = 151.0 || 



best way: [0, 3, 13, 2, 10, 12, 8, 18, 17, 7, 5, 9, 20, 6, 16, 14, 19, 4, 15, 11, 1, 21]
best way distance: 329.39255707170776
--------------------------------------------

Sol ID : 2
Sol costs: 309.7230393705268
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 123.58805867180536 || Demand  = 150.0 || 
Route 2 || Costs = 186.13498069872145 || Demand  = 160.0 || 



best way: [0, 10, 7, 16, 5, 9, 13, 2, 6, 19, 20, 1, 14, 18, 3, 8, 17, 12, 15, 4, 11, 21]
best way distance: 309.72303937052675
--------------------------------------------

Sol ID : 3
Sol costs: 316.02396424236247
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 153.72603472030437 || Demand  = 155.0 || 
Route 2 || Costs = 162.29792952205807 || Demand  = 155.0 || 



best way: [0, 1, 11, 3, 18, 8, 9, 16, 14, 5, 7, 20, 10, 15, 17, 13, 2, 12, 4, 6, 19, 21]
best way distance: 316.0239642423624
--------------------------------------------

Sol ID : 4
Sol costs: 319.3104969810726
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 159.4270299598358 || Demand  = 158.0 || 
Route 2 || Costs = 159.8834670212368 || Demand  = 152.0 || 



best way: [0, 19, 2, 18, 17, 15, 11, 4, 12, 3, 1, 20, 7, 14, 5, 9, 13, 8, 10, 6, 16, 21]
best way distance: 319.3104969810727
--------------------------------------------

Sol ID : 5
Sol costs: 335.313973409239
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 188.43934035282538 || Demand  = 156.0 || 
Route 2 || Costs = 146.87463305641364 || Demand  = 154.0 || 



best way: [0, 19, 14, 16, 5, 7, 13, 1, 6, 9, 18, 20, 3, 17, 2, 8, 12, 15, 11, 4, 10, 21]
best way distance: 335.313973409239
--------------------------------------------
