
Sol ID : 1
Sol costs: 241.88297767790186
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 137.79047696791594 || Demand  = 155.0 || 
Route 2 || Costs = 104.09250070998591 || Demand  = 155.0 || 



best way: [0, 1, 10, 4, 11, 15, 12, 3, 8, 17, 18, 13, 20, 6, 19, 5, 7, 16, 14, 9, 2, 21]
best way distance: 241.8829776779018
--------------------------------------------

Sol ID : 2
Sol costs: 262.0481935076069
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 138.42181817382325 || Demand  = 160.0 || 
Route 2 || Costs = 123.6263753337837 || Demand  = 150.0 || 



best way: [0, 1, 10, 4, 11, 15, 12, 3, 2, 7, 13, 5, 20, 6, 19, 14, 16, 9, 8, 17, 18, 21]
best way distance: 262.0481935076069
--------------------------------------------

Sol ID : 3
Sol costs: 245.53785842113177
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 121.47167274403294 || Demand  = 158.0 || 
Route 2 || Costs = 124.06618567709884 || Demand  = 152.0 || 



best way: [0, 1, 10, 6, 19, 7, 5, 14, 16, 9, 13, 20, 4, 11, 15, 12, 17, 18, 3, 8, 2, 21]
best way distance: 245.5378584211318
--------------------------------------------

Sol ID : 4
Sol costs: 243.59254155632289
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 119.78324464313353 || Demand  = 158.0 || 
Route 2 || Costs = 123.80929691318936 || Demand  = 152.0 || 



best way: [0, 6, 19, 5, 14, 16, 7, 9, 13, 1, 10, 20, 4, 11, 15, 12, 8, 17, 18, 3, 2, 21]
best way distance: 243.5925415563229
--------------------------------------------

Sol ID : 5
Sol costs: 244.28539970858992
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 124.26633142934935 || Demand  = 160.0 || 
Route 2 || Costs = 120.01906827924056 || Demand  = 150.0 || 



best way: [0, 10, 4, 11, 15, 12, 3, 18, 17, 8, 2, 20, 19, 6, 14, 16, 5, 7, 9, 13, 1, 21]
best way distance: 244.28539970858992
--------------------------------------------
