
Sol ID : 1
Sol costs: 319.4598313792775
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 132.95070579594451 || Demand  = 158.0 || 
Route 2 || Costs = 186.509125583333 || Demand  = 152.0 || 



--------------------------------------------

Sol ID : 2
Sol costs: 293.6562378505727
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 104.38649541438798 || Demand  = 155.0 || 
Route 2 || Costs = 189.26974243618474 || Demand  = 155.0 || 



--------------------------------------------

Sol ID : 3
Sol costs: 340.1549596489589
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 144.59904661876067 || Demand  = 151.0 || 
Route 2 || Costs = 195.55591303019824 || Demand  = 159.0 || 



--------------------------------------------

Sol ID : 4
Sol costs: 294.3654662050802
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 136.23641885957622 || Demand  = 156.0 || 
Route 2 || Costs = 158.12904734550398 || Demand  = 154.0 || 



--------------------------------------------

Sol ID : 5
Sol costs: 341.5327597078251
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 133.84054104004238 || Demand  = 150.0 || 
Route 2 || Costs = 207.6922186677827 || Demand  = 160.0 || 



--------------------------------------------

Sol ID : 6
Sol costs: 323.0311315495924
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 144.43396751292525 || Demand  = 150.0 || 
Route 2 || Costs = 178.59716403666715 || Demand  = 160.0 || 



--------------------------------------------

Sol ID : 7
Sol costs: 282.0137108838344
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 141.07512456724325 || Demand  = 154.0 || 
Route 2 || Costs = 140.93858631659114 || Demand  = 156.0 || 



--------------------------------------------

Sol ID : 8
Sol costs: 298.4831281772084
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 183.17006475643265 || Demand  = 155.0 || 
Route 2 || Costs = 115.31306342077573 || Demand  = 155.0 || 



--------------------------------------------

Sol ID : 9
Sol costs: 290.2357543865861
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 159.53551179580654 || Demand  = 158.0 || 
Route 2 || Costs = 130.70024259077957 || Demand  = 152.0 || 



--------------------------------------------

Sol ID : 10
Sol costs: 337.2833389917914
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 163.79985501984112 || Demand  = 158.0 || 
Route 2 || Costs = 173.48348397195028 || Demand  = 152.0 || 



--------------------------------------------
