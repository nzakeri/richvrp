
Sol ID : 1
Sol costs: 227.68693122143054
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 137.79047696791594 || Demand  = 155.0 || 
Route 2 || Costs = 89.8964542535146 || Demand  = 155.0 || 



best way: [0, 1, 10, 4, 11, 15, 12, 3, 8, 17, 18, 13, 20, 6, 19, 5, 14, 16, 9, 7, 2, 21]
best way distance: 227.6869312214305
--------------------------------------------

Sol ID : 2
Sol costs: 226.39646536087906
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 137.79047696791594 || Demand  = 155.0 || 
Route 2 || Costs = 88.60598839296314 || Demand  = 155.0 || 



best way: [0, 1, 10, 4, 11, 15, 12, 3, 8, 17, 18, 13, 20, 19, 5, 14, 16, 9, 7, 2, 6, 21]
best way distance: 226.39646536087903
--------------------------------------------

Sol ID : 3
Sol costs: 222.71964242778535
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 109.32229347317013 || Demand  = 158.0 || 
Route 2 || Costs = 113.39734895461521 || Demand  = 152.0 || 



best way: [0, 6, 7, 19, 5, 14, 16, 9, 13, 10, 1, 20, 4, 11, 15, 12, 3, 18, 17, 8, 2, 21]
best way distance: 222.71964242778535
--------------------------------------------

Sol ID : 4
Sol costs: 225.2454578449963
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 106.54089991909689 || Demand  = 158.0 || 
Route 2 || Costs = 118.70455792589942 || Demand  = 152.0 || 



best way: [0, 6, 19, 5, 14, 16, 9, 13, 7, 10, 1, 20, 11, 4, 15, 12, 3, 18, 17, 8, 2, 21]
best way distance: 225.24545784499634
--------------------------------------------

Sol ID : 5
Sol costs: 225.01614157573894
# of routes in sol: 2


List of routes (cost and nodes): 

Route 1 || Costs = 97.9840643939324 || Demand  = 154.0 || 
Route 2 || Costs = 127.03207718180654 || Demand  = 156.0 || 



best way: [0, 6, 19, 5, 14, 16, 9, 13, 2, 10, 20, 1, 4, 11, 15, 12, 3, 17, 18, 8, 7, 21]
best way distance: 225.01614157573897
--------------------------------------------
