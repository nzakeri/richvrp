
Sol ID : 1
Sol costs: 1387.5505076677844
# of routes in sol: 9


List of routes (cost and nodes): 

Route 1 || Costs = 167.8911707194854 || Demand  = 139.0 || 
Route 2 || Costs = 284.8284968559899 || Demand  = 140.0 || 
Route 3 || Costs = 242.51283656029858 || Demand  = 140.0 || 
Route 4 || Costs = 155.51699614900357 || Demand  = 77.0 || 
Route 5 || Costs = 125.2831573516537 || Demand  = 80.0 || 
Route 6 || Costs = 157.48131568337948 || Demand  = 75.0 || 
Route 7 || Costs = 155.0235695710212 || Demand  = 79.0 || 
Route 8 || Costs = 46.17358552246078 || Demand  = 28.0 || 
Route 9 || Costs = 52.839379254491625 || Demand  = 19.0 || 



best way: [0, 17, 32, 16, 2, 47, 4, 5, 37, 15, 46, 51, 12, 3, 50, 49, 38, 43, 29, 48, 26, 31, 52, 44, 45, 35, 28, 11, 1, 36, 20, 8, 53, 27, 10, 34, 21, 13, 54, 6, 14, 41, 19, 22, 55, 18, 39, 42, 40, 56, 23, 24, 30, 33, 9, 57, 25, 58, 7, 59]
best way distance: 1387.550507667784
--------------------------------------------
