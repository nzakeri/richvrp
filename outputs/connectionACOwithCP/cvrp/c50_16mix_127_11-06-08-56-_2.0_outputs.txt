
Sol ID : 1
Sol costs: 907.5580168495035
# of routes in sol: 9


List of routes (cost and nodes): 

Route 1 || Costs = 111.847792471605 || Demand  = 139.0 || 
Route 2 || Costs = 156.191140787091 || Demand  = 137.0 || 
Route 3 || Costs = 159.41234413723365 || Demand  = 137.0 || 
Route 4 || Costs = 52.5589657417923 || Demand  = 64.0 || 
Route 5 || Costs = 103.8952848426913 || Demand  = 73.0 || 
Route 6 || Costs = 74.84749127509585 || Demand  = 79.0 || 
Route 7 || Costs = 120.54063779539969 || Demand  = 74.0 || 
Route 8 || Costs = 42.04759208325728 || Demand  = 30.0 || 
Route 9 || Costs = 86.21676771533741 || Demand  = 37.0 || 



best way: [0, 46, 12, 37, 17, 4, 47, 27, 32, 22, 1, 48, 51, 42, 44, 15, 45, 38, 49, 34, 9, 50, 21, 52, 19, 41, 18, 13, 24, 23, 31, 53, 14, 25, 6, 54, 20, 35, 36, 3, 29, 55, 11, 16, 30, 10, 5, 56, 8, 26, 7, 43, 28, 57, 2, 58, 33, 39, 59, 0]
best way distance: 1.0000090755801685E8
--------------------------------------------
