package converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.uoc.rgcws.Node;

public class VRPPD2 {

	//final static String nameFile="dataFile.txt";//benchmarks files";
	//final static String benchFolder = "Benchmarks";

	final static String inFolder = "\\inputs\\pickup_delivery";
	final static String proFolder = "\\Prolog\\pickup_delivery";

	final static String sufixFileInput = "_input_nodes.txt";
	final static String sufixFileVehicles = "_input_vehicles.txt";
	final static String sufixFileProlog = ".ecl";

	private static String RVRPFrameWorkDir = "K:\\RichVRP\\SR-GCWS-CS_CP_v2";
	

	public static void main(String[] args) throws IOException {

		File dir = new File("D:\\CPlib_diffVer\\Benchmarks\\PDPTW-BCP-DataSet1");

		String prologPath = RVRPFrameWorkDir + proFolder ;

		String javaInPath = RVRPFrameWorkDir + inFolder;

		for (File file : dir.listFiles()) {
			convertToReadableFiles(file, javaInPath, prologPath);
		}
	}

	static void convertToReadableFiles (File theFile, String javaIndir, String proDir) throws FileNotFoundException{

		ArrayList<Integer> demand = new ArrayList<Integer>();
		ArrayList<Float> corX = new ArrayList<Float>();
		ArrayList<Float> corY = new ArrayList<Float>();
		
		ArrayList<Integer> custID = new ArrayList<Integer>();

		ArrayList<Integer> pickupIndex = new ArrayList<Integer>();
		ArrayList<Integer> deliveryIndex = new ArrayList<Integer>();
		ArrayList<Integer> serviceTime = new ArrayList<Integer>();
		ArrayList<Integer> latestTW1 = new ArrayList<Integer>();
		ArrayList<Integer> earliestTW1 = new ArrayList<Integer>();
		
		ArrayList<Integer> numberOfpickups = new ArrayList<Integer>();
		
		int requestNumber = 0;
		int capacity = 0;

		Scanner s = new Scanner(theFile);
		//int numCustomer = 0;
		//int numCustomer = 100;

		while (s.hasNextLine()){
			String line = s.nextLine();
			String[] parts = line.trim().split("\\s+");
			if (parts.length <= 5){
				requestNumber = Integer.parseInt(parts[1]);
				System.out.println(requestNumber);
				capacity = Integer.parseInt(parts[3]);
				System.out.println(capacity);
			}




			if (parts.length > 5){

				if (!line.contains("//") && !line.isEmpty()){

					//for (String d : parts) System.out.println(d);
					
					custID.add(Integer.parseInt(parts[0]));	
					
					corX.add(Float.parseFloat(parts[1]));
					corY.add(Float.parseFloat(parts[2]));
					demand.add(Integer.parseInt(parts[4]));					
					
					earliestTW1.add(Integer.parseInt(parts[5]));

					latestTW1.add(Integer.parseInt(parts[6]));

					serviceTime.add(Integer.parseInt(parts[3]));

				//	pickupIndex.add(Integer.parseInt(parts[7]));
				//	deliveryIndex.add(Integer.parseInt(parts[8]));
				}
			}
		}
		s.close();

		String outputsFilePath = javaIndir + File.separator + theFile.getName().replace(".txt", sufixFileInput);

		String vehFilePath = javaIndir + File.separator + theFile.getName().replace(".txt", sufixFileVehicles);

		PrintWriter writer = new PrintWriter(outputsFilePath);
		int h = corX.size() - 2;
		for (int i = 0; i < h ; i++){
			writer.println( corX.get(i) + "\t" + corY.get(i) + "\t" + demand.get(i));
		}
		writer.print( corX.get(h) + "\t" + corY.get(h) + 
				"\t" + demand.get(h));

		writer.close();

		PrintWriter vehWriter = new PrintWriter(vehFilePath);


		int vehNum = 25;
		for (int i = 0; i < vehNum - 1   ; i++) {

			vehWriter.println(capacity);


		}

		vehWriter.print(capacity);


		vehWriter.close();
		
		
		
		/*for (int i = 1;  i <= h; i++){
			
			if (demand.get(i) > 0){			
				numberOfpickups.add(i);			
				
			}			
		}
		*/
		


		String proOutput = proDir + File.separator + theFile.getName().replace(".txt", ".ecl");
		PrintWriter proWriter = new PrintWriter(proOutput);
		//writer.print("maxDistLength(" + demand.get(0) + ").\n\n" );

		proWriter.print("customers([");
		for (int i = 1; i <= h; i++) {

			proWriter.print(demand.get(i));
			if (i != h){
				proWriter.println(",");
			}else{
				proWriter.print("]).");
			}
		}

		//int counter = corX.size() + numVehicles-1;

		proWriter.print("\n\ncoords([");

		for (int i = 1;  i < h + vehNum; i++){

			if (i <= h){
				proWriter.println("[" + corX.get(i) + "," + corY.get(i) + "],");

			}else{

				proWriter.println("[" + corX.get(0) + "," + corY.get(0) + "],");
			}


		}
		proWriter.print("[" + corX.get(0) + "," + corY.get(0) + "]]).");

	

		proWriter.print("\n\nvehicles([");

		
			for (int i = 0; i < vehNum; i++) {
				

				proWriter.print(capacity);
				if(i < vehNum-1){
					proWriter.println(",");
				} else{
					proWriter.print("]).");
				}

			}
			
			proWriter.print("\n\nservice_times([");
			for (int i = 1; i <= h; i++) {

				proWriter.print(serviceTime.get(i));
				if (i != h){
					proWriter.println(",");
				}else{
					proWriter.print("]).");
				}
			}
		

			proWriter.print("\n\ntime_windows([");

			for (int i = 1;  i < h + vehNum; i++){

				if (i <= h){
					proWriter.println("[" + earliestTW1.get(i) + "," + latestTW1.get(i) + "],");

				}else{

					proWriter.println("[" + earliestTW1.get(0) + "," + latestTW1.get(0) + "],");
				}


			}
			proWriter.print("[" + earliestTW1.get(0) + "," + latestTW1.get(0) + "]]).");
			
			
			int k = numberOfpickups.size();
			
			proWriter.print("\n\npairs([");
			
			for (int i = 1;  i < requestNumber ; i++){		
														
					proWriter.println("[" + i + "," + (i + requestNumber) + "],");
				}
				
			proWriter.print("[" + requestNumber + "," +  (2*requestNumber) + "]]).");
				
			
			

		proWriter.close();
	}
}




