package converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.uoc.rgcws.Node;

public class TaillardConverter {

	//final static String nameFile="dataFile.txt";//benchmarks files";
	//final static String benchFolder = "Benchmarks";

	final static String inFolder = "\\inputs";
	final static String proFolder = "\\Prolog";

	final static String sufixFileInput = "_input_nodes.txt";
	final static String sufixFileVehicles = "_input_vehicles.txt";
	final static String sufixFileProlog = ".ecl";

	private static String RVRPFrameWorkDir = "K:\\RichVRP\\SR-GCWS-CS_CP_v2";

	public static void main(String[] args) throws IOException {

		File dir = new File("D:\\CPlib_diffVer\\Benchmarks\\Christofides et al. and Taillard");

		String prologPath = RVRPFrameWorkDir + proFolder ;

		String javaInPath = RVRPFrameWorkDir + inFolder;

		for (File file : dir.listFiles()) {
			convertToReadableFiles(file, javaInPath, prologPath);
		}
	}

	static void convertToReadableFiles (File theFile, String javaIndir, String proDir) throws FileNotFoundException{

		ArrayList<Integer> demand = new ArrayList<Integer>();
		ArrayList<Integer> corX = new ArrayList<Integer>();
		ArrayList<Integer> corY = new ArrayList<Integer>();

		ArrayList<Integer> numberVehAvailable = new ArrayList<Integer>();
		ArrayList<Float> variableCost = new ArrayList<Float>();
		ArrayList<Float> fixedCost = new ArrayList<Float>();
		ArrayList<Integer> volume = new ArrayList<Integer>();

		Scanner s = new Scanner(theFile);
		int numCustomer = s.nextInt();
		//int numCustomer = 100;
		int k = 0;
		while (s.hasNextLine()){
			String line = s.nextLine();
			String[] parts = line.trim().split("\\s+");
			if (parts.length > 3){

				if (!line.contains("//") && !line.isEmpty()){
					if (k <= numCustomer){

						for (String d : parts) System.out.println(d);
						corX.add(Integer.parseInt(parts[1]));
						corY.add(Integer.parseInt(parts[2]));
						demand.add(Integer.parseInt(parts[3]));
						k ++;
					}else{

						volume.add(Integer.parseInt(parts[0]));

						fixedCost.add(Float.parseFloat(parts[1]));

						variableCost.add(Float.parseFloat(parts[2]));

						numberVehAvailable.add(Integer.parseInt(parts[3]));
					}

				}
			}
		}
		s.close();

		String outputsFilePath = javaIndir + File.separator + theFile.getName().replace(".txt", sufixFileInput);

		String vehFilePath = javaIndir + File.separator + theFile.getName().replace(".txt", sufixFileVehicles);

		PrintWriter writer = new PrintWriter(outputsFilePath);
		for (int i =0; i <= numCustomer; i++){
			writer.println( corX.get(i) + "\t" + corY.get(i) + "\t" + demand.get(i));
		}

		writer.close();

		PrintWriter vehWriter = new PrintWriter(vehFilePath);
		int countNumVeh = 0;
		for (int j = volume.size() -1; j>= 0 ;j--){
			for (int i = 0; i < numberVehAvailable.get(j); i++) {

				vehWriter.println(volume.get(j));

				countNumVeh ++;
			}


		}
		vehWriter.close();


		String proOutput = proDir + File.separator + theFile.getName().replace(".txt", ".ecl");
		PrintWriter proWriter = new PrintWriter(proOutput);
		//writer.print("maxDistLength(" + demand.get(0) + ").\n\n" );

		proWriter.print("customers([");
		for (int i = 1; i <= numCustomer; i++) {

			proWriter.print(demand.get(i));
			if (i != numCustomer){
				proWriter.println(",");
			}else{
				proWriter.print("]).");
			}
		}

		//int counter = corX.size() + numVehicles-1;

		proWriter.print("\n\ncoords([");

		for (int i = 1;  i < numCustomer + countNumVeh; i++){

			if (i <= numCustomer){
				proWriter.println("[" + corX.get(i) + "," + corY.get(i) + "],");

			}else{

				proWriter.println("[" + corX.get(0) + "," + corY.get(0) + "],");
			}


		}
		proWriter.print("[" + corX.get(0) + "," + corY.get(0) + "]]).");

		int count = 0 ;

		proWriter.print("\n\nvehicles([");

		for (int j =volume.size() -1; j >= 0 ;j--){
			for (int i = 0; i < numberVehAvailable.get(j); i++) {
				count ++;

				proWriter.print(volume.get(j));
				if(count < countNumVeh){
					proWriter.println(",");
				} else{
					proWriter.print("]).");
				}

			}
		}


		proWriter.close();
	}
}




