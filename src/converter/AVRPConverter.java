package converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

import org.uoc.rgcws.Node;

public class AVRPConverter {

	//final static String nameFile="dataFile.txt";//benchmarks files";
	//final static String benchFolder = "Benchmarks";

	final static String inFolder = "\\inputs";
	final static String proFolder = "\\prolog";

	final static String sufixFileInput = "_input_nodes.txt";
	final static String sufixFileVehicles = "_input_vehicles.txt";
	final static String sufixFileProlog = ".ecl";

	private static String RVRPFrameWorkDir = "D:\\AVRP";

	public static void main(String[] args) throws IOException {

		File location = new File("D:\\CPlib_diffVer\\Benchmarks\\Instances_CVRP\\cvrp\\localizaciones2");

		String prologPath = RVRPFrameWorkDir + proFolder ;

		String javaInPath = RVRPFrameWorkDir + inFolder;

		for (File file_loc : location.listFiles()) {
			convertToReadableFiles(file_loc, javaInPath, prologPath);
		}
	}

	static void convertToReadableFiles (File theFile, String javaIndir, String proDir) throws FileNotFoundException{

		ArrayList<Integer> demand = new ArrayList<Integer>();
		ArrayList<Float> corX = new ArrayList<Float>();
		ArrayList<Float> corY = new ArrayList<Float>();
		/*ArrayList<Integer> numberVehAvailable = new ArrayList<Integer>();
		ArrayList<Float> variableCost = new ArrayList<Float>();
		ArrayList<Float> fixedCost = new ArrayList<Float>();
		ArrayList<Integer> volume = new ArrayList<Integer>();*/

		Scanner s1 = new Scanner(theFile);
		int k = 0;
		while (s1.hasNextLine()){
			String line = s1.nextLine();
			String[] parts = line.trim().split("\\s+");
			for (String d : parts) System.out.println(d);
			corX.add(Float.parseFloat(parts[0].replace(",",".")));
			corY.add(Float.parseFloat(parts[1].replace(",",".")));
			k++;

		}
		s1.close();
		File demcap = new File("D:\\CPlib_diffVer\\Benchmarks\\Instances_CVRP\\cvrp\\demcap2");						
		File distMatrix = new File("D:\\CPlib_diffVer\\Benchmarks\\Instances_CVRP\\cvrp\\matrices2");

		for (File file_dem : demcap.listFiles()) {

			int j = theFile.getName().length()-11;
			String endOfFileName = theFile.getName().substring(j);

			if (file_dem.getName().endsWith(endOfFileName)){


				Scanner s2 = new Scanner(file_dem);

				String line = s2.nextLine();
				String[] parts = line.trim().split("\\s+");
				for (String d : parts){
					System.out.println(d);

					demand.add(Integer.parseInt(d));
				}
				int cap = s2.nextInt();

				s2.close();



				//ArrayList<String> rowMatrixOfDist = new ArrayList<String>();
				int count = 0;

				//ArrayList<ArrayList<ArrayList<String>>> allMatrices = new ArrayList<ArrayList<ArrayList<String>>>();

				for (File file_dist : distMatrix.listFiles()) {
					ArrayList<ArrayList<String>> matrixOfDis = new ArrayList<ArrayList<String>>();

					int end = theFile.getName().length()-8;

					String endOflocFile = theFile.getName().substring(end);

					String startOflocFile = theFile.getName().substring(0, 4);
					//String charOfType = Character.toString(theFile.getName().charAt(0));

					if (file_dist.getName().endsWith(endOflocFile) && file_dist.getName().startsWith(startOflocFile))
						//&& file_dist.getName().startsWith(charOfType))
					{
						Scanner s3 = new Scanner(file_dist);
						String firstLine = s3.nextLine().replace(",", ".");
						ArrayList<String> arrFirstLine = new ArrayList<>();

						for( String s : firstLine.trim().split("\\s+")){
							arrFirstLine.add(s);	
						}

						while (s3.hasNextLine() && count < k){
							ArrayList<String> rowMatrixOfDist = new ArrayList<String>();
							String line3 = s3.nextLine().replace(",", ".");;
							ArrayList<String> disPart = new ArrayList<>();
							for( String s : line3.trim().split("\\s+")){
								disPart.add(s);	
							}

							for (int i = 1; i < disPart.size()+10; i++){
								if (i < disPart.size()){
									rowMatrixOfDist.add( disPart.get(i));
								}else{
									rowMatrixOfDist.add( disPart.get(0));
								}

							}
							//Collections.reverse(rowMatrixOfDist);
							matrixOfDis.add(rowMatrixOfDist);
							count++;

						}
						for (int vehNum = 0; vehNum < 10; vehNum++){
							ArrayList<String> rowMatrixOfDist = new ArrayList<String>();

							for (int i = 1; i < arrFirstLine.size()+10; i++){
								if (i < arrFirstLine.size()){
									rowMatrixOfDist.add( arrFirstLine.get(i));	
								}else{
									rowMatrixOfDist.add( arrFirstLine.get(0));
								}
							}

							matrixOfDis.add(rowMatrixOfDist);
						}

						//allMatrices.add(matrixOfDis);

						//s3.close();
				//	}


					int h = file_dem.getName().length()-11;
					String beginFileName = file_dem.getName().substring(0, h);
					String fileName = beginFileName.concat(file_dist.getName());
					System.out.println(fileName);


					String outputsFilePath = javaIndir + File.separator + fileName.replace(".txt", sufixFileInput);

					String vehFilePath = javaIndir + File.separator + fileName.replace(".txt", sufixFileVehicles);

					PrintWriter writer = new PrintWriter(outputsFilePath);
					for (int i =0; i < k; i++){
						if (i != k-1){
							writer.println( corX.get(i) + "\t" + corY.get(i) + "\t" + demand.get(i));
						}else{
							writer.print( corX.get(i) + "\t" + corY.get(i) + "\t" + demand.get(i));
						}
					}

					writer.close();

					PrintWriter vehWriter = new PrintWriter(vehFilePath);
					//int countNumVeh = 0;

					for (int i = 0; i < 9; i++) {

						vehWriter.println(cap);


					}
					vehWriter.print(cap);

					vehWriter.close();






					String proOutput = proDir + File.separator + fileName.replace(".txt", ".ecl");
					PrintWriter proWriter = new PrintWriter(proOutput);
					//writer.print("maxDistLength(" + demand.get(0) + ").\n\n" );

					proWriter.print("customers([");
					for (int i = 1; i < k; i++) {

						proWriter.print(demand.get(i));
						if (i != k-1){
							proWriter.println(",");
						}else{
							proWriter.print("]).");
						}
					}

					//int counter = corX.size() + numVehicles-1;

					proWriter.print("\n\ncoords([");

					for (int i = 1;  i < k + 9; i++){

						if (i < k){
							proWriter.println("[" + corX.get(i) + "," + corY.get(i) + "],");

						}else{

							proWriter.println("[" + corX.get(0) + "," + corY.get(0) + "],");
						}


					}
					proWriter.print("[" + corX.get(0) + "," + corY.get(0) + "]]).");

					//int count = 0 ;

					proWriter.print("\n\nvehicles([");


					for (int i = 0; i < 9; i++) {

						proWriter.print(cap);

						proWriter.println(",");
					}

					proWriter.print(cap+"]).");

					/*
				for (j = 0; j < allMatrices.size();j++){
					ArrayList<ArrayList<String>> matrixOfDis = allMatrices.get(j);*/
					proWriter.print("\n\ngetDistanceMatrix([");

					for (int i = 0; i < matrixOfDis.size(); i++) {
						 ArrayList<Integer> newList = new ArrayList<Integer>(matrixOfDis.get(i).size());
						for (String myInt : matrixOfDis.get(i)) 
			            { 
			             
						newList.add(Math.round(Float.valueOf(myInt))); 
			            }
												

						proWriter.print(newList);
						if (i < matrixOfDis.size()-1){
							proWriter.println(",");
						}else{
							proWriter.print("]).");
						}
					}

					

					proWriter.close();

					s3.close();
					}
				}
			}
		}
	}
}



