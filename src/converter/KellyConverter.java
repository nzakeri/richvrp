package converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class KellyConverter {

	//final static String nameFile="dataFile.txt";//benchmarks files";
	//final static String benchFolder = "Benchmarks";

	final static String inFolder = "\\inputs";
	final static String proFolder = "\\Prolog";

	final static String sufixFileInput = "_input_nodes.txt";
	final static String sufixFileNodes = ".ecl";
	
	private static String RVRPFrameWorkDir = "K:\\RichVRP\\SR-GCWS-CS_CP_v2";
	
	public static void main(String[] args) throws IOException {

		File dir = new File("K:\\RichVRP\\Benchmarks\\kelly\\kelly");

		String prologPath = RVRPFrameWorkDir + proFolder ;

		for (File file : dir.listFiles()) {
			convertToProlog(file,prologPath );
		}
	}


/*String outputsFilePath = outputFolder + File.separator + 
aTest.getInstanceName() + "_" + aTest.getSeed() + sufixFileOutput;
output.sendToFile(outputsFilePath);*/
	

	static void convertToProlog (File theFile, String prologFiledir) throws FileNotFoundException{
		
		ArrayList<Integer> demand = new ArrayList<Integer>();
		ArrayList<Float> corX = new ArrayList<Float>();
		ArrayList<Float> corY = new ArrayList<Float>();
		
		int numVehicles = 30;
		Scanner s = new Scanner(theFile);

		while (s.hasNextLine()){
			String line = s.nextLine();
			String[] parts = line.trim().split("\\s+");
			for (String d : parts) System.out.println(d);
			corX.add(Float.parseFloat(parts[0]));
			corY.add(Float.parseFloat(parts[1]));
			if (parts.length < 3){
				demand.add(0);
			} else{
				demand.add(Integer.parseInt(parts[2]));
			}

		}
			

//		for (Float x : corX) System.out.println("x: " + x + ",");
//		for (Float y : corY) System.out.println("y: " + y);
		//for (String d : demand) System.out.println("demand: " + d);

//		System.out.println(demand.toString());
//		s.close();
		
		
		String outputsFilePath = prologFiledir + File.separator + theFile.getName().replace(".txt", ".ecl");
		PrintWriter writer = new PrintWriter(outputsFilePath);
		writer.print("maxDistLength(" + demand.get(0) + ").\n\n" );
		
		writer.print("customers([");
		for (int i = 0; i < demand.size(); i++) {
			if (i == 0 || i == 1)
				continue;
			writer.print(demand.get(i));
			if (i != demand.size()-1){
				writer.println(",");
			}else{
				writer.println("]).");
			}
		}
		
		int counter = corX.size() + numVehicles-1;
		
		writer.print("\ncoords([");
		
		for (int i = 0;  i < counter-1; i++){
			if (i == 0 || i == 1)
				continue;

			if (i < corX.size()){
				writer.println("[" + corX.get(i) + "," + corY.get(i) + "],");

			}else{
								
					writer.println("[" + corX.get(1) + "," + corY.get(1) + "],");
				}
				

				}
		writer.println("[" + corX.get(1) + "," + corY.get(1) + "]]).");

		

		writer.print("\nvehicles([");
		
		int vehicleCapacity = Math.round(corY.get(0));
		for (int i = 0; i < numVehicles-1; i++) {
			
			writer.print(vehicleCapacity);
			if (i != numVehicles-2){
				writer.println(",");
			}else{
				writer.println("]).");
			}
			
		}
						
		
		writer.close();
	}
}

	
	 



