package converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;



public class VRP_Set_P {

	//final static String nameFile="dataFile.txt";//benchmarks files";
	//final static String benchFolder = "Benchmarks";

	final static String inFolder = "\\inputs\\ACO_cvrp_partialSol\\setP";
	final static String proFolder = "\\Prolog\\ACO_cvrp_partialsol\\setP";

	final static String sufixFileInput = "_input_nodes.txt";
	final static String sufixFileVehicles = "_input_vehicles.txt";
	final static String sufixFileNodes = ".ecl";

	private static String RVRPFrameWorkDir = "K:\\RichVRP\\SR-GCWS-CS_CP_v2";

	public static void main(String[] args) throws IOException {

		File dir = new File("L:\\projects\\VRP-Set-P");



		String prologPath = RVRPFrameWorkDir + proFolder ;

		String javaInPath = RVRPFrameWorkDir + inFolder;

		for (File file : dir.listFiles()) {
			if (file.getName().endsWith((".vrp"))) {
			     			
			convertToReadableFiles(file, javaInPath, prologPath);
			}
		}
	}

	static void convertToReadableFiles (File theFile, String javaIndir, String proDir) throws FileNotFoundException{

		ArrayList<Integer> demand = new ArrayList<Integer>();
		ArrayList<Integer> corX = new ArrayList<Integer>();
		ArrayList<Integer> corY = new ArrayList<Integer>();



		Scanner s = new Scanner(theFile);
		int numVeh = 0;
		int numNodes=0;
		int vehCap=0;
		s.nextLine();

		while (s.hasNextLine()){
			String line = s.nextLine();

			if (line.contains("COMMENT :")){
				line = line.replaceAll(":", ",");

				String[] parts = line.trim().split(",");
				for (String d : parts) System.out.println(d);
				System.out.println(parts[3]);
				numVeh = Integer.parseInt(parts[3].trim());
				line = "";

			}

			if (line.contains("DIMENSION :")){
				line = line.replaceAll(":", ",");

				String[] parts = line.trim().split(",");
				for (String d : parts) System.out.println(d);
				System.out.println(parts[1]);
				numNodes = Integer.parseInt(parts[1].trim());	
				line = "";
			}
			if (line.contains("CAPACITY :")){
				line = line.replaceAll(":", ",");

				String[] parts = line.trim().split(",");
				for (String d : parts) System.out.println(d);
				System.out.println(parts[1]);
				vehCap = Integer.parseInt(parts[1].trim());	
				line = "";
			}

			if (line.contains("TYPE :")){
				line = "";
			}
			if (line.contains("EDGE_WEIGHT_TYPE :")){
				line = "";
			}
			
			String[] parts = line.trim().split("\\s+");
			if (parts.length == 3){

				for (String d : parts) System.out.println(d);
				corX.add(Integer.parseInt(parts[1]));
				corY.add(Integer.parseInt(parts[2]));


			}
			if (parts.length == 2){


				for (String d : parts) System.out.println(d);

				demand.add(Integer.parseInt(parts[1]));
			}




		}
		s.close();

		String outputsFilePath = javaIndir + File.separator + theFile.getName().replace(".vrp", sufixFileInput);

		String vehFilePath = javaIndir + File.separator + theFile.getName().replace(".vrp", sufixFileVehicles);

		PrintWriter writer = new PrintWriter(outputsFilePath);
		for (int i =0; i < numNodes; i++){
			if (i != numNodes-1){
			
			writer.println( corX.get(i) + "\t" + corY.get(i) + "\t" + demand.get(i));
			}else{
				writer.print( corX.get(i) + "\t" + corY.get(i) + "\t" + demand.get(i));
			}
		}

		writer.close();

		PrintWriter vehWriter = new PrintWriter(vehFilePath);

		for (int j = 0; j<numVeh ;j++){

			vehWriter.println(vehCap);


		}
		vehWriter.print(vehCap);

		vehWriter.close();


		String proOutput = proDir + File.separator + theFile.getName().replace(".vrp", ".ecl");
		PrintWriter proWriter = new PrintWriter(proOutput);
		//writer.print("maxDistLength(" + demand.get(0) + ").\n\n" );

		proWriter.print("customers([");
		for (int i = 1; i < numNodes; i++) {

			proWriter.print(demand.get(i));
			if (i != numNodes-1){
				proWriter.println(",");
			}else{
				proWriter.print("]).");
			}
		}

		//int counter = corX.size() + numVehicles-1;

		proWriter.print("\n\ncoords([");

		for (int i = 1;  i < numNodes + numVeh; i++){

			if (i < numNodes){
				proWriter.println("[" + corX.get(i) + "," + corY.get(i) + "],");

			}else{

				proWriter.println("[" + corX.get(0) + "," + corY.get(0) + "],");
			}


		}
		proWriter.print("[" + corX.get(0) + "," + corY.get(0) + "]]).");



		proWriter.print("\n\nvehicles([");

		for (int i = 0; i <= numVeh; i++) {


			proWriter.print(vehCap);
			if(i != numVeh){
				proWriter.println(",");
			} else{
				proWriter.print("]).");
			}

		}

		proWriter.close();
	}
}



