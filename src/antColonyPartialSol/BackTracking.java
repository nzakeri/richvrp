package antColonyPartialSol;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class BackTracking  {

	public AgentPartialSol myAgent;
	int stepToCheck = 5;// 5 for SetP
	HashSet<Integer> myListofDepot = new HashSet<Integer>();
	File file = new File("K:\\RichVRP\\deps\\analyze_results\\partialSol_cvrp\\unfeasibleRes.txt");
	private Random random = new Random(33333);

	public BackTracking (AgentPartialSol agent){
		this.myAgent = agent;
		for(int j = myAgent.instance.inputs.getNodes().length; 
				j<myAgent.instance.matrix.length; j++){

			myListofDepot.add(j);
		}
	}



	public final int [] backTrack(int nextNode, int [] curPath, int curPos) throws Exception {

		curPath[curPos]=nextNode;
		myAgent.visited[nextNode] = true;
		curPos++;
		myAgent.toVisit--;
		boolean hasNotFailedFlag = true;


		ArrayList<ArrayList<Integer>> variablesToCheck = 
				AgentPartialSol.cpCaller.getVariablesArrayList(myAgent.createSucPredEsp(curPath, myAgent.instance.inputs.getNodes().length, myAgent.instance.inputs.getVehs()));
		boolean res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);
		if (res == false){
			//System.out.println(curPath);
			hasNotFailedFlag = false;
			for (Integer x : curPath){
				//		System.out.print(x + " , ");
			}
			int a = curPos -1;
			System.out.println("             No           " + a);

		}

		if (curPos == myAgent.visited.length && hasNotFailedFlag){
			System.out.println("yes");
			/*for (Integer x : curPath){
			System.out.print(x + " , ");
			}*/
			//	System.out.println();
			return curPath;	

		}

		HashSet<Integer> myListOfVisited = new HashSet<Integer>(); 

		while(hasNotFailedFlag){

			int extraNextNode = myAgent.getNextProbableNode(nextNode,myListOfVisited);

			if (extraNextNode == -1){
				hasNotFailedFlag = false;

			}else{
				int[] res2 = backTrack(extraNextNode, curPath, curPos);
				if (res2 != null)
					return res2;
				else
					myListOfVisited.add(extraNextNode);
				curPath [curPos] = 0;
			}
		}

		curPos--;
		//	curPath [curPos] = 0;
		myAgent.toVisit++;
		myAgent.visited[nextNode] = false;
		return null;
	}

	public final int [] backTrackWithTimeLimit(int nextNode, int [] curPath, int curPos, double timePortion) throws Exception {
		long startTime = System.nanoTime();

		curPath[curPos]=nextNode;
		myAgent.visited[nextNode] = true;
		curPos++;
		myAgent.toVisit--;
		boolean hasNotFailedFlag = true;
		int nBranches = myAgent.toVisit;


		if (( nBranches % stepToCheck) == 0 ||  (curPos == myAgent.visited.length )){

			ArrayList<ArrayList<Integer>> variablesToCheck = 
					AgentPartialSol.cpCaller.getVariablesArrayList(myAgent.createSucPredEsp(curPath, myAgent.instance.inputs.getNodes().length, myAgent.instance.inputs.getVehs()));
			boolean res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);
			if (res == false){
				//System.out.println(curPath);
				hasNotFailedFlag = false;
				/*for (Integer x : curPath){
				System.out.print(x + " , ");
				}*/
				int a = curPos -1;
				System.out.println("             No           " + a);



				/*try(PrintWriter writeSol = new PrintWriter(new BufferedWriter
						(new FileWriter(file, true)))) {
					for (Integer x : curPath){
						writeSol.print(x + " , ");
						}
					writeSol.println ();					


				}catch (IOException e) {
					System.err.println(e);
				}*/
			}
		}
		if (curPos == myAgent.visited.length && hasNotFailedFlag){
			System.out.println("yes");
			/*for (Integer x : curPath){
				System.out.print(x + " , ");
			}
			System.out.println();*/
			return curPath;	

		}


		HashSet<Integer> myListOfVisited = new HashSet<Integer>(); 

		while(hasNotFailedFlag){

			int extraNextNode = myAgent.getNextProbableNode(nextNode,myListOfVisited);

			if (extraNextNode == -1){
				hasNotFailedFlag = false;

			}else{
				//int timeDiv = Math.min(nBranches,10);
				int timeDiv = 4;
				//int[] res2 = backTrackWithTimeLimit(extraNextNode, curPath, curPos,timePortion/nBranches);
				int[] res2 = backTrackWithTimeLimit(extraNextNode, curPath, curPos,timePortion/timeDiv);//Exploring 4 branches in each level
				if (res2 != null )
					return res2;
				else if ((System.nanoTime()-startTime) > timePortion)
					hasNotFailedFlag = false;
				myListOfVisited.add(extraNextNode);
				curPath [curPos] = 0;

			}
		}

		curPos--;
		//	curPath [curPos] = 0;
		myAgent.toVisit++;
		myAgent.visited[nextNode] = false;
		return null;
	}

	public final int [] backTrackWithExactTimeLimit(int nextNode, int [] curPath, int curPos, long startTime, long endTime) throws Exception {


		curPath[curPos]=nextNode;
		myAgent.visited[nextNode] = true;
		curPos++;
		myAgent.toVisit--;
		boolean hasNotFailedFlag = true;
		boolean res = true;
		boolean wasChecked = false;

		if (( myAgent.toVisit % stepToCheck) == 0 ||  (curPos == myAgent.visited.length )){
			ArrayList<ArrayList<Integer>> variablesToCheck = 
					AgentPartialSol.cpCaller.getVariablesArrayList(myAgent.createSucPredEsp(curPath, myAgent.instance.inputs.getNodes().length, myAgent.instance.inputs.getVehs()));
			res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);
			wasChecked = true;
		}
		if (res == false){
			//System.out.println(curPath);
			hasNotFailedFlag = false;
			/*for (Integer x : curPath){
				//		System.out.print(x + " , ");
			}
			 */			int a = curPos -1;
			 System.out.println("             No           " + a);
			 /*
			try(PrintWriter writeSol = new PrintWriter(new BufferedWriter
					(new FileWriter(file, true)))) {
				for (Integer x : curPath){
					writeSol.print(x + " , ");
					}
				writeSol.println ();					


			}catch (IOException e) {
				System.err.println(e);
			}*/


		}

		if (curPos == myAgent.visited.length && hasNotFailedFlag){
			System.out.println("yes");
			/*for (Integer x : curPath){
			System.out.print(x + " , ");
			}*/
			//	System.out.println();
			return curPath;	

		}

		//long childTimeSlices = (long) ((endTime-startTime)/Math.min(myAgent.toVisit,1.5));
		long childTimeSlices = (long) ((endTime-startTime)/Math.min(myAgent.toVisit,1.2));


		HashSet<Integer> myListOfVisited = new HashSet<Integer>(); 

		while(hasNotFailedFlag){

			int extraNextNode = myAgent.getNextProbableNode(nextNode,myListOfVisited);

			if (extraNextNode == -1){
				hasNotFailedFlag = false;
			}else{
				int[] res2 = backTrackWithExactTimeLimit(extraNextNode, curPath, curPos,startTime,Math.min(startTime+childTimeSlices,endTime));
				if (res2 != null)
					return res2;
				else if ((startTime = System.nanoTime())>endTime)
					hasNotFailedFlag = false;
				else
					myListOfVisited.add(extraNextNode);
				curPath [curPos] = 0;

				if (wasChecked == false){
					ArrayList<ArrayList<Integer>> variablesToCheck = 
							AgentPartialSol.cpCaller.getVariablesArrayList(myAgent.createSucPredEsp(curPath, myAgent.instance.inputs.getNodes().length, myAgent.instance.inputs.getVehs()));
					res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);
					wasChecked = true;
					if (res == false){
						//System.out.println(curPath);
						hasNotFailedFlag = false;
						/*for (Integer x : curPath){
							//		System.out.print(x + " , ");
						}*/
						int a = curPos -1;
						System.out.println("             No           " + a);
					}


				}

			}
		}

		curPos--;
		//	curPath [curPos] = 0;
		myAgent.toVisit++;
		myAgent.visited[nextNode] = false;
		return null;
	}



	public void makeReady() {
		
	}

}
