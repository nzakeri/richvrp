package antColonyPartialSol;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.Callable;

import org.uoc.rgcws.ElapsedTime;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.Route;
import org.uoc.rgcws.Solution;

import sun.font.CreatedFontTracker;

import com.sun.org.apache.bcel.internal.generic.CPInstruction;

import eclipse.CheckCP;
import eclipse.CheckCP.myInt;
import antColonyPartialSol.ACOSolverPartialSol.WalkedWay;




//import de.jungblut.antcolony.AntColonyOptimization.WalkedWay;


public final class OldAgentPartialSol implements Callable<WalkedWay> {

	private final ACOSolverPartialSol instance;
	private double distanceWalked = 0.0d;
	private final int start;
	private final boolean[] visited;
	private final int[] way;
	private int toVisit;
	private Random random = null;
	float totalCapacity = 0;
	private boolean routeNotPossible = false;
	int goalPoint;
	int stepToCheck;
	public static  CheckCP cpCaller;
	File file = new File("K:\\RichVRP\\deps\\analyze_results\\partialSol_cvrp\\unfeasibleRes.txt");


	public OldAgentPartialSol(ACOSolverPartialSol instance, int start,int seed) {
		super(); //call its father's constructor
		this.instance = instance;
		this.visited = new boolean[instance.matrix.length];
		visited[start] = true;
		toVisit = visited.length;
		this.start = start;
		this.way = new int[visited.length];
		random = new Random(seed);
		//System.out.println("seed is "+seed);
		goalPoint = (this.instance.inputs.getNodes().length)+ (this.instance.inputs.getVehs()/2);
		stepToCheck = 1;//(instance.matrix.length)/10;
	}


	// TODO really needs improvement
	private final int getNextProbableNode(int y, HashSet<Integer> myListOfVisited) {


		if (toVisit > 0) {
			int danglingUnvisited = -1;
			final double[] weights = new double[visited.length];

			ArrayList<Integer> computableNodes = new ArrayList<Integer>();


			for (int j = 0; j < visited.length; j++){

				float remain = this.instance.inputs.getVehCap() - totalCapacity;
				if (!visited[j] && !myListOfVisited.contains(j)){

					float nodeDemand1 =0;

					if (j < this.instance.inputs.getNodes().length){

						nodeDemand1 = this.instance.inputs.getNodes()[j].getDemand();
					}

					//if(  nodeDemand1 <= remain) {
					computableNodes.add(j);

					//}

				}

			}

			/*if (computableNodes.isEmpty()){

				routeNotPossible  = true;

			}*/



			double sum = 0.0d;

			//	      for (int i = 0; i < computableNodes.size(); i++){
			//	    	  int x = computableNodes.get(i);
			for (Integer x : computableNodes) {
				weights[x] = calculateNonNormalizedProbability(x, y);
				sum += weights[x];
				danglingUnvisited = x;
			}

			if (sum == 0.0d)
				return danglingUnvisited;


			double probSum = 0.0d;
			double curRand = random.nextDouble();
			final double r = curRand*sum;
			for (Integer x2 : computableNodes) {

				probSum +=weights[x2];

				if (r <= probSum) {

					float nextNodedemand =0;

					if (x2 < this.instance.inputs.getNodes().length){

						nextNodedemand = this.instance.inputs.getNodes()[x2].getDemand();
						totalCapacity += nextNodedemand;
					}else{

						totalCapacity = 0;
					}
					/*if (x2 == goalPoint){
						routeNotPossible  = true;
					}
					 */
					return x2;

				}
			}
		}


		return -1;
	}



	private final double calculateNonNormalizedProbability(int row, int column) {
		final double p = Math.pow(instance.readPheromone(column, row),
				ACOSolverPartialSol.ALPHA)
				* Math.pow(instance.invertedMatrix[column][row],
						ACOSolverPartialSol.BETA);
		return (p + this.instance.probBias);
	}


	public final int [] backTrack(int nextNode, int [] curPath, int curPos) throws Exception {

		curPath[curPos]=nextNode;
		visited[nextNode] = true;
		curPos++;
		toVisit--;
		boolean hasNotFailedFlag = true;


		ArrayList<ArrayList<Integer>> variablesToCheck = 
				cpCaller.getVariablesArrayList(createSucPredEsp(curPath, this.instance.inputs.getNodes().length, this.instance.inputs.getVehs()));
		boolean res = cpCaller.checkEsp(variablesToCheck);
		if (res == false){
			//System.out.println(curPath);
			hasNotFailedFlag = false;
			for (Integer x : curPath){
				//		System.out.print(x + " , ");
			}
			int a = curPos -1;
			System.out.println("             No           " + a);

		}

		if (curPos == visited.length && hasNotFailedFlag){
			System.out.println("yes");
			/*for (Integer x : curPath){
			System.out.print(x + " , ");
			}*/
			//	System.out.println();
			return curPath;	

		}

		HashSet<Integer> myListOfVisited = new HashSet<Integer>(); 

		while(hasNotFailedFlag){

			int extraNextNode = getNextProbableNode(nextNode,myListOfVisited);

			if (extraNextNode == -1){
				hasNotFailedFlag = false;

			}else{
				int[] res2 = backTrack(extraNextNode, curPath, curPos);
				if (res2 != null)
					return res2;
				else
					myListOfVisited.add(extraNextNode);
				curPath [curPos] = 0;
			}
		}

		curPos--;
		//	curPath [curPos] = 0;
		toVisit++;
		visited[nextNode] = false;
		return null;
	}

	public final int [] backTrackWithTimeLimit(int nextNode, int [] curPath, int curPos, double timePortion) throws Exception {
		long startTime = System.nanoTime();

		curPath[curPos]=nextNode;
		visited[nextNode] = true;
		curPos++;
		toVisit--;
		boolean hasNotFailedFlag = true;
		int nBranches = toVisit;


		if (( nBranches % stepToCheck) == 0 ||  (curPos == visited.length )){

			ArrayList<ArrayList<Integer>> variablesToCheck = 
					cpCaller.getVariablesArrayList(createSucPredEsp(curPath, this.instance.inputs.getNodes().length, this.instance.inputs.getVehs()));
			boolean res = cpCaller.checkEsp(variablesToCheck);
			if (res == false){
				//System.out.println(curPath);
				hasNotFailedFlag = false;
				/*for (Integer x : curPath){
				System.out.print(x + " , ");
				}*/
				int a = curPos -1;
				System.out.println("             No           " + a);
				
				
				
				try(PrintWriter writeSol = new PrintWriter(new BufferedWriter
						(new FileWriter(file, true)))) {
					for (Integer x : curPath){
						writeSol.print(x + " , ");
						}
					writeSol.println ();					
					

				}catch (IOException e) {
					System.err.println(e);
				}
			}
		}
		if (curPos == visited.length && hasNotFailedFlag){
			System.out.println("yes");
			/*for (Integer x : curPath){
				System.out.print(x + " , ");
			}
			System.out.println();*/
			return curPath;	

		}


		HashSet<Integer> myListOfVisited = new HashSet<Integer>(); 

		while(hasNotFailedFlag){

			int extraNextNode = getNextProbableNode(nextNode,myListOfVisited);

			if (extraNextNode == -1){
				hasNotFailedFlag = false;

			}else{
				//int timeDiv = Math.min(nBranches,10);
				int timeDiv = 4;
				//int[] res2 = backTrackWithTimeLimit(extraNextNode, curPath, curPos,timePortion/nBranches);
				int[] res2 = backTrackWithTimeLimit(extraNextNode, curPath, curPos,timePortion/timeDiv);//Exploring 4 branches in each level
				if (res2 != null )
					return res2;
				else if ((System.nanoTime()-startTime) > timePortion)
					hasNotFailedFlag = false;
				myListOfVisited.add(extraNextNode);
				curPath [curPos] = 0;

			}
		}

		curPos--;
		//	curPath [curPos] = 0;
		toVisit++;
		visited[nextNode] = false;
		return null;
	}
	
	public final int [] backTrackWithExactTimeLimit(int nextNode, int [] curPath, int curPos, long startTime, long endTime) throws Exception {
		
		
		curPath[curPos]=nextNode;
		visited[nextNode] = true;
		curPos++;
		toVisit--;
		boolean hasNotFailedFlag = true;
		boolean res = true;
		boolean wasChecked = false;
		
		if (( toVisit % stepToCheck) == 0 ||  (curPos == visited.length )){
			ArrayList<ArrayList<Integer>> variablesToCheck = 
					cpCaller.getVariablesArrayList(createSucPredEsp(curPath, this.instance.inputs.getNodes().length, this.instance.inputs.getVehs()));
			res = cpCaller.checkEsp(variablesToCheck);
			wasChecked = true;
		}
		if (res == false){
			//System.out.println(curPath);
			hasNotFailedFlag = false;
			for (Integer x : curPath){
				//		System.out.print(x + " , ");
			}
			int a = curPos -1;
			System.out.println("             No           " + a);
			/*
			try(PrintWriter writeSol = new PrintWriter(new BufferedWriter
					(new FileWriter(file, true)))) {
				for (Integer x : curPath){
					writeSol.print(x + " , ");
					}
				writeSol.println ();					
				

			}catch (IOException e) {
				System.err.println(e);
			}*/
			
			
		}
		
		if (curPos == visited.length && hasNotFailedFlag){
			System.out.println("yes");
			/*for (Integer x : curPath){
			System.out.print(x + " , ");
			}*/
			//	System.out.println();
			return curPath;	

		}
		
		long childTimeSlices = (long) ((endTime-startTime)/Math.min(toVisit,1.2));


		HashSet<Integer> myListOfVisited = new HashSet<Integer>(); 

		while(hasNotFailedFlag){

			int extraNextNode = getNextProbableNode(nextNode,myListOfVisited);

			if (extraNextNode == -1){
				hasNotFailedFlag = false;
			}else{
				int[] res2 = backTrackWithExactTimeLimit(extraNextNode, curPath, curPos,startTime,Math.min(startTime+childTimeSlices,endTime));
				if (res2 != null)
					return res2;
				else if ((startTime = System.nanoTime())>endTime)
					hasNotFailedFlag = false;
				else
					myListOfVisited.add(extraNextNode);
				curPath [curPos] = 0;
				
				if (wasChecked == false){
					ArrayList<ArrayList<Integer>> variablesToCheck = 
							cpCaller.getVariablesArrayList(createSucPredEsp(curPath, this.instance.inputs.getNodes().length, this.instance.inputs.getVehs()));
					res = cpCaller.checkEsp(variablesToCheck);
					wasChecked = true;
					if (res == false){
						//System.out.println(curPath);
						hasNotFailedFlag = false;
						for (Integer x : curPath){
							//		System.out.print(x + " , ");
						}
						int a = curPos -1;
						System.out.println("             No           " + a);
					}

					
				}
				
			}
		}

		curPos--;
		//	curPath [curPos] = 0;
		toVisit++;
		visited[nextNode] = false;
		return null;
	}


	public final WalkedWay call() throws Exception { 

		/*try {
			new FileWriter(file,false);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/

		long curTime = System.nanoTime();
		
				int[] foundWay = backTrackWithExactTimeLimit(start, way, 0, curTime, curTime + 100000000000L);
//				int[] foundWay = backTrackWithTimeLimit(start, way, 0 ,10000000000L*1000000000000000000L);
		//		int[] foundWay = backTrack(start, way, 0);

		//assert (foundWay != null): "foundWay is null";;

		if (foundWay!=null){
			for (int k=0; k<foundWay.length-1; k++){
				distanceWalked += instance.matrix[foundWay[k]][foundWay[k+1]];
			}
			double a =0;
			a = instance.matrix[foundWay[foundWay.length-1]][0];
			distanceWalked += a ;


			double phero = (ACOSolverPartialSol.Q / (distanceWalked ));
			int previous = foundWay[0];
			for(int h = 1; h < foundWay.length; h++){
				instance.adjustPheromone(previous, foundWay[h], phero);
				previous = foundWay[h];

			}
		}else{
			routeNotPossible = true;
			System.out.println("Solution is not found!!!!!");

		}

		return new WalkedWay(foundWay, distanceWalked + (routeNotPossible?1000000000:0));
	}

	public ArrayList<int[]> createSucPredEsp(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else {
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}

		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		//		System.out.print(" visits: ");
		//		for (int vis : visits){
		//			System.out.print( vis + "  ");
		//		}
		//		System.out.println("\n");
		//		System.out.print(" succesors: ");
		//		for (int suc : succesors){
		//			System.out.print( suc + "  ");
		//		}
		//
		//		System.out.println("\n");
		//		System.out.print(" predecessors: ");
		//		for (int pre : predecessors){
		//			System.out.print( pre + "  ");
		//		}
		//		System.out.println("\n");

		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}


}





