package antColonyPartialSol;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.Callable;

import org.uoc.rgcws.ElapsedTime;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.Route;
import org.uoc.rgcws.Solution;

import sun.font.CreatedFontTracker;

import com.sun.org.apache.bcel.internal.generic.CPInstruction;

import eclipse.CheckCP;
import eclipse.CheckCP.myInt;
import antColonyPartialSol.ACOSolverGlobalUpdate.WalkedWay;




//import de.jungblut.antcolony.AntColonyOptimization.WalkedWay;


public final class AntFunctionGlobalUpdate implements Callable<WalkedWay> {

	public final ACOSolverGlobalUpdate instance;
	private double distanceWalked = 0.0d;
	private final int start;
	public  boolean[] visited;
	public  int[] way;
	public int toVisit;
	private Random random = null;
	float totalCapacity = 0;
	private boolean routeNotPossible = false;
	int goalPoint;
	int stepToCheck;
	private float[] distanceWalkedArr;
	private int groupAgent;
	private int[] antWay;
	private Double groupCost = 0.0;




	public static CheckCP cpCaller;


	public AntFunctionGlobalUpdate(ACOSolverGlobalUpdate instance, int start,int seed) {
		super(); //call its father's constructor
		this.instance = instance;
		this.visited = new boolean[instance.matrix.length];
		visited[start] = true;
		toVisit = visited.length;
		this.start = start;
		this.way = new int[visited.length];
		random = new Random(seed);
		//System.out.println("seed is "+seed);
		goalPoint = (this.instance.inputs.getNodes().length)+ (this.instance.inputs.getVehs()/2);
		stepToCheck = 5;//(instance.matrix.length)/10;
	}


	// TODO really needs improvement
	public final int getNextProbableNode(int y) {

		if (toVisit > 0) {
			int danglingUnvisited = -1;
			final double[] weights = new double[visited.length];
			ArrayList<Integer> computableNodes = new ArrayList<Integer>();

			for (int j = 0; j < instance.inputs.getNodes().length; j++){
				//float remain = this.instance.inputs.getVehCap() - totalCapacity;
				if (!visited[j]){

					//float nodeDemand1 =0;

					/*if (j < this.instance.inputs.getNodes().length){

						nodeDemand1 = this.instance.inputs.getNodes()[j].getDemand();
					}
					 */
					//if(  nodeDemand1 <= remain) {
					computableNodes.add(j);

					//}

				}

			}

			if (computableNodes.isEmpty())
				return -1;

			/*if (computableNodes.isEmpty()){

				routeNotPossible  = true;

			}*/



			double sum = 0.0d;

			//	      for (int i = 0; i < computableNodes.size(); i++){
			//	    	  int x = computableNodes.get(i);
			for (Integer x : computableNodes) {
				weights[x] = calculateNonNormalizedProbability(x, y);
				sum += weights[x];
				danglingUnvisited = x;
			}

			if (sum == 0.0d)
				return danglingUnvisited;


			double probSum = 0.0d;
			double curRand = random.nextDouble();
			final double r = curRand*sum;
			for (Integer x2 : computableNodes) {

				probSum +=weights[x2];

				if (r <= probSum) {

					//float nextNodedemand =0;

					/*if (x2 == goalPoint){
						routeNotPossible  = true;
					}
					 */
					return x2;

				}
			}
		}


		return -1;
	}



	public final double calculateNonNormalizedProbability(int row, int column) {
		final double p = Math.pow(instance.readPheromone(column, row),
				ACOSolverGlobalUpdate.ALPHA)
				* Math.pow(instance.invertedMatrix[column][row],
						ACOSolverGlobalUpdate.BETA);
		return (p + this.instance.probBias);
	}




	public final WalkedWay call() throws Exception { 


		int nextDepot = instance.inputs.getNodes().length;
		int maxNodePos = instance.inputs.getNodes().length + instance.inputs.getVehs()-1;
		int i = 1;
		for (; i < maxNodePos; i++ ){

			int nextNode = getNextProbableNode(way[i-1]);


			if (nextNode != -1){

				way[i] = nextNode;
				visited[nextNode] = true;

				ArrayList<ArrayList<Integer>> variablesToCheck = 
						AgentPartialSol.cpCaller.getVariablesArrayList(createSucPredEsp(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));

				boolean res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);

				if (!res){
					visited[nextNode] = false;
					if (nextDepot == maxNodePos){
						break;
					}
					way[i] = nextDepot;
					visited[nextDepot] = true;
					nextDepot++;
				}
			}
			else{
				way[i] = nextDepot;
				visited[nextDepot] = true;
				nextDepot++;
				if (nextDepot == maxNodePos){
					i++;
					break;
				}
			}

		}

		int nMissed = maxNodePos - i;
		float maxDistance = 100000000;//1000000;

		for (int k=0; k<i-1; k++){
			distanceWalked += instance.matrix[way[k]][way[k+1]];
		}
		double a =0;
		a = instance.matrix[way[way.length-1]][0];
		distanceWalked += a ;
		if (nMissed > 0){
			distanceWalked += maxDistance*(nMissed);
			//distanceWalkedArr[groupAgent] +=maxDistance*(nMissed+1);//New added
		}
		else {
			//			run full cp
			ArrayList<ArrayList<Integer>> variablesToCheck = 
					AgentPartialSol.cpCaller.getVariablesArrayList(createSucPredCompleteSol(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));

			boolean res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);
			if (!res){
				distanceWalked += maxDistance*(1);

			}
		}

		instance.arrayWays.add(way);
		instance.arrayDis.add(distanceWalked);
		instance.stepToUpdatePher +=1;
		if (instance.stepToUpdatePher == 100) //New added
		{



			double minDisInGroup = instance.arrayDis.get(0);

			System.out.println("!!!!PHEROMONE UPDATING PROCESS!!!!");

			//System.out.println (minDisInGroup);

			double minPheremone = 0;
			int minID = 0;
			int numberOfChanges  = 0;
			for(int h3=0; h3 < instance.stepToUpdatePher; h3++){

				double phero = (ACOSolverGlobalUpdate.Q / (instance.arrayDis.get(h3)));//calculate pheromone for each ant in the group

				if (instance.arrayDis.get(h3) < minDisInGroup) {

					minDisInGroup = instance.arrayDis.get(h3);
					minID  = h3;
					minPheremone = phero;

					numberOfChanges += 1;				


					System.out.println("HASSAAAAAAAAAAANNNNNNNNNNNNNNN");

				}

				//System.out.println("Khoobi?");
			}



			System.out.println("Updating the Min Dis In This Group" + "                 " + minDisInGroup);


			int previous = instance.arrayWays.get(minID)[0]; //update pheromone for a solution found by the best ant in this group
			for(int h = 1; h < instance.arrayWays.get(minID).length; h++){  
				instance.adjustPheromone(previous, instance.arrayWays.get(minID)[h], minPheremone);
				previous = instance.arrayWays.get(minID)[h];
			}





			//System.out.println ("==============");		
			System.out.println("We are Updating the bestKnown Solution!" + "                 " + 
					instance.bestKnown.distance);

			if (instance.bestKnown.distance < instance.arrayDis.get(minID)){

				System.out.println ("==============");


				double phero = (ACOSolverGlobalUpdate.Q / (instance.bestKnown.distance));

				int previousBest = instance.bestKnown.way[0];			

				for(int h = 1; h < instance.bestKnown.way.length; h++){  				

					instance.adjustPheromone(previousBest, instance.bestKnown.way[h], phero);
					previousBest = instance.bestKnown.way[h];
				}

			}

			PickUp_Delivery.writeAveCost.write("AntID" + minID + "              " + "Number Of Changes: " + numberOfChanges
					+ "\r\n" + "Minimum distance in this group:" + "    "+ minDisInGroup +  "\r\n");
			PickUp_Delivery.writeAveCost.write("-----------------------------------------------------------------\r\n");
			PickUp_Delivery.writeAveCost.flush();

			groupCost = 0.0;
			instance.stepToUpdatePher = 0;
			instance.arrayWays.clear();
			instance.arrayDis.clear();

			/*for(int h2 = 1; h2 < instance.bestKnown.way.length-1; h2++){  				

				System.out.println(instance.pheromones[h2][h2+1]);

			}
			 */

		}


		//	System.out.println("Number of missed Nodes:" + nMissed);
		return new WalkedWay(way,distanceWalked);
	}

	public ArrayList<int[]> createSucPredEsp(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else {
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}

		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}


	public ArrayList<int[]> createSucPredCompleteSol(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else {
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}

		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}

}





