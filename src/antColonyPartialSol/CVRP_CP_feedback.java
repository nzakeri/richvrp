package antColonyPartialSol;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.uoc.rgcws.ElapsedTime;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.InputsManager;
import org.uoc.rgcws.RandCWS;
import org.uoc.rgcws.Solution;
import org.uoc.rgcws.Test;
import org.uoc.rgcws.TestsManager;

import antColony.AntColonyOptimization;
import antColonyPartialSol.ACOSolverPartialSol.WalkedWay;
import umontreal.iro.lecuyer.rng.LFSR113;
import eclipse.CheckCP;

public class CVRP_CP_feedback {


	final static String inputFolder = "inputs\\CPAIOR2015\\cvrp";
	final static String outputFolder = "outputs\\paper_first\\cvrp";
	final static String fileNameTest = "setP.txt";

	final static String testFolder = "tests\\CPAIOR2015";
	final static String sufixFileNodes = "_input_nodes.txt";
	final static String sufixFileVehicules = "_input_vehicles.txt";
	final static String sufixFileOutput = "_outputs.txt";
    public static FileWriter writeAveCost;
    
	public static void main(String[] args) throws Exception
	{
		System.out.println("****  WELCOME TO THIS PROGRAM  ****");
		long programStart = ElapsedTime.systemTime();

		/***************************************************************************
		 * 1. GET THE LIST OF TESTS TO RUN FROM "test2run.txt"
		 *  aTest = instanceName + testParameters
		 ***************************************************************************/
		String testsFilePath = testFolder + File.separator + fileNameTest;
		ArrayList<Test> testsList = TestsManager.getTestsList(testsFilePath);

		/***************************************************************************
		 * 2. FOR EACH TEST (instanceName + testParameters) IN THE LIST...
		 ***************************************************************************/
		int nTests = testsList.size();
		System.out.println("numbre de test "+nTests);
		for( int k = 0; k < nTests; k++ )
		{   
			int count = 0;
			ArrayList<Solution> solList = new ArrayList<Solution>();

			Test aTest = testsList.get(k);
			CheckCP cpChecker = new CheckCP(aTest.getInstanceName());
			AgentPartialSol.cpCaller = cpChecker;

			String inputNodesPath = inputFolder + File.separator +
					aTest.getInstanceName() + sufixFileNodes;
		//	System.out.println(inputNodesPath);

			String inputVehPath = inputFolder + File.separator +
					aTest.getInstanceName() + sufixFileVehicules;


			int s = Math.max(aTest.getSeed(), 128);
			int seedArray[] = { s, s, s, s };
			LFSR113.setPackageSeed(seedArray); // L'Ecuyer LFSR113 (period 2^113-1)
			LFSR113 rand = new LFSR113();

			Inputs inputs = InputsManager.readInputs(inputNodesPath, inputVehPath);



			double elapsed = 0;
			long startTime = ElapsedTime.systemTime();



			String date = new SimpleDateFormat("dd-MM-HH-mm-").format(new Date());



			String outputsFilePath = outputFolder + File.separator +
					aTest.getInstanceName() + "_" + aTest.getSeed() +"_"+ date + sufixFileOutput;


			//while (elapsed < aTest.getMaxTime()) {
				
				
				while (elapsed < 50) {
				

				ACOSolverPartialSol antColonyOptimization = new ACOSolverPartialSol(aTest, inputs,rand.nextInt(0, Integer.MAX_VALUE-1));
				Solution acoSol = null;		
				
				try {
					String date2 = new SimpleDateFormat("dd-MM-HH-mm-ss").format(new Date());
					writeAveCost = new FileWriter("gen\\paper_first" + File.separator +
							aTest.getInstanceName() + "_" + aTest.getSeed() +"_"+ date2 + ".txt");
					 
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//Starting the search

				try {
					acoSol = antColonyOptimization.solve(inputs);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				//add each solution to an array
				if (acoSol!=null){
					solList.add(acoSol);
				}

				//elapsed = ElapsedTime.calcElapsed(startTime, ElapsedTime.systemTime());
				elapsed +=1;

			}
			int numNodes = inputs.getNodes().length;
			int numVeh = inputs.getVehs();//solList.get(k1).getRoutes().size();
			float vehCapacity = inputs.getVehCap();
			float maxDis = aTest.getMaxRouteCosts();
			//          CheckCP cpChecker = new CheckCP(aTest.getInstanceName());
			/*int yesCount = 0;
          int noCount = 0;*/
			//check  feasibility of each solution
			File file = new File(outputsFilePath);
			try {
				new FileWriter(file,false);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for(int k1 = 0; k1 < solList.size(); k1++){


				/*count++;

        	  if ( (count % 10) == 0)
        	  {
        		  System.out.println(count);
        	  }
        	  ArrayList<ArrayList<Integer>> variablesToCheck = 
        			  cpChecker.getVariablesArrayList(solList.get(k1).createSucPred(numNodes, numVeh));

        	  if (cpChecker.check(variablesToCheck)) {
        		  System.out.println("yes");


        		  yesCount ++;


        	  }else{
        		  System.out.println("no");


        		  noCount ++;
        	  }*/

				try(PrintWriter writeSol = new PrintWriter(new BufferedWriter
						(new FileWriter(file, true)))) {
					writeSol.println(solList.get(k1).toString() + "\r");
					writeSol.println ("--------------------------------------------");
					writeSol.close();

				}catch (IOException e) {
					System.err.println(e);
				}



			}	

			// float proportion =  ((float)yesCount / solList.size())*100;
			try(PrintWriter out = new PrintWriter(new BufferedWriter
					(new FileWriter("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\gen\\paper_first\\cvrp.txt", true)))) {
				
				long programEnd = ElapsedTime.systemTime();
				out.println("Total elapsed time = "
						+ ElapsedTime.calcElapsedHMS(programStart, programEnd) + "\t\t" + date);
				out.println("\r\n" + aTest.getInstanceName()+"\t\t" + solList.size() +"\t\t"  +
						numNodes+ "\t\t"+ vehCapacity +"\t\t"+ maxDis +"\t\t"+ aTest.getMaxTime()+"\t\t" + ACOSolverPartialSol.NUM_AGENTS+ "\r\n" );
				out.println("-----------------------------------------------------------------");
				//out.println(yesCount);
				out.close();
			}catch (IOException e) {
				System.err.println(e);
			}




		}


		/***************************************************************************
		 * 3. END OF PROGRAM
		 ***************************************************************************/
		System.out.println("\n****  END OF PROGRAM, CHECK OUTPUTS FILES  ****");
		long programEnd = ElapsedTime.systemTime();
		System.out.println("Total elapsed time = "
				+ ElapsedTime.calcElapsedHMS(programStart, programEnd));


	}

}
