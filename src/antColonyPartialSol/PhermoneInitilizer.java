package antColonyPartialSol;

import java.util.ArrayList;
import java.util.Random;

public class PhermoneInitilizer{

	private ACOSolverPartialSol theSolver;
	private int nNotVisited;
	Random rand;
	boolean visited [];
	private int stepToCheck;
	int curPath [];
	int nNodes;
	int vehOrder;
	
	public PhermoneInitilizer(ACOSolverPartialSol acoSolverPartialSol,
			long theSeed) {
		this.theSolver = acoSolverPartialSol;
		this.stepToCheck = 1;// 5 is good for setP
		nNodes = nNotVisited = theSolver.inputs.getNodes().length;
		rand = new Random(theSeed);
		visited = new boolean [nNotVisited];
		curPath = new int [nNotVisited];
		vehOrder = theSolver.inputs.getVehs();
		
	}

	public final boolean getOneCarTripRecursively(int nextNode, int curPos) throws Exception {

		curPath[curPos]=nextNode;
		visited[nextNode] = true;
		curPos++;
		nNotVisited--;
		boolean weCanReturnTrueAndSetPhermones = false;
		if (( curPos % stepToCheck) == (stepToCheck-1) || nNotVisited == 0 ){

			/*ArrayList<ArrayList<Integer>> variablesToCheck = 
					AgentPartialSol.cpCaller.getVariablesArrayList(createSucPredEsp(curPath, theSolver.inputs.getNodes().length, theSolver.inputs.getVehs()));*/
			
			
			ArrayList<ArrayList<Integer>> variablesToCheck = 
					AgentPartialSol.cpCaller.getVariablesArrayList(createSucPredEspOrder(curPath, theSolver.inputs.getNodes().length, theSolver.inputs.getVehs()));
			
			boolean res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);
			//wasChecked = true;

			if (res == false){
				int a = curPos -1;
				System.out.println("             No           " + a);
				curPos--;
				nNotVisited++;
				visited[nextNode] = false;
				curPath [curPos] = 0;
				return false;
			}
		}
		int myLocalnNotVisited = nNotVisited;
		if (nNotVisited != 0)
		{
			int extraNextNode = getRandomFreeNodeBasedOnDistance(nextNode );
			boolean initRes = getOneCarTripRecursively(extraNextNode, curPos);

			if (initRes == false){
				/*ArrayList<ArrayList<Integer>> variablesToCheck = 
						AgentPartialSol.cpCaller.getVariablesArrayList(createSucPredEsp(curPath, theSolver.inputs.getNodes().length, theSolver.inputs.getVehs()));*/
				
				ArrayList<ArrayList<Integer>> variablesToCheck = 
						AgentPartialSol.cpCaller.getVariablesArrayList(createSucPredEspOrder(curPath, theSolver.inputs.getNodes().length, theSolver.inputs.getVehs()));
				
				boolean res = AgentPartialSol.cpCaller.checkEsp(variablesToCheck);

				if (res == true){
					weCanReturnTrueAndSetPhermones = true;
				}
				else{
					curPos--;
					nNotVisited++;
					visited[nextNode] = false;
					curPath [curPos] = 0;
					return false;
				}
			}
		}

		if (weCanReturnTrueAndSetPhermones || myLocalnNotVisited == 0){
			for (Integer x : curPath){
				System.out.print(x + " , ");
			}
			System.out.println();

			double phero = 2d;
			int previous = curPath[0];
			int last = curPath[curPos-1];
			for(int h = 1; h < curPos; h++){
				theSolver.adjustPheromone(previous, curPath[h], phero);
				previous = curPath[h];
				curPath[h] = 0;
			}
			theSolver.adjustPheromone(last, curPath[0], phero);
			for (int j = 1; j < nNodes; j++){
				this.visited[j] = false;				
				
			}

			
			System.out.println(vehOrder);
			if (curPos != 0 && curPos != 1){
			vehOrder = vehOrder - 1; //for sending results of vehicles to CP in defined order
			}
			
		}

		return true;
	}

	// TODO really needs improvement
	public final int getRandomFreeNodeBasedOnDistance( int curPos) throws Exception {

		double weights[] = new double [nNodes];

		ArrayList<Integer> computableNodes = new ArrayList<Integer>();

		for (int j = 1; j < nNodes; j++){
			if (!this.visited[j]){
				computableNodes.add(j);
			}
		}


		double sum = 0.0d;

		for (Integer x : computableNodes) {
			weights[x] = Math.pow(theSolver.invertedMatrix[curPos][x], ACOSolverPartialSol.BETAInitialization);
			sum += weights[x];
		}

		if (sum < 0.000000000000000000000000001d)
			throw new Exception("too small sum for probability");


		double probSum = 0.0d;
		double curRand = rand.nextDouble();
		final double r = curRand*sum;
		for (Integer x2 : computableNodes) {

			probSum +=weights[x2];

			if (r <= probSum) {
				return x2;
			}
		}

		throw new Exception("we shouldn't be here");
	}

	public void initialize() throws Exception {
		nNotVisited--;
		
		do {
			nNotVisited++;
			getOneCarTripRecursively(0,0);
		}while (vehOrder != 0);  //(nNotVisited > 0 ||  vehOrder == 0); 
	}


	public ArrayList<int[]> createSucPredEsp(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		//int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				//visits[parWay[i]] = k +1;
				visits[parWay[i]] = vehOrder;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else {
//					predecessors[parWay[i]] = numNodes+k;
//					succesors[numNodes + k] = parWay[i];
					predecessors[parWay[i]] = numNodes+vehOrder-1;
					succesors[numNodes + vehOrder-1] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
//					succesors[parWay[i]] = numNodes+k;
//					predecessors[numNodes+k] = parWay[i];
//					k += 1;
					succesors[parWay[i]] = numNodes+vehOrder-1;
					predecessors[numNodes+ vehOrder-1] = parWay[i];
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}

		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		//		System.out.print(" visits: ");
		//		for (int vis : visits){
		//			System.out.print( vis + "  ");
		//		}
		//		System.out.println("\n");
		//		System.out.print(" succesors: ");
		//		for (int suc : succesors){
		//			System.out.print( suc + "  ");
		//		}
		//
		//		System.out.println("\n");
		//		System.out.print(" predecessors: ");
		//		for (int pre : predecessors){
		//			System.out.print( pre + "  ");
		//		}
		//		System.out.println("\n");

		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}

	void updateIntPherOfDepots(){

		int j = this.theSolver.pheromones.length;
		int i = this.theSolver.inputs.getNodes().length;
		for (int k = i; k < j; k ++)
			for (int h = 0; h < j; h++){

				this.theSolver.pheromones[k][h] = this.theSolver.pheromones[0][h];
				this.theSolver.pheromones[h][k] = this.theSolver.pheromones[h][0];
				//System.out.println(this.theSolver.matrix[k][h]+ ";" + this.theSolver.matrix[0][h]);

				//System.out.println(this.theSolver.matrix[h][k]+ ";" + this.theSolver.matrix[h][0]);

			}
	/*	for (int row = 0; row < j; row ++){
			for (int column = 0; column < j; column++){


				System.out.print(this.theSolver.pheromones[row][column] + "  ;  ");


			}
			System.out.println();

		}*/
	}
	
	
	public ArrayList<int[]> createSucPredEspOrder(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		//int k =0;

		System.out.println(vehOrder);
		System.out.println("fffffffffffffffffffffff");
		
		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				
				visits[parWay[i]] = vehOrder;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else {
					
					predecessors[parWay[i]] = numNodes+vehOrder-1;
					succesors[numNodes + vehOrder-1] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()

					succesors[parWay[i]] = numNodes+vehOrder-1;
					predecessors[numNodes+ vehOrder-1] = parWay[i];
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}

		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		

		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}

	
}
