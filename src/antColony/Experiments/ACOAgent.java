package antColony.Experiments;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

import org.uoc.rgcws.Inputs;

import antColony.Experiments.ACOsolver.WalkedWay;



//import de.jungblut.antcolony.AntColonyOptimization.WalkedWay;


public final class ACOAgent implements Callable<WalkedWay> {

	private final ACOsolver instance;
	private double distanceWalked = 0.0d;
	private final int start;
	private final boolean[] visited;
	private final int[] way;
	private int toVisit;
	private Random random = null;
	float totalCapacity = 0;
	private boolean routeNotPossible = false;
	int starPoint=0;	
	int aveVeh;
	public ACOAgent(ACOsolver instance, int start,int seed) {
		super(); //call its father's constructor
		this.instance = instance;
		this.visited = new boolean[instance.matrix.length];
		visited[start] = true;
		toVisit = visited.length - 1;
		this.start = start;
		this.way = new int[visited.length];
		random = new Random(seed);	
		aveVeh = this.instance.inputs.getVehs();
		
		//System.out.println("seed is "+seed);
		
		//starPoint = (this.instance.inputs.getNodes().length)+ (this.instance.inputs.getVehs()/2);
	}


	// TODO really needs improvement
	private final int getNextProbableNode(int y) {


		if (toVisit > 0) {
			int danglingUnvisited = -1;
			final double[] weights = new double[visited.length];

			ArrayList<Integer> computableNodes = new ArrayList<Integer>();


			for (int j = 0; j < visited.length; j++){

				float remain = this.instance.inputs.getVehCap() - totalCapacity;
				if (!visited[j]){

					float nodeDemand1 =0;

					if (j < this.instance.inputs.getNodes().length){

						nodeDemand1 = this.instance.inputs.getNodes()[j].getDemand();
					}

					if(  nodeDemand1 <= remain) {
						computableNodes.add(j);

					}

				}

			}
			
			if (computableNodes.isEmpty()){
				
				routeNotPossible  = true;
				
			}
			
			//double columnSum = 0.0d;
			//	      for (int i = 0; i < visited.length; i++) {
				//	        columnSum += Math.pow(instance.readPheromone(y, i),
						//	            ACOsolver.ALPHA)
			//	            * Math.pow(instance.invertedMatrix[y][i],
			//	                ACOsolver.BETA);
			//	      }
			//
			//	      double sum = 0.0d;
			//	      for (int x = 0; x < visited.length; x++) {
			//	        if (!visited[x]) {
			//	          weights[x] = calculateProbability(x, y, columnSum);
			//	          sum += weights[x];
			//	          danglingUnvisited = x;
			//	        }
			//	      }

			double sum = 0.0d;

			//	      for (int i = 0; i < computableNodes.size(); i++){
			//	    	  int x = computableNodes.get(i);
			for (Integer x : computableNodes) {
				weights[x] = calculateNonNormalizedProbability(x, y);
				sum += weights[x];
				danglingUnvisited = x;
			}

			if (sum == 0.0d)
				return danglingUnvisited;

			// weighted indexing stuff
			//	      double pSum = 0.0d;
			//	      for (int i = 0; i < visited.length; i++) {
			//	        pSum += weights[i] / sum;
			//	        weights[i] = pSum;
			//	      }
			double probSum = 0.0d;
			double curRand = random.nextDouble();
			final double r = curRand*sum;
			for (Integer x2 : computableNodes) {

				probSum +=weights[x2];

				if (r <= probSum) {

					float nextNodedemand =0;

					if (x2 < this.instance.inputs.getNodes().length){

						nextNodedemand = this.instance.inputs.getNodes()[x2].getDemand();
						totalCapacity += nextNodedemand;
					}else{

						aveVeh  = aveVeh -1;
						totalCapacity = 0;
					}
					if (x2 == starPoint || aveVeh ==0){
						routeNotPossible  = true;
					}

					return x2;

				}
			}
		}


		return -1;
	}

	// test method
	//	  @SuppressWarnings("unused")
	//	  private final int calculateChoice(double[] probabilityDistr, Random rnd) {
	//	    double rndNumber = rnd.nextDouble();
	//	    int counter = -1;
	//
	//	    while (rndNumber > 0) {
	//	      rndNumber -= probabilityDistr[++counter];
	//	    }
	//
	//	    return counter;
	//	  }

	/*
	 * (pheromones ^ ALPHA) * ((1/length) ^ BETA) divided by the sum of all rows.
	 */
	//	  private final double calculateProbability(int row, int column, double sum) {
	//	    final double p = Math.pow(instance.readPheromone(column, row),
	//	        ACOsolver.ALPHA)
	//	        * Math.pow(instance.invertedMatrix[column][row],
	//	            ACOsolver.BETA);
	//	    return p / sum;
	//	  }

	private final double calculateNonNormalizedProbability(int row, int column) {
		final double p = Math.pow(instance.readPheromone(column, row),
				ACOsolver.ALPHA)
				* Math.pow(instance.invertedMatrix[column][row],
						ACOsolver.BETA);
		return (p + this.instance.probBias);
	}


	public final WalkedWay call() throws Exception { 

		int lastNodeId = start;
		int nextNodeId = start;
		int i = 0;
		
		while ((nextNodeId = getNextProbableNode(lastNodeId)) != -1) {
			way[i] = lastNodeId;
			i++;
			distanceWalked += instance.matrix[lastNodeId][nextNodeId];
			visited[nextNodeId] = true;
			lastNodeId = nextNodeId;
			toVisit--;
			if (routeNotPossible){
				break;
			}
		}
		distanceWalked += instance.matrix[lastNodeId][start];
		way[i] = lastNodeId;
		int fine = (routeNotPossible?ACOsolver.failurePunishmentRate*toVisit:0) ;	
		double phero = (ACOsolver.Q / (distanceWalked ));
//		if (routeNotPossible)
//			phero = -0.00000001;
		int previous = way[0];
		for(int h = 1; h < way.length; h++){
			instance.adjustPheromone(previous, way[h], phero);
			previous = way[h];
		}

		System.out.println ("DECAY STEP");
		instance.decayAllPheromones(ACOsolver.phermoneDecayStep);
		return new WalkedWay(way, distanceWalked + (routeNotPossible?fine:0));
	}
}




