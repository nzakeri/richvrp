package antColony.Experiments;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.uoc.rgcws.Edge;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.Node;
import org.uoc.rgcws.Route;
import org.uoc.rgcws.Solution;
import org.uoc.rgcws.Test;

import cern.jet.random.engine.MersenneTwister;
import connectionACOwithCP.ACOSolverGlobalUpdate;

public final class ACOsolver {

	public static String TSP_FILE = "C:\\Users\\Negar\\Dropbox\\projects\\berlin52.tsp.txt";
	// greedy
	//public static final double ALPHA = 0.6d;for all except hvrp
	public static final double ALPHA = 9d;
	// rapid selection
	//public static final double BETA = 5.5d; for all except hvrp
	public static final double BETA = 1d;

	// heuristic parameters
	//public static final double Q = 30d; // somewhere between 0 and 1......for all except hvrp
	public static final double Q = 1d;
	public static final double evaperationRate = 0.1d; // between 0 and 1
	public static final double INITIAL_PHEROMONES = 3d; // can be anything

	public static final double MAX_PHEROMONES = 3d;// INITIAL_PHEROMONES;
	public static final double MIN_PHEROMONES = 1d;

	// use power of 2
	public static final int NUM_AGENTS = 50000; //2048 * 20;
	private static final int POOL_SIZE = 1; //Runtime.getRuntime().availableProcessors();
	//public final double probBias = 0d;for all except hvrp
	public final double probBias = 0d;

	//private Uniform uniform;

	private static final ExecutorService THREAD_POOL = Executors
			.newFixedThreadPool(POOL_SIZE);
	static final int phermoneDecayStep = 1;
	public static final int failurePunishmentRate = 20000;

	//we get an agent to it and it executes until finish
	private final ExecutorCompletionService<WalkedWay> agentCompletionService = new ExecutorCompletionService<WalkedWay>(
			THREAD_POOL);

	final double[][] matrix;
	final double[][] invertedMatrix;

	private final double[][] pheromones;
	private final Object[][] mutexes;
	private MersenneTwister numgen;

	Inputs inputs = null;
	private double meanDis;

	public ACOsolver() throws IOException {
		// read the matrix
		matrix = readMatrixFromFile();
		invertedMatrix = invertMatrix();
		pheromones = initializePheromones();
		mutexes = initializeMutexObjects();
		// (double min, double max, int seed)
		//uniform = new Uniform(0, matrix.length - 1,1234);
		//(int) System.currentTimeMillis());
	}

	public ACOsolver(Test aTest, Inputs inputs, int seed) {
		this.inputs = inputs;
		matrix = createDistMatrix(inputs.getNodes() , inputs.getVehs());
		invertedMatrix = invertMatrix();
		pheromones = initializePheromones();
		mutexes = initializeMutexObjects();
		//uniform = new Uniform(new cern.jet.random.engine.MersenneTwister(seed));
		System.out.println("the acosolver seed is:" + seed);
		numgen = new cern.jet.random.engine.MersenneTwister(seed);
		//(int) System.currentTimeMillis());
	}
	private double[][] createDistMatrixOld(Node[] nodes, int vehNum) {

		int numVehicles = vehNum-1;
		int matSize = nodes.length + numVehicles;
		double[][] disMatrix = new double[matSize+10][matSize+10];
		double sumDistance = 0;		

		for(int i = 0; i < matSize; i++ ){
			for (int j = 0; j < matSize; j++){
				int ri = i;
				int rj = j;
				if (ri >= nodes.length)
					ri = 0;
				if (rj >= nodes.length)
					rj = 0;
				disMatrix[i][j] = calculateEuclidianDistance(nodes[ri].getX(), nodes[ri].getY(), 
						nodes[rj].getX(),nodes[rj].getY());	
				sumDistance += disMatrix[i][j]; //For calculate distance of external nodes
			}
		}

		//Calculate the amount of disMatrix for external Nodes
		meanDis = sumDistance / (matSize*matSize);

		for(int i2 = 0; i2 < matSize; i2++ ){
			for (int j2 = matSize; j2 < matSize+10; j2++){

				disMatrix[i2][j2] = meanDis;

			}
		}
		for (int i3=matSize; i3 < matSize+10 ; i3++ ){

			for(int j3 = 0; j3 < matSize+10; j3++ ){

				if (j3 >= matSize){
					disMatrix[i3][j3] = sumDistance;
				}else{
					disMatrix[i3][j3] = meanDis;
				}

			}
		}

		/*
			for (int i1 = 0; i1 < disMatrix.length; i1++) {
			    for (int j = 0; j < disMatrix.length; j++) {
			        System.out.print(disMatrix[i1][j] + " ");
			    }
			    System.out.print("\n");
			}*/



		return disMatrix;
	}

	private double[][] createDistMatrix(Node[] nodes, int vehNum) {

		//int numVehicles = vehNum-1;// the nodes already have one depot
		int numVehicles = vehNum;
		int matSize = nodes.length + numVehicles; 
		double[][] disMatrix = new double[matSize][matSize];
		double sumDistance = 0;		

		for(int i = 0; i < matSize; i++ ){
			for (int j = 0; j < matSize; j++){
				int ri = i;
				int rj = j;
				if (ri >= nodes.length)
					ri = 0;
				if (rj >= nodes.length)
					rj = 0;
				disMatrix[i][j] = calculateEuclidianDistance(nodes[ri].getX(), nodes[ri].getY(), 
						nodes[rj].getX(),nodes[rj].getY());	
				sumDistance += disMatrix[i][j]; //For calculate distance of external nodes
				//	System.out.print("  " + disMatrix[i][j]);
			}
			//	System.out.println();
		}


		return disMatrix;
	}

	private final Object[][] initializeMutexObjects() {
		final Object[][] localMatrix = new Object[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = new Object();
			}
		}

		return localMatrix;
	}

	final double readPheromone(int x, int y) {
		return pheromones[x][y];
	}

	/*final void adjustPheromone(int x, int y, double deltaPheromone) {
		synchronized (mutexes[x][y]) {
			pheromones[x][y] += deltaPheromone;
		}
	}*/
	final void adjustPheromone(int x, int y, double deltaPheromone) {
		synchronized (mutexes[x][y]) {
			pheromones[x][y] += deltaPheromone;
			//Adding on 11-09-2015
			if (pheromones[x][y] > MAX_PHEROMONES){
				pheromones[x][y] = MAX_PHEROMONES;
			}
			//	System.out.println (x+" " + y + " " +pheromones[x][y]);
		}
	}


	/* void decayAllPheromones(int times) {
		double mutliplier = Math.pow((1 - ACOsolver.evaperationRate),times);
		for (int x = 0; x < mutexes.length; x++){
			for (int y = 0; y < mutexes.length; y++){
				synchronized (mutexes[x][y]) {
					pheromones[x][y] *= mutliplier;
				}
			}
		}
	}*/

	void decayAllPheromones(int times) {
		double mutliplier = Math.pow((1 - ACOSolverGlobalUpdate.evaperationRate),times);
		for (int x = 0; x < mutexes.length; x++){
			for (int y = 0; y < mutexes.length; y++){
				synchronized (mutexes[x][y]) {
					pheromones[x][y] *= mutliplier;
					//Adding on 11-09-2015
					if (pheromones[x][y] < MIN_PHEROMONES){
						pheromones[x][y] = MIN_PHEROMONES;
						//	System.out.println("Lower Bound");
					}
				}
			}
		}
	}
	private final double[][] initializePheromones() {
		final double[][] localMatrix = new double[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = INITIAL_PHEROMONES;
			}
		}

		return localMatrix;
	}

	private final double[][] readMatrixFromFile() throws IOException {

		final BufferedReader br = new BufferedReader(new FileReader(new File(
				TSP_FILE)));

		final LinkedList<Record> records = new LinkedList<Record>();

		boolean readAhead = false;
		String line;
		while ((line = br.readLine()) != null) {

			if (line.equals("EOF")) {
				break;
			}

			if (readAhead) {
				String[] split = sweepNumbers(line.trim());
				records.add(new Record(Double.parseDouble(split[1].trim()), Double
						.parseDouble(split[2].trim())));
			}

			if (line.equals("NODE_COORD_SECTION")) {
				readAhead = true;
			}
		}

		br.close();

		final double[][] localMatrix = new double[records.size()][records.size()];

		int rIndex = 0;
		for (Record r : records) {
			int hIndex = 0;
			for (Record h : records) {
				localMatrix[rIndex][hIndex] = calculateEuclidianDistance(r.x, r.y, h.x,
						h.y);
				hIndex++;
			}
			rIndex++;
		}

		return localMatrix;
	}

	private final String[] sweepNumbers(String trim) {
		String[] arr = new String[3];
		int currentIndex = 0;
		for (int i = 0; i < trim.length(); i++) {
			final char c = trim.charAt(i);
			if ((c) != 32) {
				for (int f = i + 1; f < trim.length(); f++) {
					final char x = trim.charAt(f);
					if ((x) == 32) {
						arr[currentIndex] = trim.substring(i, f);
						currentIndex++;
						break;
					} else if (f == trim.length() - 1) {
						arr[currentIndex] = trim.substring(i, trim.length());
						break;
					}
				}
				i = i + arr[currentIndex - 1].length();
			}
		}
		return arr;
	}

	private final double[][] invertMatrix() {
		double[][] local = new double[matrix.length][matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				local[i][j] = invertDouble(matrix[i][j]);
			}
		}
		return local;
	}

	private final double invertDouble(double distance) {
		if (distance == 0d)
			return 0.01;
		else
			return 1.0d / distance;
	}

	private final double calculateEuclidianDistance(double x1, double y1,
			double x2, double y2) {
		final double xDiff = x2 - x1;
		final double yDiff = y2 - y1;
		return Math.abs((Math.sqrt((xDiff * xDiff) + (yDiff * yDiff))));
	}

	public final WalkedWay start() throws InterruptedException, ExecutionException {

		WalkedWay bestDistance = null;

		int agentsSend = 0;
		int agentsDone = 0;
		int agentsWorking = 0;
		for (int agentNumber = 0; agentNumber < NUM_AGENTS; agentNumber++) {
			agentCompletionService.submit(new ACOAgent(this,
					getGaussianDistributionRowIndex(), numgen.nextInt()));
			agentsSend++;
			agentsWorking++;
			while (agentsWorking >= POOL_SIZE) { //each time 4 ants work simultaneously
				WalkedWay way = agentCompletionService.take().get();//wait until the 1st ant finishes its job
				if (bestDistance == null || way.distance < bestDistance.distance) {
					bestDistance = way;
					//					System.out.println("Agent "+ agentsDone +"returned with new best distance of: "
					//							+ way.distance);
					//System.out.println(Arrays.toString(bestDistance.way));

				} 
				System.out.println("" + agentsDone + " cur dist " + way.distance + " best dist " + bestDistance.distance );
				agentsDone++;
				/*if ((agentsDone % phermoneDecayStep) == phermoneDecayStep - 1){
					decayAllPheromones(phermoneDecayStep);
				}*/
				agentsWorking--;
			}
		}
		final int left = agentsSend - agentsDone;
		/*System.out.println("Waiting for " + left
				+ " agents to finish their random walk!");*/

		for (int i = 0; i < left; i++) {
			WalkedWay way = agentCompletionService.take().get();
			if (bestDistance == null || way.distance < bestDistance.distance) {
				bestDistance = way;
				System.out.println("Agent returned with new best distance of: "
						+ way.distance);
			}
		}

		//		THREAD_POOL.shutdown();
		//		THREAD_POOL.awaitTermination(1, TimeUnit.SECONDS);
		System.out.println("Found best so far: " + bestDistance.distance);
		//		System.out.println(Arrays.toString(bestDistance.way));

		return bestDistance;

	}

	private final int getGaussianDistributionRowIndex() {
		//return uniform.nextInt();
		return 0;
	}

	static class Record {
		double x;
		double y;

		public Record(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}
	}

	static class WalkedWay {
		int[] way;
		double distance;

		public WalkedWay(int[] way, double distance) {
			super();
			this.way = way;
			this.distance = distance;
		}
	}

	public static void main(String[] args) throws IOException,
	InterruptedException, ExecutionException {

		if (args.length > 0) {
			ACOsolver.TSP_FILE = args[0];
			System.out.println("Using " + args[0]);
		}

		long start = System.currentTimeMillis();
		ACOsolver antColonyOptimization = new ACOsolver();
		double result = antColonyOptimization.start().distance;
		System.out
		.println("Took: " + (System.currentTimeMillis() - start) + " ms!");
		System.out.println("Result was: " + result);
	}

	public Solution solve( Inputs inputs) throws InterruptedException, ExecutionException {

		WalkedWay bestSol = this.start();
		Solution sol = new Solution();


		// int[] wayReversed = bestSol.way;

		System.out.println("best way:" + Arrays.toString(bestSol.way));

		/*for(int i = 0; i < wayReversed.length / 2; i++)
			{
			    int temp = wayReversed[i];
			    wayReversed[i] = wayReversed[wayReversed.length - i - 1];
			    wayReversed[wayReversed.length - i - 1] = temp;
			}*/

		//System.out.println(Arrays.toString(wayReversed));
		ArrayList<Node> listNodes = new ArrayList<Node>();

		//	for (int i = 0; i <bestSol.way.length - 10; i++){
		for (int i = 0; i <bestSol.way.length ; i++){

			int k = bestSol.way[i];

			if ( k < inputs.getNodes().length && k != 0 ){

				Node iNode = inputs.getNodes()[k];
				listNodes.add(iNode);

			}else if (k >= inputs.getNodes().length +inputs.getVehs()){

				//Node externalNode = new Node(Integer.parseInt("X"), Float.parseFloat("X"),Float.parseFloat("X"), Float.parseFloat("X"));
				Node externalNode = new Node(-1, -1, -1, -1);
				listNodes.add(externalNode);
			}else 
				//if (k >= inputs.getNodes().length || i == bestSol.way.length -1 )

				if (listNodes != null && !listNodes.isEmpty()) {


					Route route = new Route();
					Node firstNode = listNodes.get(0);
					Edge startEdge = new Edge(inputs.getNodes()[0],firstNode);

					startEdge.setCosts(startEdge.calcCosts(inputs.getNodes()[0], firstNode));

					route.getEdges().add(startEdge);
					route.setDemand(route.getDemand() + startEdge.getEnd().getDemand());
					route.setCosts(route.getCosts() + startEdge.getCosts());


					for (int j = 0; j < listNodes.size() -1; j++){
						Node start = listNodes.get(j);
						Node end = listNodes.get(j+1);
						Edge edge = new Edge(start, end);

						edge.setCosts(edge.calcCosts(start, end));

						route.getEdges().add(edge);
						route.setDemand(route.getDemand() + edge.getEnd().getDemand());
						route.setCosts(route.getCosts() + edge.getCosts());
					}

					Node lastNodeStart = listNodes.get(listNodes.size()-1);
					Edge endEdge = new Edge(lastNodeStart, inputs.getNodes()[0]);

					endEdge.setCosts(endEdge.calcCosts(lastNodeStart, inputs.getNodes()[0]));

					route.getEdges().add(endEdge);
					route.setDemand(route.getDemand() + endEdge.getEnd().getDemand());
					route.setCosts(route.getCosts() + endEdge.getCosts());

					sol.getRoutes().add(route);
					sol.setCosts(sol.getCosts() + route.getCosts());
					sol.setDemand(sol.getDemand() + route.getDemand());
					listNodes.clear();

				}


		}


		System.out.println(sol.toString());
		return sol;

	}
}





