package localSearch_ACO_CP;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Random;

import org.uoc.rgcws.Edge;
import org.uoc.rgcws.ElapsedTime;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.Node;
import org.uoc.rgcws.Route;
import org.uoc.rgcws.Solution;
import org.uoc.rgcws.Test;	


import cern.jet.random.engine.MersenneTwister;

public class ACOSolverWithLS_insideCP {


	// greedy
	//public static final double ALPHA = 0.6d for all except hvrp//3 for setp
	public static  double ALPHA =7d; //P&D Ic101 0.5
	// rapid selection
	//public static final double BETA = 5.5d for all except hvrp//3 for setp 
	public static  double BETA =7d; //P&D Ic101 5
	public static final double BETAInitialization = 1d;// 9 is good for SetP

	// heuristic parameters
	//public static final double Q = 100d; // somewhere between 0 and 1......for all except hvrp
	public static final double Q = 1d;//setP 100
	public static final double evaperationRate = 0.001d; // between 0 and 1//P&D Ic101 0.01
	public static final double INITIAL_PHEROMONES = 5d; // can be anything//2d

	public static final double MAX_PHEROMONES = 5d;// INITIAL_PHEROMONES;
	public static  double MIN_PHEROMONES = 1d;

	// use power of 2
	public static final int NUM_AGENTS = 1; //setP 50;

	public final double probBias = 0d;
	public static int[] finalWay;
	public static double finalDis;

	static final int phermoneDecayStep1 = 5 ;//3
	static final int phermoneDecayStep2 = 5 ;//3
	static  int phermoneDecayStep ;//3
	public static final int failurePunishmentRate = 2000;//setP 2000


	final double[][] matrix;
	final double[][] invertedMatrix;

	final double[][] pheromones;
	private final Object[][] mutexes;
	private MersenneTwister numgen;

	Inputs inputs = null;

	int stepToUpdatePher = 0;

	ArrayList<int[]> arrayWays = new ArrayList<int[]>();
	ArrayList<Double>  arrayDis =new ArrayList<Double>();
	WalkedWay bestKnown;

	public  Hashtable<IntArrayWrapper1 ,int []>   theTable = new Hashtable<IntArrayWrapper1 , int []>(60);

	public boolean callEachTwoSteps = false;
	public boolean endInitSol = false;
	public boolean changeCPCallStrategy = false;
	public int countCallCPEachStep =0;
	static int varLS = 0;

	boolean stopChange = false;

	public  ACOSolverWithLS_insideCP (Test aTest, Inputs inputs, int seed) throws IOException {
		this.inputs = inputs;

		matrix = createDistMatrix(inputs.getNodes() , inputs.getVehs());

		invertedMatrix = invertMatrix();
		pheromones = initializePheromones();
		mutexes = initializeMutexObjects();		

		numgen = new cern.jet.random.engine.MersenneTwister(seed);
		//(int) System.currentTimeMillis());
	}	


	private double[][] createDistMatrix(Node[] nodes, int vehNum) {

		//int numVehicles = vehNum-1;// the nodes already have one depot
		int numVehicles = vehNum;
		int matSize = nodes.length + numVehicles; 
		double[][] disMatrix = new double[matSize][matSize];
		//	double sumDistance = 0;		

		for(int i = 0; i < matSize; i++ ){
			for (int j = 0; j < matSize; j++){
				int ri = i;
				int rj = j;
				if (ri >= nodes.length)
					ri = 0;
				if (rj >= nodes.length)
					rj = 0;
				disMatrix[i][j] = calculateEuclidianDistance(nodes[ri].getX(), nodes[ri].getY(), 
						nodes[rj].getX(),nodes[rj].getY());	
				//		sumDistance += disMatrix[i][j]; //For calculate distance of external nodes
				//	System.out.print("  " + disMatrix[i][j]);
			}
			//	System.out.println();
		}


		return disMatrix;
	}

	private final Object[][] initializeMutexObjects() {
		final Object[][] localMatrix = new Object[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = new Object();
			}
		}

		return localMatrix;
	}

	final double readPheromone(int x, int y) {
		return pheromones[x][y];
	}

	final void adjustPheromone(int x, int y, double deltaPheromone) {
		synchronized (mutexes[x][y]) {
			pheromones[x][y] += deltaPheromone;
			//Adding on 11-09-2015
			if (pheromones[x][y] > MAX_PHEROMONES){
				pheromones[x][y] = MAX_PHEROMONES;
			}
			//	System.out.println (x+" " + y + " " +pheromones[x][y]);
		}
	}


	void decayAllPheromones(int times) {
		double mutliplier = Math.pow((1 - evaperationRate),times);
		for (int x = 0; x < mutexes.length; x++){
			for (int y = 0; y < mutexes.length; y++){
				synchronized (mutexes[x][y]) {
					pheromones[x][y] *= mutliplier;
					//Adding on 11-09-2015
					if (pheromones[x][y] < MIN_PHEROMONES){
						pheromones[x][y] = MIN_PHEROMONES;
						//	System.out.println("Lower Bound");
					}
				}
			}
		}
	}


	private final double[][] initializePheromones() {
		final double[][] localMatrix = new double[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = INITIAL_PHEROMONES;
			}
		}

		return localMatrix;
	}




	final double[][] invertMatrix() {
		double[][] local = new double[matrix.length][matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				local[i][j] = invertDouble(matrix[i][j]);
			}
		}
		return local;
	}

	private final double invertDouble(double distance) {
		if (distance == 0d)
			return 0.01;
		else
			return 1.0d / distance;
	}

	private final double calculateEuclidianDistance(double x1, double y1,
			double x2, double y2) {
		final double xDiff = x2 - x1;
		final double yDiff = y2 - y1;
		return Math.abs((Math.sqrt((xDiff * xDiff) + (yDiff * yDiff))));
	}

	public final WalkedWay start() throws Exception {

		WalkedWay bestWalkedWay = null;

		for (int agentNumber = 0; agentNumber < NUM_AGENTS; agentNumber++) {

			WalkedWay route = (new AntFunctionWithLS(this, 0, numgen.nextInt()).findWay());


			if (bestWalkedWay == null || route.distance < bestWalkedWay.distance) {
				bestWalkedWay = route;

			} 


			System.out.println("" + agentNumber + " cur dist " + route.distance + 
					" best dist " + bestWalkedWay.distance );


			System.out.println ("==============");
			if (agentNumber % phermoneDecayStep1 == 0){

				System.out.println("We are Updating the bestKnown Solution!" + "                 " + 
						bestWalkedWay.distance);			

				System.out.println ("==============");


				double phero = (Q / (bestWalkedWay.distance));

				int previousBest = bestWalkedWay.way[0];			

				for(int h = 1; h < bestWalkedWay.way.length; h++){  				

					adjustPheromone(previousBest, bestWalkedWay.way[h], phero);
					previousBest = bestWalkedWay.way[h];
				}
			}



		}	
		//-----------------------------------------------------------------------
		// Call CP to apply Local Search
		//-----------------------------------------------------------------------
		System.out.print("OLD Way:  ");
		for (int w = 0; w < bestWalkedWay.way.length; w++){
			System.out.print(bestWalkedWay.way[w] + ", ");
		}
		System.out.println();
		varLS = 1;
		AntFunctionWithLS localAnt = new AntFunctionWithLS(this, 0, numgen.nextInt());
		int alterVar = 0;

		while (!stopChange){
			int[] tmpWay = Arrays.copyOf(bestWalkedWay.way, bestWalkedWay.way.length);
			int[] incompleteWay = specifyCenters( inputs, tmpWay, alterVar);

			System.out.print("New Incomplete Way  : ");
			for (int h = 0; h < incompleteWay.length; h++){
				System.out.print(incompleteWay[h] + " ");			
			}
			System.out.println();

			System.out.println("------------------------");

			ArrayList<ArrayList<Integer>> variablesToCheck2 = 
					AntFunctionWithLS.cpCall.getVariablesArrayList
					(localAnt.createSucPredEsp(incompleteWay, inputs.getNodes().length, inputs.getVehs()));

			LinkedList<LinkedList<Integer>> res2 = AntFunctionWithLS.cpCall.checkEsp(variablesToCheck2,
					9999, inputs.getVehs(), varLS);
			int[] tmpRes = convertCPresults (res2, inputs.getNodes().length-1, inputs.getVehs());
			int[] givenWayByCP = new int[tmpRes.length+1];
			for (int i =0; i < tmpRes.length; i++){
				givenWayByCP[i+1] =tmpRes[i];
			}
			
			

			System.out.print("NEW Way:  ");
			double distanceWalked = 0;
			for (int w = 0; w < givenWayByCP.length-1; w++){
				System.out.print(givenWayByCP[w] + ", ");
				distanceWalked  += matrix[givenWayByCP[w]][givenWayByCP[w+1]];
			}
			System.out.println();
			
			System.out.println("bestWalkedWay:   " + distanceWalked);


			WalkedWay finalSol = new WalkedWay(Arrays.copyOf(givenWayByCP, givenWayByCP.length), distanceWalked);
			if (finalSol.distance < bestWalkedWay.distance){
				bestWalkedWay = finalSol;
			}
			alterVar++;

		}
		//	return bestWalkedWay;
		System.out.println("New bestWalkedWay:   " + bestWalkedWay.distance);
		return bestWalkedWay;

	}





	/*	private final int getGaussianDistributionRowIndex() {
		//return uniform.nextInt();
		return 0;
	}*/



	static class WalkedWay {
		int[] way;
		double distance;

		public WalkedWay(int[] way, double distance) {
			super();
			this.way = way;
			this.distance = distance;
		}
	}



	public Solution solve( Inputs inputs) throws Exception {

		WalkedWay bestSol = this.start();
		Solution sol = new Solution();


		if (bestSol.way!=null){

			finalWay = bestSol.way;
			finalDis = bestSol.distance;

			try(PrintWriter out3 = new PrintWriter(new BufferedWriter				
					(new FileWriter("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\gen\\localSearch\\cvrp\\results\\sol.txt", true)))) {
				out3.println("best way:" + finalWay.toString()+ "    "+
						finalDis+  "\r\n"+ CVRP.aTest.getInstanceName());
				for (int node: finalWay){

					out3.print(node + ", ");
				}

				out3.println("\r");
				out3.println("-----------------------------------------------------------------");

				out3.close();
			}catch (IOException e) {
				System.err.println(e);
			}


			ArrayList<Node> listNodes = new ArrayList<Node>();

			for (int i = 0; i < finalWay.length ; i++){

				int k = finalWay[i];

				if ( k < inputs.getNodes().length && k != 0 && 
						k!= finalWay[finalWay.length-1] ){

					Node iNode = inputs.getNodes()[k];
					listNodes.add(iNode);

				} else

					if (listNodes != null && !listNodes.isEmpty()) {

						Node lastEgdeStart;
						Route route = new Route();
						Node firstNode = listNodes.get(0);
						Edge startEdge = new Edge(inputs.getNodes()[0],firstNode);

						startEdge.setCosts(startEdge.calcCosts(inputs.getNodes()[0], firstNode));

						route.getEdges().add(startEdge);
						route.setDemand(route.getDemand() + startEdge.getEnd().getDemand());
						route.setCosts(route.getCosts() + startEdge.getCosts());


						for (int j = 0; j < listNodes.size() -1; j++){
							Node start = listNodes.get(j);
							Node end = listNodes.get(j+1);
							Edge edge = new Edge(start, end);

							edge.setCosts(edge.calcCosts(start, end));

							route.getEdges().add(edge);
							route.setDemand(route.getDemand() + edge.getEnd().getDemand());
							route.setCosts(route.getCosts() + edge.getCosts());
						}


						lastEgdeStart = listNodes.get(listNodes.size()-1);
						Edge lastEdge = new Edge(lastEgdeStart, inputs.getNodes()[0]);

						lastEdge.setCosts(lastEdge.calcCosts(lastEgdeStart, inputs.getNodes()[0]));

						route.getEdges().add(lastEdge);
						route.setDemand(route.getDemand() + lastEdge.getEnd().getDemand());
						route.setCosts(route.getCosts() + lastEdge.getCosts());

						sol.getRoutes().add(route);
						sol.setCosts(sol.getCosts() + route.getCosts());
						sol.setDemand(sol.getDemand() + route.getDemand());
						listNodes.clear();


					} else{


					}

			}

			System.out.println(finalDis);



			return sol;

		}
		return null;
	}	

	public double calcCostsAVRP(Node origin, Node end)
	{   double X1 = origin.getX();
	double Y1 = origin.getY();
	double X2 = end.getX();
	double Y2 = end.getY();
	double d = Math.sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1));
	//return Math.round(d); // OJO!
	return (d);
	}

	int[] convertCPresults (LinkedList<LinkedList<Integer>> cpResult, int numCus, int numVeh)
	{
		LinkedList<LinkedList<Integer>> results = new LinkedList<LinkedList<Integer>>();
		results = cpResult;
		LinkedList<Integer> v = new LinkedList<Integer>();
		v = results.get(0);
		LinkedList<Integer> p = new LinkedList<Integer>();
		p = results.get(1);
		LinkedList<Integer> successor = new LinkedList<Integer>();
		successor = results.get(2);


		System.out.print("Succ:  ");
		for (int i =0; i < successor.size(); i++){
			System.out.print(successor.get(i)+ ", ");
		}


		System.out.println();
		int[] way = new int[numCus+numVeh];
		int w = 0;

		for (int i =0; i < numVeh; i++){
			int tmp = successor.get(numCus+i);
			while (tmp <= numCus){
				way[w] = tmp;
				tmp = successor.get(tmp-1);
				w++;
			}
			way[w] = tmp;
			w++;
		}
		return way;


	}	


	public ArrayList<int[]> createSucPredLocalSearch(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){ //if it is not depot or zero

				//	visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else //if (i-1!=0)
				{
					//	predecessors[parWay[i]] = numNodes+k;
					//	succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				}// else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
				else if (parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					//	k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					//	assert (parWay[i+1] == 0);
					//	succesors[parWay[i]] = 0;
				}

			}if (parWay[i] != 0){
				k += 1; //use next vehicle
			}
		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	} 



	public int[] specifyCenters( Inputs inputs, int[] tmpWay, int order) {

		int[] localWay = Arrays.copyOf(tmpWay, tmpWay.length);


		if (localWay!=null){

			ArrayList<Integer> listOfNodesInaTrip = new ArrayList<Integer>();
			ArrayList<ArrayList<Integer>> tripForVeh = new ArrayList<ArrayList<Integer>>();

			double sumX = 0;
			double sumY = 0;		
			double meanX = 0;
			double meanY = 0;
			ArrayList<Point> centers = new ArrayList<Point>();	

			for (int i = 0; i < localWay.length ; i++){

				int k =  localWay[i];

				if ( k < inputs.getNodes().length && k != 0 ){


					listOfNodesInaTrip.add(k);

				} else

					if (listOfNodesInaTrip != null && !listOfNodesInaTrip.isEmpty()) {						

						for (int j = 0; j < listOfNodesInaTrip.size(); j++){						

							//-----------------------
							int idNode = listOfNodesInaTrip.get(j);
							Node node = inputs.getNodes()[idNode];
							sumX += node.getX();
							sumY += node.getY();;
							//-----------------------
						}

						listOfNodesInaTrip.add(k);

						sumX += inputs.getNodes()[0].getX();
						sumY += inputs.getNodes()[0].getY();						 

						ArrayList<Integer> clonedList = new ArrayList<Integer>();
						clonedList.addAll(listOfNodesInaTrip);
						tripForVeh.add(clonedList);

						// ---------------------------------------------
						// call getCentroidOfNodes
						// ---------------------------------------------
						meanX = (sumX / (listOfNodesInaTrip.size()));
						meanY = (sumY / (listOfNodesInaTrip.size()));
						Point ctr = new Point((int) meanX,(int) meanY);
						centers.add(ctr); 

						listOfNodesInaTrip.clear();


					} else{


					}
			}

			ArrayList<RecordCent> distCenters = new ArrayList<RecordCent>();

			// Calculate distance between centers
			for(int firstCtr = 0; firstCtr < centers.size()-1; firstCtr++ ){

				for (int seconCtr = firstCtr+1; seconCtr < centers.size(); seconCtr++){			
					double dist = calculateEuclidianDistance(centers.get(firstCtr).x, centers.get(firstCtr).y ,
							centers.get(seconCtr).x, centers.get(seconCtr).y);
					RecordCent r1 = new RecordCent(firstCtr, seconCtr, dist);
					distCenters.add(r1);
				}				

			}

			for (int element = 0; element < distCenters.size(); element++){
				System.out.println("Pair:  " + distCenters.get(element).x + " " + distCenters.get(element).y +
						" " + distCenters.get(element).dist);
			}

			//Specify a pair with min dist
			Collections.sort(distCenters);

			//Remove trips with min dist between their centers
			RecordCent candidateTrips = distCenters.get(order);
			if (order == distCenters.size()-1){
				stopChange = true;
			}
			/*ArrayList<Integer> firstCandTrip = tripForVeh.get(candidateTrips.x); 

			for (int j1 = 0; j1 < firstCandTrip.size(); j1++){
				firstCandTrip.set(j1, 0);
			}*/

			ArrayList<Integer> secondCandTrip = tripForVeh.get(candidateTrips.y); 
			for (int j1 = 0; j1 < secondCandTrip.size(); j1++){
				secondCandTrip.set(j1, 0);
			}

			int i = 1;

			for (ArrayList<Integer> trip : tripForVeh){

				for (int nodeInTrip : trip){

					localWay[i] = nodeInTrip;
					i++;
				}
			}

			return localWay;

		}
		return null;
	}	


}
class IntArrayWrapper1{
	int [] theArray = null;
	IntArrayWrapper1(int [] _theArray){
		//theArray =  _theArray.clone();
		theArray =  _theArray;
	}

	@Override
	public boolean equals(Object o){

		//	IntArrayWrapper a = (IntArrayWrapper) o;

		if ( (o instanceof IntArrayWrapper1) 	&&
				java.util.Arrays.equals( ((IntArrayWrapper1) o ).theArray, theArray)
				)  
		{
			return true;
		} else {
			return false;
		}
	}



	@Override public int hashCode() {
		//System.out.println(java.util.Arrays.hashCode(theArray));
		return (java.util.Arrays.hashCode(theArray) );
	}

}

class RecordCent implements Comparable<RecordCent> {
	int x;
	int y;
	double dist;

	public RecordCent(int x, int y, double dist) {

		this.x = x;
		this.y = y;
		this.dist = dist;
	}

	public int compareTo(RecordCent record){
		double compareage = record.dist;
		/* For Ascending order*/
		return  (int)(this.dist-compareage);
	}
}






