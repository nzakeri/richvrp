package localSearch_ACO_CP;
//import java.io.IOException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
//import java.util.concurrent.Callable;

import eclipse.CheckCP_everyTwoSteps;
import localSearch_ACO_CP.ACOSolverGlobalUpdate.WalkedWay;




// public final class AntFunctionGlobalUpdate implements Callable<WalkedWay> {

public final class AntFunctionGlobalUpdate2  {
	public final ACOSolverGlobalUpdate instance;
	private double distanceWalked = 0.0d;
	private final int start;
	//	public  boolean[] visited;
	public  int[] way;
	public int toVisit;
	private Random random = null;
	float totalCapacity = 0;
	//	private boolean routeNotPossible = false;
	int goalPoint;
	int stepToCheck;
	//	private float[] distanceWalkedArr;
	//	private int groupAgent;
	//	private int[] antWay;
	private Double groupCost = 0.0;
	int backtrackNode;
	//	private boolean callEachTwoSteps;
	private int lineNum = 1;


	public static CheckCP_everyTwoSteps cpCaller;

	public AntFunctionGlobalUpdate2(ACOSolverGlobalUpdate acoSolverGlobalUpdate2, int start,int seed) {
		//	super(); //call its father's constructor
		this.instance = acoSolverGlobalUpdate2;

		this.start = start;

		//this.way = new int[visited.length];
		this.way = new int[acoSolverGlobalUpdate2.matrix.length];

		random = new Random(seed);
		//System.out.println("seed is "+seed);
		goalPoint = (this.instance.inputs.getNodes().length)+ (this.instance.inputs.getVehs()/2);
		stepToCheck = 5;//(instance.matrix.length)/10;


	}

	public final Point getNextProbableNode(int[] possibleList, int idCurNode) {


		double sum = 0.0d;			

		for (int j = 0; j < possibleList.length; j++){

			if (((j % 2) ==0) && possibleList[j] >= instance.inputs.getNodes().length){
				backtrackNode = idCurNode;
			}
		}


		int numberOFpairs = (possibleList.length/2);
		final double[] weights = new double[numberOFpairs];
		int k = 0;

		for (int i = 0; i <possibleList.length; i++){			

			double weightpart1 = calculateNonNormalizedProbability(possibleList[i], way[idCurNode]);				
			double weightpart2 = calculateNonNormalizedProbability(possibleList[i], possibleList[i+1]);
			weights[k] = weightpart1 * weightpart2;
			sum += weights[k];	
			k+=1;
			i+=1;

		}

		if (sum == 0.0d)
			return  null;		

		double probSum = 0.0d;
		double curRand = random.nextDouble();
		final double r = curRand*sum;

		for (int i = 0; i < weights.length; i++) {

			if (weights [i] != 0 ) {
				probSum +=weights[i];

				if (r <= probSum) {

					Point probNode = new Point(possibleList[(i*2)], possibleList[((i*2)+1)]);

					return probNode;

				}
			}
		}



		return  null;
	}


	public final double calculateNonNormalizedProbability(int row, int column) {
		final double p = Math.pow(instance.readPheromone(column, row),
				ACOSolverGlobalUpdate.ALPHA)
				* Math.pow(instance.invertedMatrix[column][row],
						ACOSolverGlobalUpdate.BETA);

		return (p + this.instance.probBias);
	}


	//	public final WalkedWay call() throws Exception { 

	@SuppressWarnings("unused")
	public  WalkedWay findWay()  { 


		int nextDepot = instance.inputs.getNodes().length;
		int maxNodePos = instance.inputs.getNodes().length + instance.inputs.getVehs()-1;	

		int i = 1;		

		for (; i <= maxNodePos; i++ ){

			int idcurNode = i-1;
			int requiredNode = way[idcurNode];

			//	System.out.println("NEXT Depot:  " + nextDepot);
			if (way[idcurNode] >= instance.inputs.getNodes().length){

				requiredNode = nextDepot;

			}

			if (idcurNode==0){
				requiredNode = instance.inputs.getNodes().length;
			}

			/*if (instance.endInitSol == true && instance.countCallCPEachStep >= ACOSolverGlobalUpdate.NUM_AGENTS){
				//	if (instance.endInitSol == true && instance.countCallCPEachStep >= 100){
				instance.callEachTwoSteps = true;
				instance.changeCPCallStrategy = true;
				ACOSolverGlobalUpdate.ALPHA =1d;
				ACOSolverGlobalUpdate.BETA =5d;
				System.out.println(" ALPHA:  " + ACOSolverGlobalUpdate.ALPHA);
				System.out.println(" BETA:  " + ACOSolverGlobalUpdate.BETA);

			}*/

			int curVehicle = nextDepot - instance.inputs.getNodes().length +1;
			Point idNextNodes;

			//*----------------------------------------------------*
			//-------------------- Using Hashmap ------------------
			//*----------------------------------------------------*

			if(instance.theTable.containsKey (new IntArrayWrapper(way))){
				//	ArrayList<Integer>  probableSucList = new ArrayList<Integer>(instance.theTable.get(new IntArrayWrapper(way)).size());
				int[]  probableSucList = new int[instance.theTable.get(new IntArrayWrapper(way)).length];
				probableSucList= instance.theTable.get(new IntArrayWrapper(way)); 
				System.out.println("!!!!!!!!!!!!!" + "FINALLY!!!");

				idNextNodes = getNextProbableNode(probableSucList, idcurNode);

			}
			else {				
				ArrayList<ArrayList<Integer>> variablesToCheck = 
						cpCaller.getVariablesArrayList
						(createSucPredEsp(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));

				LinkedList<LinkedList<Integer>> myAL = cpCaller.checkEsp(variablesToCheck, requiredNode, curVehicle);

				int[]  probableSucList = new int[(myAL.size()*2)];
				int elementProSucList =0;
				for (LinkedList<Integer> test : myAL){

					probableSucList[elementProSucList] = test.getFirst();
					probableSucList[elementProSucList+1] = test.getLast();
					elementProSucList+=2;

				}

				int[] copyOFtheWay = way.clone();
				instance.theTable.put(new IntArrayWrapper(copyOFtheWay), probableSucList);
				// System.out.println(instance.theTable.get(new IntArrayWrapper(way)));
				System.out.println("Insert inside The table!    " + lineNum );
				lineNum+=1;

				idNextNodes = getNextProbableNode(probableSucList, idcurNode);
			}

			if (idNextNodes != null){

				if (idNextNodes.x < instance.inputs.getNodes().length){

					way[i] = idNextNodes.x;
				}else{
					way[i] = nextDepot;
					nextDepot++;
				}
				if (way[i] == maxNodePos){

					break;
				}

				if (instance.callEachTwoSteps == true && instance.changeCPCallStrategy == true){
					System.out.println(" EACH 2 STEPS ");						

					if (i+1<= maxNodePos  && idNextNodes.y != 0){

						if (idNextNodes.y < instance.inputs.getNodes().length){

							way[i+1] = idNextNodes.y;
						}else{
							way[i+1] = nextDepot;
							nextDepot++;
						}

						i++;

					}
				}
			}
			else {

				for(int j =i; j > backtrackNode; j-- ){
					way[j] = 0;
				}


				i = backtrackNode+1;

				way[i] = nextDepot;
				//	visited[nextDepot] = true;
				backtrackNode = i;

				nextDepot++;

			}


			if ( way[i] == maxNodePos || nextDepot > maxNodePos){
				//	i++;
				break;
			}

		}


		int nMissed = maxNodePos - i;
		float maxDistance = 100000000;//1000000;

		for (int k=0; k<i; k++){
			distanceWalked += instance.matrix[way[k]][way[k+1]];
		}
		double a =0;
		a = instance.matrix[way[way.length-1]][0];
		distanceWalked += a ;
		if (nMissed > 0){
			distanceWalked += maxDistance*(nMissed);

		}
		else {

			System.out.println("FirstdistanceWalked:   " + distanceWalked);
			//----------------------------------------------------------------
			//local search
			//----------------------------------------------------------------
			int [] bestPermuteWay = new int [way.length];
			double bestPermutedist = 9999999999.99999999;
		//	int [] way1 = {3,2,5,6};
			/*for (int ele : way){
			System.out.print(ele + "  ");
			}*/
		//	int startingIndex = (2*(way.length)) / 3;
			int startingIndex = new Random().nextInt(way.length-1)+1;	
			int maximum = way.length  - startingIndex;
			int endIndex = 0;
			if (maximum < 5){
			 endIndex = new Random().nextInt(way.length  - startingIndex) + startingIndex;
			}
			else {
				 endIndex =  startingIndex +5;
			}
		//	int randomNum = rand.nextInt((max - min) + 1) + min;
			System.out.println(startingIndex+ "  " + endIndex);
		//	int[] newPartWay = Arrays.copyOfRange(way, startingIndex, way.length);
			int[] newPartWay = Arrays.copyOfRange(way, startingIndex, endIndex);
			ArrayList<ArrayList<Integer>> out = permute((newPartWay));
			for (ArrayList<Integer> b : out) {

				int temp = startingIndex;
				for (int j = 0; j < b.size(); j++){
		//			System.out.print(b.get(j));					

					way[temp] = b.get(j);
				//	System.out.print(way[temp]);
					temp++;
				}
			//	System.out.println();
				/*for (int ele : way){
					System.out.print(ele + "  ");
					}*/
				double distPerWay = 0.0d;
				for (int k=0; k<way.length-1; k++){
					distPerWay += instance.matrix[way[k]][way[k+1]];
				}
				double lastNode =0;
				lastNode = instance.matrix[way[way.length-1]][0];
				distPerWay += lastNode ;
				

				//			run full cp
				ArrayList<ArrayList<Integer>> variablesToCheck2 = 
						cpCaller.getVariablesArrayList(createSucPredCompleteSol(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));

				boolean res2 = cpCaller.check(variablesToCheck2, 9999, instance.inputs.getVehs());

				if (!res2){
					distPerWay += maxDistance*(1);
		//			System.out.println("THE ROUTE IS NOT FEASIBLE");

				} else {
					instance.endInitSol = true;
					instance.countCallCPEachStep ++;
				}
		//		System.out.println("distPerWay:   " + distPerWay);
				
				if (distPerWay < bestPermutedist){
				bestPermuteWay = way;
				bestPermutedist = distPerWay;
				
		//		System.out.println("bestPermutedist:   " + bestPermutedist);
				
				}
				
				
			}
			
			way = bestPermuteWay;
			distanceWalked = bestPermutedist;
			System.out.println("BestdistanceWalked:   " + distanceWalked);


		}





		/*try {

			CVRP.writeAveCost.write("Variable To Check2: " + variablesToCheck2+ "\r\n" );
			CVRP.writeAveCost.write("distanceCost:  " + distanceWalked+ "\r\n" );

			CVRP.writeAveCost.write("\r\n ------------------------------------------------- \r\n");

			CVRP.writeAveCost.flush(); 

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
	

	//----------------------------------------------------------------

	instance.arrayWays.add(way);
	instance.arrayDis.add(distanceWalked);
	instance.stepToUpdatePher +=1;
	if (instance.stepToUpdatePher >= ACOSolverGlobalUpdate.phermoneDecayStep) //New added
	{

		double minDisInGroup = 9999999999.99999999d;
		double minPheremone = 0;
		int minID = 0;
		int numberOfChanges  = 0;

		System.out.println ("DECAY STEP");
		instance.decayAllPheromones(ACOSolverGlobalUpdate.phermoneDecayStep);

		System.out.println("!!!!PHEROMONE UPDATING PROCESS!!!!");
		for(int h3=0; h3 < instance.stepToUpdatePher; h3++){

			double phero = (ACOSolverGlobalUpdate.Q / (instance.arrayDis.get(h3)));//calculate pheromone for each ant in the group

			if (instance.arrayDis.get(h3) < minDisInGroup){
				minDisInGroup = instance.arrayDis.get(h3);
				minID  = h3;
				minPheremone = phero;

				numberOfChanges += 1;				


				System.out.println("HASSAAAAAAAAAAANNNNNNNNNNNNNNN");

			}


		}		
		System.out.println("Updating the Min Dis In This Group: " + "    " + minDisInGroup);


		int previous = instance.arrayWays.get(minID)[0]; //update pheromone for a solution found by the best ant in this group
		for(int h = 1; h < instance.arrayWays.get(minID).length; h++){  
			instance.adjustPheromone(previous, instance.arrayWays.get(minID)[h], (minPheremone));
			previous = instance.arrayWays.get(minID)[h];
		}


		/*System.out.println("We are Updating the bestKnown Solution!" + "                 " + 
				instance.bestKnown.distance);			

		System.out.println ("==============");*/


		/*double phero = (ACOSolverGlobalUpdate.Q / (instance.bestKnown.distance));

		int previousBest = instance.bestKnown.way[0];			

		for(int h = 1; h < instance.bestKnown.way.length; h++){  				

			instance.adjustPheromone(previousBest, instance.bestKnown.way[h], phero);
			previousBest = instance.bestKnown.way[h];
		}*/


		groupCost = 0.0;
		instance.stepToUpdatePher = 0;
		instance.arrayWays.clear();
		instance.arrayDis.clear();


	}

	return new WalkedWay(way,distanceWalked);
}

	public ArrayList<int[]> createSucPredEsp(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else //if (i-1!=0)
				{
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				}// else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
				else if (parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}
		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}

public ArrayList<int[]> createSucPredCompleteSol(int[] parWay, int numNodes, int numVehicles) {
	ArrayList<int[]> variablesLists = new ArrayList<int[]>();
	int succesors[] = new int[numNodes + numVehicles];
	int predecessors[] = new int[numNodes + numVehicles];
	int visits[] = new int[numNodes + (numVehicles * 2)];
	int k =0;

	for (int i = 0; i <parWay.length; i++){

		if (parWay[i] != 0 && parWay [i] < numNodes && k < numVehicles){

			visits[parWay[i]] = k +1;
			if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
				predecessors[parWay[i]] = parWay[i-1];
			}else {
				predecessors[parWay[i]] = numNodes+k;
				succesors[numNodes + k] = parWay[i];
			}
			if (i+1 < parWay.length && parWay[i+1] != 0 && parWay[i+1]< numNodes){
				succesors[parWay[i]] = parWay[i+1];        			
			} //else if(i+1 == parWay.length || parWay[i+1] != 0){
			else if(i+1 < parWay.length && parWay[i+1] != 0 ){ // we have reached a depot ()
				succesors[parWay[i]] = numNodes+k;
				predecessors[numNodes+k] = parWay[i];
				k += 1;
			}
			else {
				//parWay[i+1] == 0 this is the end of the way
			//	assert (parWay[i+1] == 0);
			//	succesors[parWay[i]] = 0;
			}

		}

	}

	for (int j = 0; j < numVehicles; j++) {
		visits[numNodes + j] = j + 1;
		visits[numNodes + numVehicles + j] = j + 1;
		if (succesors[numNodes + j] == 0)
		{				
			succesors[numNodes + j] = numNodes + j;
		}
		if (predecessors[numNodes + j] == 0)
		{				
			predecessors[numNodes + j] = numNodes + j;
		}
	}




	variablesLists.add(0, visits);
	variablesLists.add(1, predecessors);
	variablesLists.add(2, succesors);

	return variablesLists;
}


public  ArrayList<ArrayList<Integer>> permute(int[] num) {
	ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

	//start from an empty list
	result.add(new ArrayList<Integer>());

	for (int i = 0; i < num.length; i++) {
		//list of list in current iteration of the array num
		ArrayList<ArrayList<Integer>> current = new ArrayList<ArrayList<Integer>>();

		for (ArrayList<Integer> l : result) {
			// # of locations to insert is largest index + 1
			for (int j = 0; j < l.size()+1; j++) {
				// + add num[i] to different locations
				l.add(j, num[i]);

				ArrayList<Integer> temp = new ArrayList<Integer>(l);
				current.add(temp);

				//System.out.println(temp);

				// - remove num[i] add
				l.remove(j);
			}
		}

		result = new ArrayList<ArrayList<Integer>>(current);
	}

	return result;
}

}








class Point{
	public int x = 0;
	public int y = 0;

	//constructor
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}






