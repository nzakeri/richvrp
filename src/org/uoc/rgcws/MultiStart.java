package org.uoc.rgcws;

import java.util.Random;

/**
 * Iteratively calls the RandCWS and saves the best solution.
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class MultiStart 
{
    /* 0. Instance fields and class constructor */
    private Test aTest;
    private Inputs inputs; // contains savingsList too
    private Random rng;
    private Solution cwsSol = null;
    private Solution bestSol = null;
    private Solution newSol = null;
    private Outputs outputs = new Outputs();
        
    MultiStart(Test myTest, Inputs myInputs, Random myRng)
    {
        aTest = myTest;
        inputs = myInputs;
        rng = myRng;
    }
    
    public Outputs solve()
    {
        /* 1. Generates the CWS solution */
        long start = ElapsedTime.systemTime();
        cwsSol = RandCWS.solve(aTest, inputs, rng, false);
        double elapsed = ElapsedTime.calcElapsed(start, ElapsedTime.systemTime());
        cwsSol.setTime(elapsed);
        System.out.println("CWS sol cost: " + cwsSol.getCosts());
        System.out.println("CWS sol demand: " + cwsSol.getDemand());
        System.out.println("CWS sol time: " + cwsSol.getTime());
        bestSol = cwsSol;
        outputs.setCWSSol(cwsSol);
        /* 3. Iterates calls to RandCWS */
        start = ElapsedTime.systemTime();
        elapsed = 0.0;
        while( elapsed < aTest.getMaxTime() )
        {
            newSol = RandCWS.solve(aTest, inputs, rng, true);
            if( newSol.getCosts() < bestSol.getCosts() ) 
            {
                bestSol = newSol;
                System.out.println("bestSol cost: " + newSol.getCosts());
            }
            elapsed = ElapsedTime.calcElapsed(start, ElapsedTime.systemTime());
        }
        System.out.println("OBSol cost: " + bestSol.getCosts());
        System.out.println("OBSol demand: " + bestSol.getDemand());
        System.out.println("OBSol time: " + bestSol.getTime());
        System.out.println("OBSol routes: " + bestSol.getRoutes().size());
        outputs.setOBSol(bestSol);
        /* 4. Returns the CWS sol. and the best-found sol. */
        return outputs;
    }
}
