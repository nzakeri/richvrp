package org.uoc.rgcws;

import java.util.LinkedList;

/**
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class Inputs
{
    /* INSTANCE FIELDS & CONSTRUCTOR */
    private Node[] nodes; // List of all nodes in the problem/sub-problem
    private float vCap = 0.0F; // Vehicle capacity (homogeneous fleet)
    private int vehs = 0; // Vehicle capacity (homogeneous fleet)
    private LinkedList<Edge> savings = null; 
    private float[] vrpCenter; // (x-bar, y-bar) is a geometric VRP center
   
    public Inputs(int n)
    {   nodes = new Node[n]; // n nodes, including the depot
        vrpCenter = new float[2];
    }

    /* GET METHODS */
    public Node[] getNodes(){return nodes;}
    public LinkedList<Edge> getSavings(){return savings;}
    public float getVehCap(){return vCap;}
    public int getVehs(){return vehs;}
    public float[] getVrpCenter(){return vrpCenter;}

    /* SET METHODS */
    public void setVrpCenter(float[] center){vrpCenter = center;}
    public void setVehCap(float c){vCap = c;}
    public void setVehs(int c){vehs = c;}
    public void setList(LinkedList<Edge> sList){savings = sList;}
}