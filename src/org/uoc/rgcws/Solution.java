package org.uoc.rgcws;


import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @author Angel A. Juan - ajuanp(
 * @)gmail.com
 * @version 130112
 */
public class Solution {
    /* INSTANCE FIELDS & CONSTRUCTOR */

    private static long nInstances = 0; // number of instances
    private long id; // solution ID
    private double costs = 0.0; // solution costs
    private float demand = 0.0F; // total demand
    private LinkedList<Route> routes; // list of routes in this solution
    private double time = 0.0; // elapsed computational time (in seconds)

    public Solution() {
        nInstances++;
        id = nInstances;
        routes = new LinkedList<Route>();
    }

    /* GET METHODS */
    public LinkedList<Route> getRoutes() {
        return routes;
    }

    public long getId() {
        return id;
    }

    public double getCosts() {
        return costs;
    }

    public float getDemand() {
        return demand;
    }

    public double getTime() {
        return time;
    }

    /* SET METHODS */
    public void setCosts(double c) {
        costs = c;
    }

    public void setDemand(float d) {
        demand = d;
    }

    public void setTime(double t) {
        time = t;
    }

    /*  AUXILIARY METHODS */
    @Override
    public String toString() {
        Route aRoute; // auxiliary Route variable
        String s = "";
        s = s.concat("\r\n");
        s = s.concat("Sol ID : " + getId() + "\r\n");
        s = s.concat("Sol costs: " + getCosts() + "\r\n");
        s = s.concat("# of routes in sol: " + routes.size());
        s = s.concat("\r\n\r\n\r\n");
        s = s.concat("List of routes (cost and nodes): \r\n\r\n");
        for (int i = 1; i <= routes.size(); i++) {
            aRoute = routes.get(i - 1);
            s = s.concat("Route " + i + " || ");
            s = s.concat("Costs = " + aRoute.getCosts() + " || ");
            s = s.concat("Demand  = " + aRoute.getDemand() + " || ");
            s = s.concat("\r\n");
        }
        s = s.concat("\r\n\r\n");
        return s;
    }

    public ArrayList<int[]> createSucPred(int numNodes, int numVehicles) {
        ArrayList<int[]> variablesLists = new ArrayList<int[]>();
        int succesors[] = new int[numNodes + numVehicles];
        int predecessors[] = new int[numNodes + numVehicles];
        int visits[] = new int[numNodes + (numVehicles * 2)];

        for (int i = 0; i < numVehicles; i++) {

            if (i < routes.size()) {

                Route iRoute = routes.get(i);

                // depot - first node
                succesors[numNodes + i] = iRoute.getEdges().get(0).getEnd().getId(); // +
                // 1;

                predecessors[numNodes + i] = iRoute
                        .getEdges().get(iRoute.getEdges().size() - 1).getOrigin()
                        .getId(); // + 1;

                for (int j = 0; j < iRoute.getEdges().size() - 2; j++) {
                    succesors[iRoute.getEdges().get(j).getEnd().getId()] = iRoute
                            .getEdges().get(j + 1).getEnd().getId(); // + 1;
                }
                for (int j = iRoute.getEdges().size() - 2; j > 0; j--) {
                    predecessors[iRoute.getEdges().get(j).getEnd().getId()] = iRoute
                            .getEdges().get(j - 1).getEnd().getId(); // + 1;
                }
                
                for (int z = 0; z < iRoute.getEdges().size() - 1; z++) {
                	visits[iRoute.getEdges().get(z).getEnd().getId()] = i + 1;
                }

                succesors[iRoute.getEdges().get(iRoute.getEdges().size() - 2).getEnd()
                        .getId()] = numNodes + i;
                
                predecessors[iRoute.getEdges().get(0).getEnd().getId()] = numNodes + i;

            } else {
                succesors[numNodes + i] = numNodes + i ;
                predecessors[numNodes + i] = numNodes + i ;
            }

        }

        for (int i = 0; i < numVehicles; i++) {
            visits[numNodes + i] = i + 1;
            visits[numNodes + numVehicles + i] = i + 1;
        }
        System.out.print(" visits: ");
        for (int vis : visits){
        	 System.out.print( vis + "  ");
        }
        System.out.println("\n");
        System.out.print(" succesors: ");
        for (int suc : succesors){
       	 System.out.print( suc + "  ");
       }
        
        System.out.println("\n");
        System.out.print(" predecessors: ");
        for (int pre : predecessors){
       	 System.out.print( pre + "  ");
       }
        System.out.println("\n");

        variablesLists.add(0, visits);
        variablesLists.add(1, predecessors);
        variablesLists.add(2, succesors);

        return variablesLists;
    }
    
    
}