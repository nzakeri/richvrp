package org.uoc.rgcws;

/***********************************************************************************
* Project SimORouting - Vehicle.java
* This class defines a vehicle in the VRP.
* Date of last revision (YYMMDD): 101224
* (C) Angel A. Juan & Juan C. Cruz
***********************************************************************************/
public class Vehicle implements Comparable<Vehicle>
{
    /*******************************************************************************
    *  INSTANCE FIELDS 
    *******************************************************************************/
    private static int nInstances = 0; // number of instances
    private int id;
    private float vCap; // vCap = vehicle max. capacity
    private boolean isAvailable;

    /*******************************************************************************
    *  CONSTRUCTOR
    *******************************************************************************/
    public Vehicle(float capacity)
    {   nInstances++;
        id = nInstances;
        vCap = capacity;
        isAvailable = true;
    }

    /*******************************************************************************
    *  GET METHODS
    *******************************************************************************/
    public int getId() 
    {   return id;
    }

    public float getVcap()
    {   return vCap;
    }

    public boolean getAvailability()
    {   return isAvailable;
    }

    /*******************************************************************************
    *  SET METHODS
    *******************************************************************************/
    public void setId(int identifier)
    {   id = identifier;
    }

    public void setVcap(int capacity)
    {   vCap = capacity;
    }

    public void setAvailability(boolean isFree)
    {   isAvailable = isFree;
    }

    /*******************************************************************************
    *  AUXILIARY METHODS
    *******************************************************************************/
    public int compareTo(Vehicle otherVehicle) 
    {  double s1 = this.getVcap();
       double s2 = otherVehicle.getVcap();
       if (s1 < s2) 
           return -1;
       else
           return 1;
    }
}