package org.uoc.rgcws.experiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Random;

import org.uoc.rgcws.ElapsedTime;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.InputsManager;
import org.uoc.rgcws.MultiStart;
import org.uoc.rgcws.Outputs;
import org.uoc.rgcws.RandCWS;
import org.uoc.rgcws.Solution;
import org.uoc.rgcws.Test;
import org.uoc.rgcws.TestsManager;
import org.uoc.rgcws.Vehicle;

import umontreal.iro.lecuyer.rng.LFSR113;
import umontreal.iro.lecuyer.rng.RandomStream;
import eclipse.CheckCP;


public class TaillardHVRP
{
    final static String inputFolder = "inputs";// + File.separator + "TSP";
    final static String outputFolder = "outputs\\taillard_CP";// + File.separator + "TSP";
    final static String fileNameTest = "taillardTest.txt";//TSP.txt";

    final static String testFolder = "tests";
    final static String sufixFileNodes = "_input_nodes.txt";
    final static String sufixFileVehicules = "_input_vehicles.txt";
    final static String sufixFileOutput = "_outputs.txt";
    

    public static void main(String[] args)
    {
        System.out.println("****  WELCOME TO THIS PROGRAM  ****");
        long programStart = ElapsedTime.systemTime();

        /***************************************************************************
        * 1. GET THE LIST OF TESTS TO RUN FROM "test2run.txt"
        *  aTest = instanceName + testParameters
        ***************************************************************************/
        String testsFilePath = testFolder + File.separator + fileNameTest;
        ArrayList<Test> testsList = TestsManager.getTestsList(testsFilePath);

        /***************************************************************************
        * 2. FOR EACH TEST (instanceName + testParameters) IN THE LIST...
        ***************************************************************************/
        int nTests = testsList.size();
        System.out.println("numbre de test "+nTests);
        for( int k = 0; k < nTests; k++ )
        {   
        	 int count = 0;
        	ArrayList<Solution> solList = new ArrayList<Solution>();
    		
        	Test aTest = testsList.get(k);
            	
            // 2.1 GET THE INSTANCE INPUTS (DATA ON NODES AND VEHICLES)
            // "instanceName_input_nodes.txt" contains data on nodes
            String inputNodesPath = inputFolder + File.separator +
                    aTest.getInstanceName() + sufixFileNodes;
            System.out.println(inputNodesPath);
            // "instanceName_input_vehicles.txt" contains data on vehicles
            String inputVehPath = inputFolder + File.separator +
                    aTest.getInstanceName() + sufixFileVehicules;

            
            int s = Math.max(aTest.getSeed(), 128);
    		int seedArray[] = { s, s, s, s };
    		LFSR113.setPackageSeed(seedArray); // L'Ecuyer LFSR113 (period 2^113-1)
   		 		
           
            // Read inputs files (nodes + vehicles) and construct the inputs object
            Inputs inputs = InputsManager.readInputs(inputNodesPath, inputVehPath);
            // Create savingsList with edges (i,j) where 0 < i < j
    		InputsManager.generateSavingsList(inputs);
    		// Create depotNodesList with paired edges (0,i) and (i,0), i > 0
    		//InputsManager.createDepotEdgesList(inputs);
    		InputsManager.generateDepotEdges(inputs);
    		
    		double elapsed = 0;
            long startTime = ElapsedTime.systemTime();
        	            
            String outputsFilePath = outputFolder + File.separator +
	                   aTest.getInstanceName() + "_" + aTest.getSeed() + sufixFileOutput;
           
    		
			while (elapsed < aTest.getMaxTime()) {

	    		// 2. COMPUTE THE CWS SOLUTION
    			
	    		//Solution cwsSol = RandGCWS.solve(aTest, inputs, true); // randomness is
	    															// off
				Random rng = new Random(); //Negar
				Solution cwsSol = RandCWS.solve(aTest, inputs, rng, true);
				
				//add each solution to an array
				
				solList.add(cwsSol);
	    						
	    		elapsed = ElapsedTime.calcElapsed(startTime, ElapsedTime.systemTime());
	    		
    		}
			int numNodes = inputs.getNodes().length;
			int numVehicles = inputs.getVehs();//solList.get(k1).getRoutes().size();
			float vehCapacity = inputs.getVehCap();
			//float maxDis = aTest.getMaxRouteCosts();
          CheckCP cpChecker = new CheckCP(aTest.getInstanceName());
          int yesCount = 0;
          int noCount = 0;
          //check each feasibility of each solution
          File file = new File(outputsFilePath);
		    try {
				new FileWriter(file,false);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
          for(int k1 = 0; k1 < solList.size(); k1++){
        	  
        	  
        	  count++;

        	  if ( (count % 10) == 0)
        	  {
        		  System.out.println(count);
        	  }
        	  ArrayList<ArrayList<Integer>> variablesToCheck = 
        			  cpChecker.getVariablesArrayList(solList.get(k1).createSucPred(numNodes, numVehicles));

        	  if (cpChecker.check(variablesToCheck)) {
        		  // System.out.println("yes");

        		  //System.out.println(cwsSol.toString());
        		  yesCount ++;
        		  System.out.println("yes");

        	  }else{
        		  System.out.println("no");


        		  //System.out.println(cwsSol.toString());

        		  noCount ++;
        	  }

        	  try(PrintWriter writeSol = new PrintWriter(new BufferedWriter
  			(new FileWriter(file, true)))) {
  		writeSol.println(solList.get(k1).toString() + "\r");
  		writeSol.println ("--------------------------------------------");
  		
  	}catch (IOException e) {
  		System.err.println(e);
  	}
      
        	  
        	  
          }	

          float proportion =  ((float)yesCount / solList.size())*100;
          try(PrintWriter out = new PrintWriter(new BufferedWriter
        		  (new FileWriter("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\gen\\taillard_results\\compare.txt", true)))) {
        	  out.println("\r\n" + aTest.getInstanceName()+"\t\t" + solList.size() +"\t\t" + yesCount +"\t\t"+ noCount + "\t\t" +
        			  proportion+"\t\t"+ numNodes+ "\t\t"+ numVehicles +"\t\t" +vehCapacity +"\r\n");
        	  //out.println(yesCount);
          }catch (IOException e) {
        	  System.err.println(e);
          }

          
          

        }


        /***************************************************************************
        * 3. END OF PROGRAM
        ***************************************************************************/
        System.out.println("\n****  END OF PROGRAM, CHECK OUTPUTS FILES  ****");
            long programEnd = ElapsedTime.systemTime();
            System.out.println("Total elapsed time = "
                + ElapsedTime.calcElapsedHMS(programStart, programEnd));
           
            
    }
    
           
        
}