package org.uoc.rgcws.experiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;

import org.uoc.rgcws.ElapsedTime;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.InputsManager;
import org.uoc.rgcws.MultiStart;
import org.uoc.rgcws.Outputs;
import org.uoc.rgcws.RandCWS;
import org.uoc.rgcws.Solution;
import org.uoc.rgcws.Test;
import org.uoc.rgcws.TestsManager;

import umontreal.iro.lecuyer.rng.LFSR113;
import umontreal.iro.lecuyer.rng.RandomStream;
import eclipse.CheckCP;

/***********************************************************************************
* Project SimORouting - MultiStartTester
* This class contains the main() function that tests the MultiStart procedure.
* It reads the test2run.txt file and, for each specified test (instanceName +
*  testParameters): (a) sets the instance inputs (instanceName_input_nodes.txt and
*  instanceName_input_vehicles.txt) and the test parameters, (b) solves the test
*  by using the MultiStart algorithm, and (c) prints the results in an outputs file.
* Date of last revision (YYMMDD): 101224
* (C) Angel A. Juan & Juan C. Cruz - http://ajuanp.wordpress.com
* Modified by Jose Caceres 110210
***********************************************************************************/
public class KellyWithoutCPcheck
{
    final static String inputFolder = "inputs";// + File.separator + "TSP";
    final static String outputFolder = "outputs\\kellywithoutCP";// + File.separator + "TSP";
    final static String fileNameTest = "kellyTest.txt";//TSP.txt";

    final static String testFolder = "tests";
    final static String sufixFileNodes = "_input_nodes.txt";
    final static String sufixFileVehicules = "_input_vehicles.txt";
    final static String sufixFileOutput = "_outputs.txt";
    

    public static void main(String[] args)
    {
        System.out.println("****  WELCOME TO THIS PROGRAM  ****");
        long programStart = ElapsedTime.systemTime();

        /***************************************************************************
        * 1. GET THE LIST OF TESTS TO RUN FROM "test2run.txt"
        *  aTest = instanceName + testParameters
        ***************************************************************************/
        String testsFilePath = testFolder + File.separator + fileNameTest;
        ArrayList<Test> testsList = TestsManager.getTestsList(testsFilePath);

        /***************************************************************************
        * 2. FOR EACH TEST (instanceName + testParameters) IN THE LIST...
        ***************************************************************************/
        int nTests = testsList.size();
        System.out.println("numbre de test "+nTests);
        for( int k = 0; k < nTests; k++ )
        {   
        	
        	int numSol = 0;
    		
        	Test aTest = testsList.get(k);
            //System.out.println("\n# STARTING TEST " + (k + 1) + " OF " + nTests);

            // 2.1 GET THE INSTANCE INPUTS (DATA ON NODES AND VEHICLES)
            // "instanceName_input_nodes.txt" contains data on nodes
            String inputNodesPath = inputFolder + File.separator +
                    aTest.getInstanceName() + sufixFileNodes;
            System.out.println(inputNodesPath);
            // "instanceName_input_vehicles.txt" contains data on vehicles
            String inputVehPath = inputFolder + File.separator +
                    aTest.getInstanceName() + sufixFileVehicules;

            
            int s = Math.max(aTest.getSeed(), 128);
    		int seedArray[] = { s, s, s, s };
    		LFSR113.setPackageSeed(seedArray); // L'Ecuyer LFSR113 (period 2^113-1)
    		RandomStream stream = new LFSR113();
    		//aTest.setRandomStream(stream);
               
            // Read inputs files (nodes + vehicles) and construct the inputs object
            Inputs inputs = InputsManager.readInputs(inputNodesPath, inputVehPath);
            
    		inputs.setVrpCenter(InputsManager.calcGeometricCenter(inputs.getNodes()));
    		// Create savingsList with edges (i,j) where 0 < i < j
    		InputsManager.generateSavingsList(inputs);
    		// Create depotNodesList with paired edges (0,i) and (i,0), i > 0
    		//InputsManager.createDepotEdgesList(inputs);
    		InputsManager.generateDepotEdges(inputs);
    		
    		double elapsed = 0;
            long startTime = ElapsedTime.systemTime();
            //CheckCP cpChecker = new CheckCP(aTest.getInstanceName());
        	
           // int yesCount = 0;
            //int noCount = 0;
    		
			while (elapsed < aTest.getMaxTime()) {

	    		// 2. COMPUTE THE CWS SOLUTION
    			
	    		Solution cwsSol = RandCWS.solve(aTest, inputs, null, true); // randomness is
	    															// off

	    		numSol ++;

	    		

	    		// 2.3. PRINT OUT THE RESULTS TO FILE "instanceName_seed_outputs.txt"
	    		//String outputsFilePath = outputFolder + File.separator +
	    		//		aTest.getInstanceName() + "_" + aTest.getSeed() + sufixFileOutput;
	    		//;		

	    		//writefile(outputsFilePath, cwsSol);
	    		elapsed = ElapsedTime.calcElapsed(startTime, ElapsedTime.systemTime());
			}
    		
    		try(PrintWriter out = new PrintWriter(new BufferedWriter
            		(new FileWriter("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\outputs\\kellywithoutCP\\withoutCP.txt", true)))) {
    			out.println("\r\n" + aTest.getInstanceName()+"\t\t" + numSol  +"\r\n");
    			//out.println(yesCount);
            }catch (IOException e) {
                System.err.println(e);
            }
	            
	    	System.out.println("num solutions: "+numSol);	 
        }
	    		

        /***************************************************************************
        * 3. END OF PROGRAM
        ***************************************************************************/
        System.out.println("\n****  END OF PROGRAM, CHECK OUTPUTS FILES  ****");
            long programEnd = ElapsedTime.systemTime();
            System.out.println("Total elapsed time = "
                + ElapsedTime.calcElapsedHMS(programStart, programEnd));
         
    }
    static public void writefile(String dir, Solution cwsSolution){

        try{
            Writer output = null;
           
            
            
            File file = new File(dir);
            output = new BufferedWriter(new FileWriter(file));
    

            output.write("Clarke & Wright Solution (Randomize version)\n");
            output.write("--------------------------------------------\n");
            
            output.write(cwsSolution.toString() + "\r\n");
            output.write("--------------------------------------------");

            output.close();
            

        }catch(Exception e){
            System.out.println("Could not create file");
        }
    }  
    
     
}