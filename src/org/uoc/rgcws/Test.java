package org.uoc.rgcws;

/**
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class Test
{
    /* INSTANCE FIELDS AND CONSTRUCTOR */
    private String instanceName;
    private float maxRouteCosts; // Maximum costs allowed for any single route
    private float serviceCosts; // Costs of completing an individual service
    private float maxTime; // Maximum computing time allowed
    private String distrib; // Statistical distribution for the randomness
    private float firstParam; // First parameter associated with the distribution
    private float secondParam; // Second parameter associated with the distribution
    private int seed; // Seed value for the Random Number Generator (RNG)
 
    private int nTestCP; // Seed value for the Random Number Generator (RNG)
    
    public Test(String name, float rCosts, float sCosts, float t, 
            String d, float p1, float p2, int s, int tCP)
    {
        instanceName = name;
        maxRouteCosts = rCosts;
        serviceCosts = sCosts;
        maxTime = t;
        distrib = d;
        firstParam = p1;
        secondParam = p2;
        seed = s;
        nTestCP = tCP;
    }

    /* GET METHODS */
    public String getInstanceName(){return instanceName;}
    public float getMaxRouteCosts(){return maxRouteCosts;}
    public float getServiceCosts(){return serviceCosts;}
    public float getMaxTime(){return maxTime;}
    public String getDistribution(){return distrib;}
    public float getFirstParam(){return firstParam;}
    public float getSecondParam(){return secondParam;}
    public int getSeed(){return seed;}
    public int getTestCP(){return nTestCP;}
}