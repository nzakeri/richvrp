package connectionACOwithCP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.uoc.rgcws.Edge;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.Node;
import org.uoc.rgcws.Route;
import org.uoc.rgcws.Solution;
import org.uoc.rgcws.Test;
import cern.jet.random.engine.MersenneTwister;


public final class ACOSolverGlobalUpdate {


	// greedy
	//public static final double ALPHA = 0.6d for all except hvrp//3 for setp
	public static final double ALPHA =3d; //P&D Ic101 0.5
	// rapid selection
	//public static final double BETA = 5.5d for all except hvrp//3 for setp 
	public static final double BETA =3d; //P&D Ic101 5
	public static final double BETAInitialization = 1d;// 9 is good for SetP

	// heuristic parameters
	//public static final double Q = 100d; // somewhere between 0 and 1......for all except hvrp
	public static final double Q = 1d;//setP 100
	public static final double evaperationRate = 0.001d; // between 0 and 1//P&D Ic101 0.01
	public static final double INITIAL_PHEROMONES = 2d; // can be anything//2d

	public static final double MAX_PHEROMONES = 3d;// INITIAL_PHEROMONES;
	public static final double MIN_PHEROMONES = 1d;

	// use power of 2
	public static final int NUM_AGENTS = 3000; //setP 50;
	private static final int POOL_SIZE = 1; //Runtime.getRuntime().availableProcessors();
	//public final double probBias = 0d;for all except hvrp
	public final double probBias = 0d;
	public static int[] finalWay;
	public static double finalDis;
	//private Uniform uniform;

	private static final ExecutorService THREAD_POOL = Executors
			.newFixedThreadPool(POOL_SIZE);
	static final int phermoneDecayStep = 50 ;//3
	public static final int failurePunishmentRate = 2000;//setP 2000

	//we get an agent to it and it executes until finish
	private final ExecutorCompletionService<WalkedWay> agentCompletionService = new ExecutorCompletionService<WalkedWay>(
			THREAD_POOL);

	final double[][] matrix;
	final double[][] invertedMatrix;

	final double[][] pheromones;
	private final Object[][] mutexes;
	private MersenneTwister numgen;

	Inputs inputs = null;
	private double meanDis;
	int stepToUpdatePher = 0;
	ArrayList<int[]> arrayWays = new ArrayList<int[]>();
	ArrayList<Double>  arrayDis =new ArrayList<Double>();
	WalkedWay bestKnown;



	public ACOSolverGlobalUpdate(Test aTest, Inputs inputs, int seed) throws IOException {
		this.inputs = inputs;

		matrix = createDistMatrix(inputs.getNodes() , inputs.getVehs());
		//matrix = createDistMatrixFromFile(inputs.getNodes() , inputs.getVehs(), aTest.getInstanceName());
		invertedMatrix = invertMatrix();
		pheromones = initializePheromones();
		mutexes = initializeMutexObjects();

		numgen = new cern.jet.random.engine.MersenneTwister(seed);
		//(int) System.currentTimeMillis());
	}

	private double[][] createDistMatrixFromFile(Node[] nodes, int vehNum, String instanceName) throws IOException {
		String fileName = "prolog\\CPAIOR2015\\avrp\\" + instanceName + ".ecl";
		//int numVehicles = vehNum-1;
		int numVehicles = vehNum;
		int matSize = nodes.length + numVehicles;
		double[][] disMatrix = new double[matSize][matSize];

		try (BufferedReader in = new BufferedReader(
				new FileReader(fileName))) {
			String line = in.readLine();
			while(line != null){
				if (line.contains( "getDistanceMatrix")){
					line = line.replace("getDistanceMatrix","");
					break;
				}
				else line = in.readLine();
			}
			int curLineNo = 0;
			while (line!= null){
				boolean lineHasCloseParan = line.contains( ")");
				line = line.replaceAll( "[\\[\\]\\(\\)\\.,]"," " ).trim();
				String[] parts = line.split("[ ]+");
				for (int i = 0; i < parts.length; i++){
					int ii = i+1;
					if (ii == parts.length)
						ii = 0;
					int jj = curLineNo+1;
					if (jj == parts.length){
						jj = 0;
					}
					disMatrix[jj][ii] = Double.parseDouble(parts[i]);
					//	System.out.print(disMatrix[ii][jj]);

				}

				if (lineHasCloseParan)
					break;
				line = in.readLine();
				curLineNo ++;
				//	System.out.println();
			}


		}

		return disMatrix;

	}


	private double[][] createDistMatrix(Node[] nodes, int vehNum) {

		//int numVehicles = vehNum-1;// the nodes already have one depot
		int numVehicles = vehNum;
		int matSize = nodes.length + numVehicles; 
		double[][] disMatrix = new double[matSize][matSize];
		double sumDistance = 0;		

		for(int i = 0; i < matSize; i++ ){
			for (int j = 0; j < matSize; j++){
				int ri = i;
				int rj = j;
				if (ri >= nodes.length)
					ri = 0;
				if (rj >= nodes.length)
					rj = 0;
				disMatrix[i][j] = calculateEuclidianDistance(nodes[ri].getX(), nodes[ri].getY(), 
						nodes[rj].getX(),nodes[rj].getY());	
				sumDistance += disMatrix[i][j]; //For calculate distance of external nodes
				//	System.out.print("  " + disMatrix[i][j]);
			}
			//	System.out.println();
		}


		return disMatrix;
	}

	private final Object[][] initializeMutexObjects() {
		final Object[][] localMatrix = new Object[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = new Object();
			}
		}

		return localMatrix;
	}

	final double readPheromone(int x, int y) {
		return pheromones[x][y];
	}

	final void adjustPheromone(int x, int y, double deltaPheromone) {
		synchronized (mutexes[x][y]) {
			pheromones[x][y] += deltaPheromone;
			//Adding on 11-09-2015
			if (pheromones[x][y] > MAX_PHEROMONES){
				pheromones[x][y] = MAX_PHEROMONES;
			}
			//	System.out.println (x+" " + y + " " +pheromones[x][y]);
		}
	}


	void decayAllPheromones(int times) {
		double mutliplier = Math.pow((1 - ACOSolverGlobalUpdate.evaperationRate),times);
		for (int x = 0; x < mutexes.length; x++){
			for (int y = 0; y < mutexes.length; y++){
				synchronized (mutexes[x][y]) {
					pheromones[x][y] *= mutliplier;
					//Adding on 11-09-2015
					if (pheromones[x][y] < MIN_PHEROMONES){
						pheromones[x][y] = MIN_PHEROMONES;
						//	System.out.println("Lower Bound");
					}
				}
			}
		}
	}


	private final double[][] initializePheromones() {
		final double[][] localMatrix = new double[matrix.length][matrix.length];
		int rows = matrix.length;
		for (int columns = 0; columns < matrix.length; columns++) {
			for (int i = 0; i < rows; i++) {
				localMatrix[columns][i] = INITIAL_PHEROMONES;
			}
		}

		return localMatrix;
	}



	private final String[] sweepNumbers(String trim) {
		String[] arr = new String[3];
		int currentIndex = 0;
		for (int i = 0; i < trim.length(); i++) {
			final char c = trim.charAt(i);
			if ((c) != 32) {
				for (int f = i + 1; f < trim.length(); f++) {
					final char x = trim.charAt(f);
					if ((x) == 32) {
						arr[currentIndex] = trim.substring(i, f);
						currentIndex++;
						break;
					} else if (f == trim.length() - 1) {
						arr[currentIndex] = trim.substring(i, trim.length());
						break;
					}
				}
				i = i + arr[currentIndex - 1].length();
			}
		}
		return arr;
	}

	private final double[][] invertMatrix() {
		double[][] local = new double[matrix.length][matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				local[i][j] = invertDouble(matrix[i][j]);
			}
		}
		return local;
	}

	private final double invertDouble(double distance) {
		if (distance == 0d)
			return 0.01;
		else
			return 1.0d / distance;
	}

	private final double calculateEuclidianDistance(double x1, double y1,
			double x2, double y2) {
		final double xDiff = x2 - x1;
		final double yDiff = y2 - y1;
		return Math.abs((Math.sqrt((xDiff * xDiff) + (yDiff * yDiff))));
	}

	public final WalkedWay start() throws Exception {

		WalkedWay bestWalkedWay = null;


		int agentsSend = 0;
		int agentsDone = 0;
		int agentsWorking = 0;


		for (int agentNumber = 0; agentNumber < NUM_AGENTS; agentNumber++) {
			agentCompletionService.submit(new AntFunctionGlobalUpdate(this,
					getGaussianDistributionRowIndex(), numgen.nextInt()));
			agentsSend++;
			agentsWorking++;
			while (agentsWorking >= POOL_SIZE) { //each time 4 ants work simultaneously
				WalkedWay way = agentCompletionService.take().get();//wait until the 1st ant finishes its job
				if (bestWalkedWay == null || way.distance < bestWalkedWay.distance) {
					bestWalkedWay = way;
					bestKnown = way;

				} 
				System.out.println("" + agentsDone + " cur dist " + way.distance + " best dist " + bestWalkedWay.distance );
				agentsDone++;

				agentsWorking--;
			}
		}
		final int left = agentsSend - agentsDone;


		for (int i = 0; i < left; i++) {
			WalkedWay way = agentCompletionService.take().get();
			if (bestWalkedWay == null || way.distance < bestWalkedWay.distance) {
				bestWalkedWay = way;

			}
		}



		return bestWalkedWay;

	}

	private final int getGaussianDistributionRowIndex() {
		//return uniform.nextInt();
		return 0;
	}

	static class Record {
		double x;
		double y;

		public Record(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}
	}

	static class WalkedWay {
		int[] way;
		double distance;

		public WalkedWay(int[] way, double distance) {
			super();
			this.way = way;
			this.distance = distance;
		}
	}



	public Solution solve( Inputs inputs) throws Exception {

		WalkedWay bestSol = this.start();
		Solution sol = new Solution();



		System.out.println("best way:" + Arrays.toString(bestSol.way));
		try(PrintWriter out = new PrintWriter(new BufferedWriter				
				(new FileWriter("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\gen\\connectionACOwithCP\\cvrp\\results\\sol.txt", true)))) {
			out.println("best way:" + Arrays.toString(bestSol.way)+ "    "+
					bestSol.distance+  "\r\n"+ CVRP.aTest.getInstanceName());
			out.println("-----------------------------------------------------------------");
		}

		if (bestSol.way!=null){

			finalWay = bestSol.way;
			finalDis = bestSol.distance;
			ArrayList<Node> listNodes = new ArrayList<Node>();

			for (int i = 0; i < bestSol.way.length ; i++){

				int k = bestSol.way[i];

				if ( k < inputs.getNodes().length && k != 0 && 
						k!= bestSol.way[bestSol.way.length-1] ){

					Node iNode = inputs.getNodes()[k];
					listNodes.add(iNode);

				} else

					if (listNodes != null && !listNodes.isEmpty()) {

						Node lastEgdeStart;
						Route route = new Route();
						Node firstNode = listNodes.get(0);
						Edge startEdge = new Edge(inputs.getNodes()[0],firstNode);

						startEdge.setCosts(startEdge.calcCosts(inputs.getNodes()[0], firstNode));

						route.getEdges().add(startEdge);
						route.setDemand(route.getDemand() + startEdge.getEnd().getDemand());
						route.setCosts(route.getCosts() + startEdge.getCosts());


						for (int j = 0; j < listNodes.size() -1; j++){
							Node start = listNodes.get(j);
							Node end = listNodes.get(j+1);
							Edge edge = new Edge(start, end);

							edge.setCosts(edge.calcCosts(start, end));

							route.getEdges().add(edge);
							route.setDemand(route.getDemand() + edge.getEnd().getDemand());
							route.setCosts(route.getCosts() + edge.getCosts());
						}


						lastEgdeStart = listNodes.get(listNodes.size()-1);
						Edge lastEdge = new Edge(lastEgdeStart, inputs.getNodes()[0]);

						lastEdge.setCosts(lastEdge.calcCosts(lastEgdeStart, inputs.getNodes()[0]));

						route.getEdges().add(lastEdge);
						route.setDemand(route.getDemand() + lastEdge.getEnd().getDemand());
						route.setCosts(route.getCosts() + lastEdge.getCosts());

						sol.getRoutes().add(route);
						sol.setCosts(sol.getCosts() + route.getCosts());
						sol.setDemand(sol.getDemand() + route.getDemand());
						listNodes.clear();


					} else{


					}

			}

			System.out.println(bestSol.distance);



			return sol;

		}
		return null;
	}	

	public double calcCostsAVRP(Node origin, Node end)
	{   double X1 = origin.getX();
	double Y1 = origin.getY();
	double X2 = end.getX();
	double Y2 = end.getY();
	double d = Math.sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1));
	//return Math.round(d); // OJO!
	return (d);
	}
}





