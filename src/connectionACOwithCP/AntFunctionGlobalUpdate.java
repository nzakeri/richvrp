package connectionACOwithCP;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.Callable;

import antColonyPartialSol.PickUp_Delivery;
import connectionACOwithCP.ACOSolverGlobalUpdate.WalkedWay;
import eclipse.CheckCP_everyTwoSteps;

public final class AntFunctionGlobalUpdate implements Callable<WalkedWay> {

	public final ACOSolverGlobalUpdate instance;
	private double distanceWalked = 0.0d;
	private final int start;
	public  boolean[] visited;
	public  int[] way;
	public int toVisit;
	private Random random = null;
	float totalCapacity = 0;
	private boolean routeNotPossible = false;
	int goalPoint;
	int stepToCheck;
	private float[] distanceWalkedArr;
	private int groupAgent;
	private int[] antWay;
	private Double groupCost = 0.0;
	int backtrackNode;
	private boolean callEachTwoSteps;


	public static CheckCP_everyTwoSteps cpCaller;
	//	private double[] weights;


	public AntFunctionGlobalUpdate(ACOSolverGlobalUpdate acoSolverGlobalUpdate2, int start,int seed) {
		super(); //call its father's constructor
		this.instance = acoSolverGlobalUpdate2;
		this.visited = new boolean[acoSolverGlobalUpdate2.matrix.length];	
		visited[start] = true;
		toVisit = visited.length;
		this.start = start;

		this.way = new int[visited.length];

		random = new Random(seed);
		//System.out.println("seed is "+seed);
		goalPoint = (this.instance.inputs.getNodes().length)+ (this.instance.inputs.getVehs()/2);
		stepToCheck = 5;//(instance.matrix.length)/10;
		
		callEachTwoSteps = true;
		
	}


	public final LinkedList<Integer> getNextProbableNode(LinkedList<LinkedList<Integer>> possibleList, int idCurNode) {

		if (!possibleList.isEmpty()) {

			double sum = 0.0d;			
			LinkedList<LinkedList<Integer>> computableNodes = new LinkedList<LinkedList<Integer>>(); //list of Nodes we can visit as successors

			for (int j = 0; j < possibleList.size(); j++){
				computableNodes.add(possibleList.get(j));
				if (possibleList.get(j).getFirst() >= instance.inputs.getNodes().length){
					backtrackNode = idCurNode;
				}
			}

			if (computableNodes.isEmpty())
				return null;

			final double[] weights = new double[computableNodes.size()];

			for (int j = 0; j < computableNodes.size(); j++){
				double weightpart1 = calculateNonNormalizedProbability(computableNodes.get(j).getFirst(), way[idCurNode]);				
				double weightpart2 = calculateNonNormalizedProbability(computableNodes.get(j).getLast(), computableNodes.get(j).getFirst());

				weights[j] = weightpart1 * weightpart2;
				sum += weights[j];	

			}

			if (sum == 0.0d)
				return null;

			double probSum = 0.0d;
			double curRand = random.nextDouble();
			final double r = curRand*sum;
			for (int i = 0; i < computableNodes.size(); i++) {

				probSum +=weights[i];

				if (r <= probSum) {

					return computableNodes.get(i);

				}
			}

		}

		return null;
	}


	public final double calculateNonNormalizedProbability(int row, int column) {
		final double p = Math.pow(instance.readPheromone(column, row),
				ACOSolverGlobalUpdate.ALPHA)
				* Math.pow(instance.invertedMatrix[column][row],
						ACOSolverGlobalUpdate.BETA);
		double a = Math.pow(instance.readPheromone(column, row),
				ACOSolverGlobalUpdate.ALPHA);
		double b =  Math.pow(instance.invertedMatrix[column][row],
				ACOSolverGlobalUpdate.BETA);
		double c = a *b;
		return (p + this.instance.probBias);
	}


	public final WalkedWay call() throws Exception { 


		int nextDepot = instance.inputs.getNodes().length;
		int maxNodePos = instance.inputs.getNodes().length + instance.inputs.getVehs()-1;	

		int i = 1;		

		for (; i <= maxNodePos; i++ ){

			int idcurNode = i-1;
			int requiredNode = way[idcurNode];

			//	System.out.println("NEXT Depot:  " + nextDepot);
			if (way[idcurNode] >= instance.inputs.getNodes().length){

				requiredNode = nextDepot;

			}

			if (idcurNode==0){
				requiredNode = instance.inputs.getNodes().length;
			} 

			int curVehicle = nextDepot - instance.inputs.getNodes().length +1;

			//	System.out.println("Next Depot" + nextDepot);
			ArrayList<ArrayList<Integer>> variablesToCheck = 
					cpCaller.getVariablesArrayList
					(createSucPredEsp(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));
			/*try {

				CVRP.writeVarEachCall.write("Variable To Check: " + variablesToCheck+ "\r\n" );			

				CVRP.writeVarEachCall.write("\r\n ------------------------------------------------- \r\n");
				//	CVRP_CP_TwoSteps.writeAveCost.write("\r\n ------------------------------------------------- \r\n");
				CVRP.writeVarEachCall.flush(); 

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	*/

			LinkedList<LinkedList<Integer>> probableSucList = cpCaller.checkEsp(variablesToCheck, requiredNode, curVehicle);
			//		System.out.println(probableSucList.toString());

			LinkedList<Integer> idNextNode = getNextProbableNode(probableSucList, idcurNode);
			//		System.out.println("ID NEXT NODE:  " + idNextNode);

			if (idNextNode != null){

				if (idNextNode.getFirst() < instance.inputs.getNodes().length){
					//setOfDepots.remove(idNextNode.getFirst());
					way[i] = idNextNode.getFirst();
				}else{
					way[i] = nextDepot;
					nextDepot++;
				}
				if (way[i] == maxNodePos){
					//	if (setOfDepots.isEmpty()){
					//	i++;
					break;
				}

				if (callEachTwoSteps == true){
				//	System.out.println(i+1);

					if (i+1<= maxNodePos  && idNextNode.getLast() != 0){

						if (idNextNode.getLast() < instance.inputs.getNodes().length){
							//setOfDepots.remove(idNextNode.getFirst());
							way[i+1] = idNextNode.getLast();
						}else{
							way[i+1] = nextDepot;
							nextDepot++;
						}

						i++;

					}
				}
			}
			else{

				for(int j =i; j > backtrackNode; j-- ){
					way[j] = 0;
				}


				i = backtrackNode+1;

				way[i] = nextDepot;
				//	visited[nextDepot] = true;
				backtrackNode = i;

				nextDepot++;

			}


			if ( way[i] == maxNodePos || nextDepot > maxNodePos){
				//	i++;
				break;
			}


		}

		int nMissed = maxNodePos - i;
		float maxDistance = 100000000;//1000000;

		for (int k=0; k<i; k++){
			distanceWalked += instance.matrix[way[k]][way[k+1]];
		}
		double a =0;
		a = instance.matrix[way[way.length-1]][0];
		distanceWalked += a ;
		if (nMissed > 0){
			distanceWalked += maxDistance*(nMissed);
			//distanceWalkedArr[groupAgent] +=maxDistance*(nMissed+1);//New added
		}
		else {
			//			run full cp
			ArrayList<ArrayList<Integer>> variablesToCheck2 = 
					cpCaller.getVariablesArrayList(createSucPredCompleteSol(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));

			boolean res2 = cpCaller.check(variablesToCheck2, 9999, instance.inputs.getVehs());

			if (!res2){
				distanceWalked += maxDistance*(1);
				System.out.println("THE ROUTE IS NOT FEASIBLE");

			}
			try {

				CVRP.writeAveCost.write("Variable To Check2: " + variablesToCheck2+ "\r\n" );
				CVRP.writeAveCost.write("distanceCost:  " + distanceWalked+ "\r\n" );

				CVRP.writeAveCost.write("\r\n ------------------------------------------------- \r\n");
				//	CVRP_CP_TwoSteps.writeAveCost.write("\r\n ------------------------------------------------- \r\n");
				CVRP.writeAveCost.flush(); 

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}


		instance.arrayWays.add(way);
		instance.arrayDis.add(distanceWalked);
		instance.stepToUpdatePher +=1;
		if (instance.stepToUpdatePher == ACOSolverGlobalUpdate.phermoneDecayStep) //New added
		{

			double minDisInGroup = 9999999999.99999999;
			System.out.println("!!!!PHEROMONE UPDATING PROCESS!!!!");
			for(int h3=0; h3 < instance.stepToUpdatePher; h3++){

				double phero = (ACOSolverGlobalUpdate.Q / (instance.arrayDis.get(h3)));//calculate pheromone for each ant in the group

				int previous = instance.arrayWays.get(h3)[0]; //update pheromone for a solution found by one ant in the group
				for(int h = 1; h < instance.arrayWays.get(h3).length; h++){  
					instance.adjustPheromone(previous, instance.arrayWays.get(h3)[h], phero);
					previous = instance.arrayWays.get(h3)[h];
				}

				groupCost += instance.arrayDis.get(h3);
				if (instance.arrayDis.get(h3) < minDisInGroup){
					minDisInGroup = instance.arrayDis.get(h3);
				}


			}
			/*CVRP.writeAveCost.write("Minimum distance in this group:" + "    "+ minDisInGroup +  "\r\n");
			CVRP.writeAveCost.write("-----------------------------------------------------------------\r\n");
			CVRP.writeAveCost.flush();*/

			double aveCost = groupCost / instance.stepToUpdatePher;

			groupCost = 0.0;
			instance.stepToUpdatePher = 0;
			instance.arrayWays.clear();
			instance.arrayDis.clear();
			System.out.println ("DECAY STEP");
			instance.decayAllPheromones(ACOSolverGlobalUpdate.phermoneDecayStep);

		}


		//	System.out.println("Number of missed Nodes:" + nMissed);
		return new WalkedWay(way,distanceWalked);
	}

	public ArrayList<int[]> createSucPredEsp(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else //if (i-1!=0)
				{
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				}// else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
				else if (parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}
		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}


	public ArrayList<int[]> createSucPredCompleteSol(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else {
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay[i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} //else if(i+1 == parWay.length || parWay[i+1] != 0){
				else if(parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}

		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
			if (succesors[numNodes + j] == 0)
			{				
				succesors[numNodes + j] = numNodes + j;
			}
			if (predecessors[numNodes + j] == 0)
			{				
				predecessors[numNodes + j] = numNodes + j;
			}
		}




		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}
}





