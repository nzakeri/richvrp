package callCP_everyTwoSteps;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.Callable;

import org.uoc.rgcws.ElapsedTime;
import org.uoc.rgcws.Inputs;
import org.uoc.rgcws.Route;
import org.uoc.rgcws.Solution;
import eclipse.CheckCP;
import eclipse.CheckCP_everyTwoSteps;
import callCP_everyTwoSteps.ZZACOSolverGlobalUpdate.WalkedWay;





//import de.jungblut.antcolony.AntColonyOptimization.WalkedWay;


public final class ZZAntFunctionGlobalUpdate2_oldver_calcNonNormalizedProb implements Callable<WalkedWay> {

	public final ZZACOSolverGlobalUpdate instance;
	private double distanceWalked = 0.0d;
	private final int start;
	public  boolean[] visited;
	public  int[] way;
	public int toVisit;
	private Random random = null;
	float totalCapacity = 0;
	private boolean routeNotPossible = false;
	int goalPoint;
	int stepToCheck;
	private float[] distanceWalkedArr;
	private int groupAgent;
	private int[] antWay;
	private Double groupCost = 0.0;
	int backtrackNode;



	public static CheckCP_everyTwoSteps cpCaller;


	public ZZAntFunctionGlobalUpdate2_oldver_calcNonNormalizedProb(ZZACOSolverGlobalUpdate instance, int start,int seed) {
		super(); //call its father's constructor
		this.instance = instance;
		this.visited = new boolean[instance.matrix.length];
		visited[start] = true;
		toVisit = visited.length;
		this.start = start;
		this.way = new int[visited.length-1];
		random = new Random(seed);
		//System.out.println("seed is "+seed);
		goalPoint = (this.instance.inputs.getNodes().length)+ (this.instance.inputs.getVehs()/2);
		stepToCheck = 5;//(instance.matrix.length)/10;
	}



	public final int getNextProbableNode(LinkedList<Integer> possibleList, int idCurNode) {

		if (!possibleList.isEmpty()) {
			int danglingUnvisited = -1;
			final double[] weights = new double[visited.length];
			ArrayList<Integer> computableNodes = new ArrayList<Integer>(); //list of Nodes we can visit as successors

			for (int j = 0; j < possibleList.size(); j++){
			//	if (!visited[j]){

					if (possibleList.get(j) < instance.inputs.getNodes().length){

						computableNodes.add(possibleList.get(j));					

					}else{
						backtrackNode = idCurNode;
						//					 System.out.println("BacktrackNode: "+ backtrackNode);
					}

				}
	//		}

			if (computableNodes.isEmpty())
				return -1;

			double sum = 0.0d;


			for (Integer x : computableNodes) {
				weights[x] = calculateNonNormalizedProbability(x, way[idCurNode]);
				sum += weights[x];
				danglingUnvisited = x;
			}

			if (sum == 0.0d)
				return danglingUnvisited;


			double probSum = 0.0d;
			double curRand = random.nextDouble();
			final double r = curRand*sum;
			for (Integer x2 : computableNodes) {

				probSum +=weights[x2];

				if (r <= probSum) {


					return x2;

				}
			}

		}

		return -1;
	}



	public final double calculateNonNormalizedProbability(int row, int column) {
		final double p = Math.pow(instance.readPheromone(column, row),
				ZZACOSolverGlobalUpdate.ALPHA)
				* Math.pow(instance.invertedMatrix[column][row],
						ZZACOSolverGlobalUpdate.BETA);
		return (p + this.instance.probBias);
	}




	public final WalkedWay call() throws Exception { 


		int nextDepot = instance.inputs.getNodes().length;
		int maxNodePos = instance.inputs.getNodes().length + instance.inputs.getVehs()-1;	
		int i = 1;		

		for (; i < maxNodePos; i++ ){
		
			int idcurNode = i-1;
			int requiredNode = way[idcurNode];

			if (way[idcurNode] >= instance.inputs.getNodes().length){

				if (way[idcurNode] == nextDepot){
					requiredNode = nextDepot+1;
				}else{
					requiredNode = nextDepot;
				}


			}

			if (idcurNode==0){
				requiredNode = instance.inputs.getNodes().length;
			} 

			int curVehicle = nextDepot - instance.inputs.getNodes().length +1;

			//System.out.println("Salam Hassan");
			ArrayList<ArrayList<Integer>> variablesToCheck = 
					cpCaller.getVariablesArrayList
					(createSucPredEsp(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));
			try {

				ZZCVRP_old.writeVarEachCall.write("Variable To Check: " + variablesToCheck+ "\r\n" );

				ZZCVRP_old.writeVarEachCall.write("\r\n ------------------------------------------------- \r\n");
			//	CVRP_CP_TwoSteps.writeAveCost.write("\r\n ------------------------------------------------- \r\n");
				ZZCVRP_old.writeVarEachCall.flush(); 

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			LinkedList<LinkedList<Integer>> probableSucList = cpCaller.checkEsp(variablesToCheck, requiredNode, curVehicle);

			LinkedList<Integer> firstNodeSet = new LinkedList<Integer>();

			for (int j = 0; j < probableSucList.size(); j++){	

			//	System.out.println(probableSucList.toString());
				if (!firstNodeSet.contains(probableSucList.get(j).getFirst())){
					firstNodeSet.add(probableSucList.get(j).getFirst());
			//		System.out.println(firstNodeSet.toString());

				}
			}

			int nextNode = getNextProbableNode(firstNodeSet, idcurNode);
			LinkedList<Integer> secondNodeSet = new LinkedList<Integer>();
			if (nextNode != -1){
				way[i] = nextNode;
				visited[nextNode] = true;				
			}
			else{

				for(int j =i; j > backtrackNode; j-- ){
					way[j] = 0;
				}

				if (nextDepot == maxNodePos){
					i++;
					break;
				}
				i = backtrackNode+1;
				way[i] = nextDepot;
				visited[nextDepot] = true;
				backtrackNode = i;

				nextDepot++;
			}
			
			if ( way[i] == maxNodePos || i+1 == maxNodePos){
				i++;
				break;
			}
			
			for (int k2 = 0; k2 <  probableSucList.size(); k2++){
				if (probableSucList.get(k2).getFirst() == way[i]){
					if (!secondNodeSet.contains(probableSucList.get(k2).getLast())){
						secondNodeSet.add(probableSucList.get(k2).getLast());
				//		System.out.println(secondNodeSet.toString());
					}
				}
			}
			int secondNode = getNextProbableNode(secondNodeSet, i-1);
			if (secondNode != -1){	
				i++;
				way[i] = secondNode;
				visited[secondNode] = true;
			}
			else{
				for(int j =i; j > backtrackNode; j-- ){
					way[j] = 0;
				}

				if (nextDepot > maxNodePos){   ///  == or > ?
					i++;
					break;
				}
				i = backtrackNode+1;
				way[i] = nextDepot;
				//visited[nextDepot] = true;					

				nextDepot++;
			}

			if ( way[i] == maxNodePos){
				i++;
				break;
			}

		}

		int nMissed = maxNodePos - i;
		float maxDistance = 100000000;//1000000;

		for (int k=0; k<i-1; k++){
			distanceWalked += instance.matrix[way[k]][way[k+1]];
		}
		double a =0;
		a = instance.matrix[way[way.length-1]][0];
		distanceWalked += a ;
		if (nMissed > 0){
			distanceWalked += maxDistance*(nMissed);
			//distanceWalkedArr[groupAgent] +=maxDistance*(nMissed+1);//New added
		}
		else {
			//			run full cp
			ArrayList<ArrayList<Integer>> variablesToCheck2 = 
					cpCaller.getVariablesArrayList(createSucPredCompleteSol(way, instance.inputs.getNodes().length, instance.inputs.getVehs()));
			try {

				ZZCVRP_old.writeAveCost.write("Variable To Check2: " + variablesToCheck2+ "\r\n" );

				ZZCVRP_old.writeAveCost.write("\r\n ------------------------------------------------- \r\n");
			//	CVRP_CP_TwoSteps.writeAveCost.write("\r\n ------------------------------------------------- \r\n");
				ZZCVRP_old.writeAveCost.flush(); 

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			boolean res2 = cpCaller.check(variablesToCheck2, 9999, instance.inputs.getVehs());

			if (!res2){
				distanceWalked += maxDistance*(1);
				System.out.println("THE ROUTE IS NOT FEASIBLE");

			}
		}

		instance.arrayWays.add(way);
		instance.arrayDis.add(distanceWalked);
		instance.stepToUpdatePher +=1;
		if (instance.stepToUpdatePher == 50 ) //New added
		{



			double minDisInGroup = instance.arrayDis.get(0);

			System.out.println("!!!!PHEROMONE UPDATING PROCESS!!!!");

			//System.out.println (minDisInGroup);

			double minPheremone = (ZZACOSolverGlobalUpdate.Q / (instance.arrayDis.get(0)));;
			int minID = 0;
			int numberOfChanges  = 0;
			for(int h3=1; h3 < instance.stepToUpdatePher; h3++){

				double phero = (ZZACOSolverGlobalUpdate.Q / (instance.arrayDis.get(h3)));//calculate pheromone for each ant in the group

				if (instance.arrayDis.get(h3) < minDisInGroup) {

					minDisInGroup = instance.arrayDis.get(h3);
					minID  = h3;
					minPheremone = phero;

					numberOfChanges ++;				


					//	System.out.println("HASSAAAAAAAAAAANNNNNNNNNNNNNNN");

				}

				//System.out.println("Khoobi?");
			}



			System.out.println("Updating the Min Dis In This Group" + "                 " + minDisInGroup);


			int previous = instance.arrayWays.get(minID)[0]; //update pheromone for a solution found by the best ant in this group
			for(int h = 1; h < instance.arrayWays.get(minID).length; h++){  
				instance.adjustPheromone(previous, instance.arrayWays.get(minID)[h], minPheremone);
				previous = instance.arrayWays.get(minID)[h];
			}





			//System.out.println ("==============");		
			System.out.println("We are Updating the bestKnown Solution!" + "                 " + 
					instance.bestKnown.distance);

			if (instance.bestKnown.distance < instance.arrayDis.get(minID)){

				System.out.println ("==============");


				double phero = (ZZACOSolverGlobalUpdate.Q / (instance.bestKnown.distance));

				int previousBest = instance.bestKnown.way[0];			

				for(int h = 1; h < instance.bestKnown.way.length; h++){  				

					instance.adjustPheromone(previousBest, instance.bestKnown.way[h], phero);
					previousBest = instance.bestKnown.way[h];
				}

			}



			groupCost = 0.0;
			instance.stepToUpdatePher = 0;
			instance.arrayWays.clear();
			instance.arrayDis.clear();

			/*for(int h2 = 1; h2 < instance.bestKnown.way.length-1; h2++){  				

				System.out.println(instance.pheromones[h2][h2+1]);

			}
			 */

		}


		//	System.out.println("Number of missed Nodes:" + nMissed);
		return new WalkedWay(way,distanceWalked);
	}

	public ArrayList<int[]> createSucPredEsp(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else //if (i-1!=0)
				{
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay [i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} else if (i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}
		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
		}


		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}


	public ArrayList<int[]> createSucPredCompleteSol(int[] parWay, int numNodes, int numVehicles) {
		ArrayList<int[]> variablesLists = new ArrayList<int[]>();
		int succesors[] = new int[numNodes + numVehicles];
		int predecessors[] = new int[numNodes + numVehicles];
		int visits[] = new int[numNodes + (numVehicles * 2)];
		int k =0;

		for (int i = 0; i <parWay.length; i++){

			if (parWay[i] != 0 && parWay [i] < numNodes){

				visits[parWay[i]] = k +1;
				if (parWay[i-1] != 0 && parWay[i-1]<numNodes){
					predecessors[parWay[i]] = parWay[i-1];
				}else {
					predecessors[parWay[i]] = numNodes+k;
					succesors[numNodes + k] = parWay[i];
				}
				if (i+1 < parWay.length && parWay[i+1] != 0 && parWay[i+1]< numNodes){
					succesors[parWay[i]] = parWay[i+1];        			
				} else if(i+1 == parWay.length || parWay[i+1] != 0){ // we have reached a depot ()
					succesors[parWay[i]] = numNodes+k;
					predecessors[numNodes+k] = parWay[i];
					k += 1;
				}
				else {//parWay[i+1] == 0 this is the end of the way
					assert (parWay[i+1] == 0);
					succesors[parWay[i]] = 0;
				}

			}

		}

		for (int j = 0; j < numVehicles; j++) {
			visits[numNodes + j] = j + 1;
			visits[numNodes + numVehicles + j] = j + 1;
			if (succesors[numNodes + j] == 0)
			{				
				succesors[numNodes + j] = numNodes + j;
			}
			if (predecessors[numNodes + j] == 0)
			{				
				predecessors[numNodes + j] = numNodes + j;
			}
		}




		variablesLists.add(0, visits);
		variablesLists.add(1, predecessors);
		variablesLists.add(2, succesors);

		return variablesLists;
	}
}

// TODO really needs improvement
/*public final int getNextProbableNode(LinkedList<LinkedList<Integer>> probableSucList, int idCurNode) {

	if (!probableSucList.isEmpty()) {
		int danglingUnvisited = -1;
		final double[] weights_firstNode = new double[visited.length];
		final double[] weights_secondNode = new double[visited.length];
		ArrayList<Integer> computableNodes_FistNode = new ArrayList<Integer>();
		ArrayList<Integer> computableNodes_SecondNode = new ArrayList<Integer>();//list of Nodes we can visit as successors
		ArrayList<Integer> reserveList = new ArrayList<Integer>(); 

		for (int j = 0; j < probableSucList.size(); j++){

			//if (probableSucList.get(j) < instance.inputs.getNodes().length){
			if (probableSucList.get(j).getFirst() < instance.inputs.getNodes().length){

				computableNodes_FistNode.add(probableSucList.get(j).getFirst());
				System.out.println(probableSucList.get(j).getFirst());

			}else{
				backtrackNode = idCurNode;
				System.out.println(probableSucList.get(j).getFirst());

			}

		}

		if (computableNodes_FistNode.isEmpty())
			return -1;	


		double sum_fistNode = 0.0d;


		for (Integer x : computableNodes_FistNode) {
			weights_firstNode[x] = calculateNonNormalizedProbability(x, way[idCurNode]);
			sum_fistNode += weights_firstNode[x];
			danglingUnvisited = x;
		}

		if (sum_fistNode == 0.0d)
			return danglingUnvisited;


		double probSum = 0.0d;
		double curRand = random.nextDouble();
		final double r = curRand*sum_fistNode;
		for (Integer x2 : computableNodes_FistNode) {

			probSum +=weights_firstNode[x2];

			if (r <= probSum) {

				for (int j = 0; j < probableSucList.size(); j++){						

					if (probableSucList.get(j).getFirst()== x2){

						if (probableSucList.get(j).getLast() < instance.inputs.getNodes().length){

							computableNodes_SecondNode.add(probableSucList.get(j).getLast());
							System.out.println(probableSucList.get(j).getLast());

						}else{
							backtrackNode = x2;
							System.out.println(probableSucList.get(j).getLast());

						}
					}

				}
				Integer firstMove = x2;

			}
		}

	}

	return -1;
}
 */




