package eclipse;

import com.parctechnologies.eclipse.*;

import java.io.*;
import java.net.InetAddress;

public class EclipseConnection {

	//	 Object representing the Eclipse process
	// private EclipseEngine eclipse;
	private RemoteEclipse remoteEclipse;
	
	// RemoteEclipse connection data
	private String hostName = "Negar-THINK";
	private int port = 51127;
	
	// Data going out from java
	private ToEclipseQueue java_to_eclipse;

	// Data coming in from eclipse
	private FromEclipseQueue eclipse_to_java;

	// EXDR translation of data going out from Java
	private EXDROutputStream java_to_eclipse_formatted;

	// EXDR translation of data coming in from eclipse
	private EXDRInputStream eclipse_to_java_formatted;
	
	
	public EclipseConnection() throws IOException, EclipseException{
		connect();
	}

	private void connect() throws IOException, EclipseException {
		if(System.getProperty("os.name").contains("Linux"))
			System.setProperty("eclipse.directory", "/usr/local/eclipse/");
		else if(System.getProperty("os.name").contains("Mac"))
			System.setProperty("eclipse.directory", "/Library/Eclipse/");
		else
			System.setProperty("eclipse.directory", "C:/Archivos de programa/ECLiPSe 6.0/");
		// Create some default Eclipse options
		EclipseEngineOptions eclipseEngineOptions = new EclipseEngineOptions();

				// Connect the Eclipse's standard streams to the JVM's
		eclipseEngineOptions.setUseQueues(false);

		// Initialise Eclipse
		//eclipse = EmbeddedEclipse.getInstance(eclipseEngineOptions);
		
		// Remote Connection
		remoteEclipse = new RemoteEclipse(
				InetAddress.getByName(hostName),port);
		
		// Create the two queues
		java_to_eclipse = remoteEclipse.getToEclipseQueue("java_to_eclipse");
		eclipse_to_java = remoteEclipse.getFromEclipseQueue("eclipse_to_java");

		// Set up the two formatting streams
		java_to_eclipse_formatted = new EXDROutputStream(java_to_eclipse);
		eclipse_to_java_formatted = new EXDRInputStream(eclipse_to_java);
	}

	public void destroy() throws IOException {
		// Destroy the Eclipse process
		//((EmbeddedEclipse) eclipse).destroy();
		remoteEclipse.disconnect();
	}
	
	public void resume() throws IOException {
		remoteEclipse.resume();
	}
	
	public EXDRInputStream getEclipseJavaQueue(){
		return eclipse_to_java_formatted;
	}
	
	public EXDROutputStream getJavaEclipseQueue(){
		return java_to_eclipse_formatted;
	}
	
	public void closeQueues() throws IOException {
		java_to_eclipse_formatted.close();
		eclipse_to_java_formatted.close();
	}

	public void compile(String nameFile) throws IOException, EclipseException {
		// Compile files
		remoteEclipse.compile(new File(nameFile));
	}

	public Object[] execute(String query) throws IOException, EclipseException {
		// Write a message
		CompoundTerm result = remoteEclipse.rpc(query);
		
		int argument = 1;
		Object[] resultArguments = new Object[result.arity()];
		while (argument <= result.arity())
			resultArguments[argument - 1] = result.arg(argument++);

		return resultArguments;
	}
	
	public void executeNoReturn(String query) throws IOException, EclipseException {
		remoteEclipse.rpc(query);
	}
	
	/**
	 * QueueListener which just writes everything to standard out.
	 */
	class OutputQL implements QueueListener {
		String name;
		EXDRInputStream eis;

		public OutputQL(String name, EXDRInputStream eis) {
			this.name = name;
			this.eis = eis;
		}

		public void dataAvailable(Object source) {
			try {
				while (((InputStream) source).available() > 0) {
					System.out.println(name + ": " + eis.readTerm());
				}
			} catch (IOException e) {
				System.err.println("IOException: " + e);
			}
		}

		public void dataRequest(Object source) {
		};
	}
}
