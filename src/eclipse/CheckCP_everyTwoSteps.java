package eclipse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import org.uoc.rgcws.Route;

import com.parctechnologies.eclipse.EclipseException;
import com.parctechnologies.eclipse.Fail;


public class CheckCP_everyTwoSteps {

	//private String check = "prolog/checkVRP";

	private static EclipseConnectionGettingFeedBack eclipse;

	public CheckCP_everyTwoSteps(String prbName1) {
		try {
			String prbName = prbName1;
			eclipse = new EclipseConnectionGettingFeedBack(prbName);
			//eclipse.compile(check);
			eclipse.resume();
		} catch (EclipseException eclException) {
			System.err.println("Couldn't establish a connection to ECLiPSe.");
			System.err.println(eclException.getMessage());
		} catch (IOException ioException) {
			System.err.println("Couldn't find the specified file.");
			System.err.println(ioException.getMessage());
		}
	}
	
	public void destroy(){
		try{
			eclipse.destroy();
		} catch (IOException ioexc){
			ioexc.printStackTrace();
		}
	}

	public boolean loadProblem(String problemName) {
		try {
			eclipse.executeNoReturn("loadProblem('" + problemName + "')");
			return true;
		} catch (EclipseException eclExcp) {
			return false;
		} catch (IOException ioExc) {
			return false;
		}
	}
	
	public boolean check(String problemName, String variablesToCheck){
		try{
			eclipse.executeNoReturn("check('" + problemName + "'," + variablesToCheck + ")");
			return true;
		} catch (Fail eclExcp) {
			return false;
		} catch(EclipseException eclipExc){
			return false;
		} catch (IOException ioExc) {
			System.err.println("Error checking: " + ioExc.getMessage());
			return false;
		}
	}
	
	// To be able to get strings instead of int in the Variables ToCheck array
	
	public class myInt {

		private Integer num;
		
		public myInt(Integer i) {
			num = i;
			// TODO Auto-generated constructor stub
		}
		@Override
		public String toString() {

			if (num == 0 || num == null){ 
				return  "_";

			}else{
				return num.toString();
			}

		}
	}

	
	@SuppressWarnings("unchecked")
	public LinkedList<LinkedList<Integer>> checkEsp(ArrayList<ArrayList<Integer>> variablesToCheck, int nodeToCheck, int curVehicle){
		try{
			for (ArrayList<Integer> arrayList : variablesToCheck) {
				for (int i = 0; i < arrayList.size(); i++) {
					if (arrayList.get(i) == 0)
						arrayList.set(i, null);
				}
			}			
			
			
			eclipse.getJavaEclipseQueue().write(variablesToCheck);
			ArrayList<Integer> currNode = new ArrayList<Integer>();
			currNode.add(nodeToCheck);
			eclipse.getJavaEclipseQueue().write(currNode);
			ArrayList<Integer> curVeh = new ArrayList<Integer>();
			curVeh.add(curVehicle);
			eclipse.getJavaEclipseQueue().write(curVeh);
			eclipse.getJavaEclipseQueue().flush();
		
			eclipse.resume();
			
			//LinkedList<Integer> resultCheck;
			LinkedList<LinkedList<Integer>> resultCheck;
			
			try{
			//	resultCheck = (LinkedList<Integer>) eclipse.getEclipseJavaQueue().readTerm();
				resultCheck = (LinkedList<LinkedList<Integer>>) eclipse.getEclipseJavaQueue().readTerm();
			//	System.out.println(resultCheck.toString());
			

			}
			catch(java.lang.ClassCastException e){
				//resultCheck = new LinkedList<Integer> ();
				resultCheck = new LinkedList<LinkedList<Integer>> ();
			}
//			System.out.println("Read from ECLiPSe: " + resultCheck.intValue());
			
			//if(resultCheck.intValue() == 1) 
			//	return true;
			//else return false;
			return resultCheck;

		} catch (IOException ioExc) {
			System.err.println("Error checking: " + ioExc.getMessage());
			System.exit(0);
//			return false;
			return new LinkedList<LinkedList<Integer>> ();
			
		}
	}
	
	public boolean check(ArrayList<ArrayList<Integer>> variablesToCheck ,int  checkingPoint, int curVehicle){
		try{
			
			for (ArrayList<Integer> arrayList : variablesToCheck) {
				for (int i = 0; i < arrayList.size(); i++) {
					if (arrayList.get(i) == 0)
						arrayList.set(i, null);
				}
			}
			
			eclipse.getJavaEclipseQueue().write(variablesToCheck);
			
			ArrayList<Integer> currNode = new ArrayList<Integer>();
			currNode.add(checkingPoint);
			eclipse.getJavaEclipseQueue().write(currNode);
			
			ArrayList<Integer> curVeh = new ArrayList<Integer>();
			curVeh.add(curVehicle);
			eclipse.getJavaEclipseQueue().write(curVeh);
			
			eclipse.getJavaEclipseQueue().flush();
		
			eclipse.resume();
			
			Integer resultCheck = (Integer) eclipse.getEclipseJavaQueue().readTerm();
//			System.out.println("Read from ECLiPSe: " + resultCheck.intValue());
			
			
			if(resultCheck.intValue() == 1) 
				return true;
			else return false;
			

		} catch (IOException ioExc) {
			System.err.println("Error checking: " + ioExc.getMessage());
			return false;
		}
	}
	
	public ArrayList<ArrayList<Integer>> getVariablesArrayList(ArrayList<int[]> variables){
		ArrayList<ArrayList<Integer>> variablesToCheckList = new ArrayList<ArrayList<Integer>>();
		
		int[] v = variables.get(0);
		int[] p = variables.get(1);
		int[] s = variables.get(2);
		
		variablesToCheckList.add(new ArrayList<Integer>());
		variablesToCheckList.add(new ArrayList<Integer>());
		variablesToCheckList.add(new ArrayList<Integer>());		
		
		for(int i=1; i < v.length; i++) variablesToCheckList.get(0).add(new Integer(v[i]));
		for(int i=1; i < p.length; i++) variablesToCheckList.get(1).add(new Integer(p[i]));
		for(int i=1; i < s.length; i++) variablesToCheckList.get(2).add(new Integer(s[i]));
		
		return variablesToCheckList;
	}

	
	public ArrayList<ArrayList<myInt>> getVariablesArrayListEsp(ArrayList<int[]> variables){
		ArrayList<ArrayList<myInt>> variablesToCheckList = new ArrayList<ArrayList<myInt>>();
		
		int[] v = variables.get(0);
		int[] p = variables.get(1);
		int[] s = variables.get(2);
		
		variablesToCheckList.add(new ArrayList<myInt>());
		variablesToCheckList.add(new ArrayList<myInt>());
		variablesToCheckList.add(new ArrayList<myInt>());		
		
		for(int i=1; i < v.length; i++) variablesToCheckList.get(0).add(new myInt(v[i]));
		for(int i=1; i < p.length; i++) variablesToCheckList.get(1).add(new myInt(p[i]));
		for(int i=1; i < s.length; i++) variablesToCheckList.get(2).add(new myInt(s[i]));
		
		return variablesToCheckList;
	}
}
