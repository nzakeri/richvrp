package eclipse;

import com.parctechnologies.eclipse.*;

import java.io.*;
import java.net.InetAddress;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;




public class EclipseConnectionGettingFeedBack {

	//	 Object representing the Eclipse process
	// private EclipseEngine eclipse;
	private RemoteEclipse remoteEclipse;

	// RemoteEclipse connection data
	private String hostName ;
	private int port ;

	// Data going out from java
	private ToEclipseQueue java_to_eclipse;

	// Data coming in from eclipse
	private FromEclipseQueue eclipse_to_java;

	// EXDR translation of data going out from Java
	private EXDROutputStream java_to_eclipse_formatted;

	// EXDR translation of data coming in from eclipse
	private EXDRInputStream eclipse_to_java_formatted;


	public EclipseConnectionGettingFeedBack(String pName) throws IOException, EclipseException{
		connect(pName);

	}

	private void connect(String probName) throws IOException, EclipseException {
		if(System.getProperty("os.name").contains("Linux"))
			System.setProperty("eclipse.directory", "/usr/local/eclipse/");
		else if(System.getProperty("os.name").contains("Mac"))
			System.setProperty("eclipse.directory", "/Library/Eclipse/");
		else
			System.setProperty("eclipse.directory", "C:/Archivos de programa/ECLiPSe 6.1/");
		// Create some default Eclipse options
		EclipseEngineOptions eclipseEngineOptions = new EclipseEngineOptions();

		// Connect the Eclipse's standard streams to the JVM's
		eclipseEngineOptions.setUseQueues(false);

		// Initialise Eclipse
		//eclipse = EmbeddedEclipse.getInstance(eclipseEngineOptions);

		// Remote Connection
		Parsing pars = new Parsing();
		String[] result = pars.parse(probName);
		hostName = result[0];
		port = Integer.parseInt(result[1]);
		remoteEclipse = new RemoteEclipse(
				InetAddress.getByName(hostName),port);

		// Create the two queues
		java_to_eclipse = remoteEclipse.getToEclipseQueue("java_to_eclipse");
		eclipse_to_java = remoteEclipse.getFromEclipseQueue("eclipse_to_java");

		// Set up the two formatting streams
		java_to_eclipse_formatted = new EXDROutputStream(java_to_eclipse);
		eclipse_to_java_formatted = new EXDRInputStream(eclipse_to_java);
	}

	public void destroy() throws IOException {
		// Destroy the Eclipse process
		//((EmbeddedEclipse) eclipse).destroy();
		remoteEclipse.disconnect();
	}

	public void resume() throws IOException {
		//	System.out.println("STARTING RESUME");
		remoteEclipse.resume();
		//	System.out.println("FINISHED RESUME");
	}

	public EXDRInputStream getEclipseJavaQueue(){
		return eclipse_to_java_formatted;
	}

	public EXDROutputStream getJavaEclipseQueue(){
		return java_to_eclipse_formatted;
	}

	public void closeQueues() throws IOException {
		java_to_eclipse_formatted.close();
		eclipse_to_java_formatted.close();
	}

	public void compile(String nameFile) throws IOException, EclipseException {
		// Compile files
		remoteEclipse.compile(new File(nameFile));
	}

	public Object[] execute(String query) throws IOException, EclipseException {
		// Write a message
		CompoundTerm result = remoteEclipse.rpc(query);

		int argument = 1;
		Object[] resultArguments = new Object[result.arity()];
		while (argument <= result.arity())
			resultArguments[argument - 1] = result.arg(argument++);

		return resultArguments;
	}

	public void executeNoReturn(String query) throws IOException, EclipseException {
		remoteEclipse.rpc(query);
	}

	/**
	 * QueueListener which just writes everything to standard out.
	 */
	class OutputQL implements QueueListener {
		String name;
		EXDRInputStream eis;

		public OutputQL(String name, EXDRInputStream eis) {
			this.name = name;
			this.eis = eis;
		}

		public void dataAvailable(Object source) {
			try {
				while (((InputStream) source).available() > 0) {
					System.out.println(name + ": " + eis.readTerm());
				}
			} catch (IOException e) {
				System.err.println("IOException: " + e);
			}
		}

		public void dataRequest(Object source) {

		}

	}



	class Parsing {

		public String[] parse(String pName) throws IOException {
			String line;
			String[] resultSplit = new String[2];
			OutputStream stdin = null;
			InputStream stderr = null;
			InputStream stdout = null;

			try {
				// launch EXE and grab stdin/stdout and stderr

				String[] envVar = new String [0]; // unused
				File workingDir = new File("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\prolog\\connectionACOwithCP\\cvrp");
				//File workingDir = new File("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\prolog\\CPAIOR2015\\cvrp");
				//File workingDir = new File("K:\\RichVRP\\SR-GCWS-CS_CP_v2\\prolog\\pickup_delivery");
			//	Process p = Runtime.getRuntime().exec("C:\\Program Files\\ECLiPSe 6.1\\lib\\i386_nt\\eclipse.exe", envVar,workingDir);
				Process p = Runtime.getRuntime().exec("C:\\Program Files\\ECLiPSe 6.1\\lib\\x86_64_nt\\eclipse.exe", envVar,workingDir);
				stdin = p.getOutputStream ();
				stderr = p.getErrorStream ();
				stdout = p.getInputStream ();
				//System.out.println("Negar");

				BufferedReader brErr=
						new BufferedReader (new InputStreamReader (stderr));
				Thread tErr = new Thread(new MessageLoop(brErr));
				tErr.start();


				line = "compile('checkVRP')." + "\n";
				stdin.write(line.getBytes() );
				stdin.flush();


				//String problemName= pName.replace('-', '_');
				String problemName= pName;

				line = "establish_connection('"+problemName+"')." + "\n";
				stdin.write(line.getBytes() );
				stdin.flush();

				//   stdin.close();
				// clean up if any output in stdout
				BufferedReader brstdout=
						new BufferedReader (new InputStreamReader (stdout));



				while ((line = brstdout.readLine ()) != null) {
					System.out.println(line);	
					if (line.contains("Socket created at address")){
						//System.out.println ("[Stdout] " + line);
						String[] splitVar1 = line.split("\\/");
						String parsPort = splitVar1[1];
						System.out.println ("[Stdout] " + parsPort);
						String subLine = splitVar1[0];
						String[] splitVar2 = subLine.split("address");
						String parsHost = splitVar2[1];
						System.out.println("[Stdout] " + parsHost);
						resultSplit[0] = parsHost.trim();
						resultSplit[1] = parsPort.trim();

						Thread t = new Thread(new MessageLoop(brstdout));
						t.start();


						break;
					}
				}


				//brCleanUp.close();
				// clean up if any output in stderr
				//brCleanUp =
				//  new BufferedReader (new InputStreamReader (stderr));
				//while ((line = brCleanUp.readLine ()) != null) {
				//  //System.out.println ("[Stderr] " + line);
				//}
				//brCleanUp.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return resultSplit;

		}



	}


	private static class MessageLoop
	implements Runnable  {
		private BufferedReader br2;

		public MessageLoop(BufferedReader br) {
			// TODO Auto-generated constructor stub
			this.br2 = br;
		}
		public void run() {

			String line;
			try {
				while ((line = br2.readLine ()) != null) {
					System.out.println ("[Stdout] " + line);
					Thread.sleep(10);
					if (line.contains("Use \"halt.\" to exit ECLiPSe."))
						break;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}


}
