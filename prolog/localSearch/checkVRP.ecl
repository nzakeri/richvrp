% VRPTW model using CP

% Libraries
:-lib(ic).
:-lib(ic_global).
:-lib(branch_and_bound).
:-lib(viewable).
:-lib(matrix_util).

:-['auxiliars.ecl'].
:-['constraintsTW2.ecl'].
:-['heuristics.ecl'].


%-------------------------------------------------------------------------------

dummy.
dummy :-
	dummy.

%-------------------------------------------------------------------------------

loadProblem(ProblemName) :-
	compile(ProblemName).

%-------------------------------------------------------------------------------

%check(ProblemName,V,P,S) :-
check1(ProblemName) :-
	compile(ProblemName),
%	maxDistLength(MDL),
	writeln("Hello Negar Khanoom!"),
	visits(V),
	successor(S),
	predecessor(P),
	variables([V,Q,T,D,R,Qv,P,S],[N,M]),
	domains([V,Q,T,D,R,Qv,P,S],[N,M]),
	writeln(N),
	writeln(M),
	constraints([V,Q,_,D,R,Qv,P,S],[N,M]),
%	constraintsHTW([V,Q,T,D,R,Qv,P,S],[N,M]),
%	objective([V,Q,_,D,R,Qv,P,S],[N,M],_).
	objective([V,Q,_,D,R,Qv,P,S],[N,M],Cost),
	
	writeln(V), writeln(P),writeln(S),writeln(T).
	
%-------------------------------------------------------------------------------

%Local Search
localSearch(ProblemName) :-
	compile(ProblemName),
	visits(V),
	successor(S),
	predecessor(P),
	variables([V,Q,T,D,R,Qv,P,S],[N,M]),
	domains([V,Q,T,D,R,Qv,P,S],[N,M]),
	constraints([V,Q,_,D,R,Qv,P,S],[N,M]),
	
	objective([V,Q,_,D,R,Qv,P,S],[N,M],Cost),
	optimise([V,Q,T,_,_,_,P,S],[_,_],Cost,B),
	writeln(V), writeln(P),writeln(S).
	
	
	
	
%-------------------------------------------------------------------------------
check2(ProblemName) :-
	compile(ProblemName),
	visits(V),
	successor(S),
	predecessor(P),
%	variables([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]),
	variables([V,Q,T,D,R,Qv,P,S],[N,M]),
%	domains([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]),
	domains([V,Q,T,D,R,Qv,P,S],[N,M]),
	constraints([V,Q,_,D,R,Qv,P,S],[N,M]),
%	constraintsPickDelivery([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]),
	collection_to_list(V, List),
	bag_create(BAG1),
	
	(
		( 
			ith(1, S, Snode1),
			% writeln(S),
			indomain(Snode1),
			writeln(Snode1),
			ith(Snode1, V, Vnode1),			
			Vnode1 = 1,
			ith(Snode1, P, Pnode1),
			Pnode1 = 1,
			(Snode1#=< N ->	
				ith(Snode1, S, Snode2),
				indomain(Snode2),
				writeln([Snode1,Snode2]),
				ith(Snode2, V, Vnode2),
				Vnode2 = 1,
	%			ith(Snode2, P, Pnode2),
	%			Pnode2 = Snode1,			
				bag_enter(BAG1,[Snode1,Snode2])
				
				;
				
				bag_enter(BAG1,[Snode1,0]),
				writeln([Snode1,Snode2])
			),
			
			fail
		);
		true
	),	
	bag_dissolve(BAG1, SOL1),
	
	writeln(SOL1),	
	objective([V,Q,_,D,R,Qv,P,S],[N,M],Cost),
%	optimise([V,Q,T,_,_,_,P,S],[_,_],Cost,B),
	writeln(V), writeln(P),writeln(S),writeln(T).	

%-------------------------------------------------------------------------------

check_remote(ProblemName) :-
	block(establish_connection(ProblemName),T,writeln(T)).
	
establish_connection(ProblemName):-
	compile(ProblemName),
	writeln('HELLO!'),
	remote_connect(_,_,_),
%	maxDistLength(MDL),
	variables([V,Q,T,D,R,Qv,P,S],[N,M]),
	domains([V,Q,T,D,R,Qv,P,S],[N,M]),
	writeln(N),
	constraints([V,Q,_,D,R,Qv,P,S],[N,M]),
%	constraintsHTW([V,Q,T,D,R,Qv,P,S],[N,M]),
	writeln('domains set!!!!'),
%	constraintsDist([V,Q,_,D,R,Qv,P,S],[N,M],MDL),

	writeln('Constraints stored in memory...'),

	dummy,
	remote_yield(peer1),

	set_stream(warning_output,null),	
		

	read_exdr(java_to_eclipse, [A,B,C]),
	read_exdr(java_to_eclipse, [NODE]),	
	read_exdr(java_to_eclipse, [CURVEH]),
	read_exdr(java_to_eclipse, [LS]),
%	writeln(LS),

	(LS#\= 1 ->
		(NODE#\= 9999 ->
		
			bag_create(BAG),

			ith(NODE, S, Snode1),		

			V = A,
			P = B,
			S = C,		
			
			(			
				(			
				indomain(Snode1),
				ith(Snode1, V, Vnode1),
				Vnode1 = CURVEH,
				ith(Snode1, P, Pnode1),
				Pnode1 = NODE,
				(Snode1#=< N ->
					ith(Snode1, S, Snode2),								
					indomain(Snode2),			
					ith(Snode2, V, Vnode2),
					Vnode2 = Vnode1,
					ith(Snode2, P, Pnode2),
					Pnode2 = Snode1,
					bag_enter(BAG,[Snode1,Snode2])
					
					;
					
					bag_enter(BAG,[Snode1,0])
	%				,writeln([Snode1,Snode2])
				),
						
				fail
				);
				
			true
			),
			
			bag_dissolve(BAG, SOL),
	%		writeln(SOL),
			
			write_exdr(eclipse_to_java,SOL)
			
			
			
		;	
			
			
			(  compare_lists([V,P,S],[A,B,C]) ->
				J is 1
				

			;
				J is 0				
				
				
			),	
			
		
			write_exdr(eclipse_to_java,J)
		
		)
	;
	
	V = A,
	P = B,
	S = C,	
	writeln('NNNNEEEGGAAARRRR!!!!'),
	objective([V,Q,_,D,R,Qv,P,S],[N,M],Cost),
	optimise([V,Q,T,_,_,_,P,S],[_,_],Cost,H),
	write_exdr(eclipse_to_java,[V,P,S])
	),
	
	flush(eclipse_to_java),		
	fail.
	
compare_lists([V,P,S],[A,B,C]):-
	V = A,
	P = B,
	S = C.

%-------------------------------------------------------------------------------	
%-------------------------------------------------------------------------------		
variables([V,Q,T,D,R,Qv,P,S],[N,M]):-
	readCustomers(N,M,R),   
	readVehicles(M,Qv),  % Vehicles
	Vdim is N + 2 * M,
	P_SN is N + M,
	length(V,Vdim),       % Visits
	length(Q,Vdim),       % Cummulative capacities
	length(T,Vdim),       % Cummulative times
	length(D,Vdim),       % Cummulative distances
	length(P,P_SN),     % Predecessors
	length(S,P_SN).     % Successors

%-------------------------------------------------------------------------------

domains([V,Q,T,D,_R,Qv,P,S],[_N,M]):-
	decompose(V,M,_,_,V_FL,_,_),
%	decompose2(V,N,V_L,_),
	V_FL #:: 1..M,
	(ic: max(Qv,Max)),
	Q #:: 0..Max,      % Cummulative capacities	
	(foreach(Ti,T) do Ti #:: 0..10000),      % Cummulative times
    (foreach(Di,D) do Di #>= 0),      % Cummulative distances
    length(P,P_SN),	
    P #:: 1..P_SN,
	S #:: 1..P_SN.

%-------------------------------------------------------------------------------	
	
	
%-------------------------------------------------------------------------------



% CVRP's predicate

constraints([V,Q,_,D,R,Qv,P,S],[N,M]):-
    constraintsCiclicRoutes(V,M),
	constraintsDifferent(P,S,N,M),
	constraintsCoherence(P,S),
	constraintsPath(V,P,S,M),
	constraintsCapacity(Q,R,P,S,M),
	constraintsMaxCapacity(Q,V,Qv,M),
	constraintsCapacityInRoute(V,Qv,R,N,M),
%	constraintsTime(T,P,S,M),
%	constraintsSymmetriesVisits(V,M),
	constraintsHamiltonianPath(P,S,N,M).
%	constraintsDistance(D,P,S,M).
	
constraintsDist([V,Q,_,D,R,Qv,P,S],[N,M],MDL):-
    constraintsCiclicRoutes(V,M),
	constraintsDifferent(P,S,N,M),
	constraintsCoherence(P,S),
	constraintsPath(V,P,S,M),
	constraintsCapacity(Q,R,P,S,M),
	constraintsMaxCapacity(Q,V,Qv,M),
	constraintsCapacityInRoute(V,Qv,R,N,M),
%	constraintsTime(T,P,S,M),
%	constraintsSymmetriesVisits(V,M),
	constraintsHamiltonianPath(P,S,N,M),
    constraintsDistance(D,P,S,M),
	(foreach(Di,D),param(MDL) do 
		Di #=< MDL).	
		
		

% STW's predicate

constraintsSTW([V,Q,T,D,R,Qv,P,S],[N,M],DeltaA,DeltaB,DeltaDep):-	
    constraintsCiclicRoutes(V,M),
    constraintsDifferent(P,S,N,M),
    constraintsCoherence(P,S),
    constraintsPath(V,P,S,M),
    constraintsCapacity(Q,R,P,S,M),
    constraintsMaxCapacity(Q,V,Qv,M),
    constraintsCapacityInRoute(V,Qv,R,N,M),
    constraintsTime(T,P,S,M),
    constraintsSoftTimeWindows(T,[N,M],DeltaA,DeltaB,DeltaDep),
    constraintsSymmetriesVisits(V,M),
    constraintsHamiltonianPath(P,S,N,M),
    constraintsDistance(D,P,S,M).
    
    
% HTW's predicate
    
constraintsHTW([V,Q,T,D,R,Qv,P,S],[N,M]):-
    constraintsCiclicRoutes(V,M),
    constraintsDifferent(P,S,N,M),
    constraintsCoherence(P,S),
    constraintsPath(V,P,S,M),
    constraintsCapacity(Q,R,P,S,M),
	constraintsMaxCapacity(Q,V,Qv,M),
    constraintsCapacityInRoute(V,Qv,R,N,M),
    constraintsTime(T,P,S,M),
	constraintsHardTimeWindowsAtTheBeginningOfVisit(T,[N,M]),
%	constraintsHardTimeWindows(T,[N,M]),
%   constraintsSymmetriesVisits(V,M),
    constraintsHamiltonianPath(P,S,N,M).
%   constraintsDistance(D,P,S,M).

% PickUp&Delivery		
		
constraintsPickDelivery([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]):-
	constraintsCiclicRoutes(V,M),
    constraintsDifferent(P,S,N,M),
    constraintsCoherence(P,S),
    constraintsPath(V,P,S,M),
    constraintsCapacity(Q,R,P,S,M),
    constraintsMaxCapacity(Q,V,Qv,M),
    constraintsCapacityInRoute(V,Qv,R,N,M),	
	constraintPickUp(_,M,P,S,O),
	constraintPickUp2(Pairs,O,V),
	constraintTimePickUp(M,T,Pairs),
%	constraintsHardTimeWindowsAtTheBeginningOfVisit(T,[N,M]),
    constraintsTime(T,P,S,M).
%    constraintsSymmetriesVisits(V,M),
%    constraintsHamiltonianPath(P,S,N,M).
%    constraintsDistance(D,P,S,M).


%-------------------------------------------------------------------------------


objective([_,_,T,_,_,_,_,_],[_,M],DeltaA,DeltaB,DeltaDep,Cost):-
    costFunction(T,M,DeltaA,DeltaB,DeltaDep,Cost).

objective([_,_,_,D,_,_,_,_],[_,M],Cost):-
    costFunction(D,M,Cost).
    
objectiveDistance([_,_,_,D,_,_,_,_],[_,M],DeltaA,DeltaB,DeltaDep,Cost,CostDistance):-
    costDistance(D,M,CostDistance,DeltaA,DeltaB,DeltaDep,Cost).

%-------------------------------------------------------------------------------

optimise([V,Q,T,_,_,_,P,S],[_,_],Cost,B):-
    viewable_create(p_s, [P,S]),
    viewable_create(v_q_t, [V,Q,T]),
    nearestNeighbourHeuristic(S,Distances),
    flatten([V,Distances,P],VARSflat),
    bb_min((search(VARSflat,0,smallest,indomain_min,complete,[backtrack(B)]),indomain(Cost)),Cost,bb_options{strategy:continue,timeout:100000.01}).
