% VRPTW model using CP

% Libraries
:-lib(ic).
:-lib(ic_global).
:-lib(branch_and_bound).
:-lib(viewable).
:-lib(matrix_util).

:-['auxiliars.ecl'].
:-['constraintsTW.ecl'].
:-['heuristics.ecl'].


%-------------------------------------------------------------------------------

dummy.
dummy :-
	dummy.

%-------------------------------------------------------------------------------

loadProblem(ProblemName) :-
	compile(ProblemName).

%-------------------------------------------------------------------------------

check(ProblemName,V,P,S) :-
	compile(ProblemName),
	variables([V,Q,_,D,R,Qv,P,S],[N,M]),
	domains([V,Q,_,D,R,Qv,P,S],[N,M]),
	constraints([V,Q,_,D,R,Qv,P,S],[N,M]),
	objective([V,Q,_,D,R,Qv,P,S],[N,M],_).

%-------------------------------------------------------------------------------

check_remote(ProblemName) :-
	block(establish_connection(ProblemName),T,writeln(T)).
	
establish_connection(ProblemName):-
	compile(ProblemName),
	writeln('HELLO!'),
	remote_connect(_,_,_),
	writeln('HELLO AGAIN!'),
%	maxDistLength(MDL),
	variables([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]),	
	domains([V,Q,T,D,_R,Qv,P,S,O,_Pairs],[N,M]),
%	constraintsDist([V,Q,_,D,R,Qv,P,S],[N,M],MDL),
%	constraintsHTW([V,Q,T,D,R,Qv,P,S],[N,M]),
%	constraints([V,Q,_,D,R,Qv,P,S],[N,M]),
	constraintsPickDelivery([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]),
	objective([V,Q,_,D,R,Qv,P,S],[N,M],_),
	writeln('Constraints stored in memory...'),
	set_stream(warning_output,null),
	dummy,
	remote_yield(peer1),
	read_exdr(java_to_eclipse, [A,B,C]),
	(  compare_lists([V,P,S],[A,B,C]) ->
		J is 1
	;
		J is 0
	),
	% writeln(V), writeln(P),writeln(S),
	write_exdr(eclipse_to_java,J),
	flush(eclipse_to_java),
	fail.
	
compare_lists([V,P,S],[A,B,C]) :-
	V = A,
	P = B,
	S = C.

%-------------------------------------------------------------------------------
variables([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]):-
	readCustomers(N,M,R),    
	readVehicles(M,Qv),
	pairs(Pairs),   % Vehicles
	time_windows(TW),
	Vdim is N + 2 * M,
	P_SN is N + M,
	length(V,Vdim),       % Visits
	length(Q,Vdim),       % Cummulative capacities
	length(T,Vdim),       % Cummulative times
	length(D,Vdim),       % Cummulative distances
	length(P,P_SN),     % Predecessors
	length(S,P_SN),     % Successors
	length(O,Vdim).	% Sequence variable
	

%-------------------------------------------------------------------------------

domains([V,Q,T,D,_R,Qv,P,S,O,_Pairs],[N,M]):-
    decompose(V,M,_,_,V_FL,_,_),
    V_FL #:: 1..M,
    (ic: max(Qv,Max)),
    Q #:: 0..Max,      % Cummulative capacities
    (foreach(Ti,T) do Ti #>= 0),      % Cummulative times
    (foreach(Di,D) do Di #>= 0),      % Cummulative distances
    length(P,P_SN),
    P #:: 1..P_SN,
    S #:: 1..P_SN,
    Vdim is N + 2 * M,
    O #::1..Vdim.

%-------------------------------------------------------------------------------
% CVRP's predicate

constraints([V,Q,_,D,R,Qv,P,S],[N,M]):-
    constraintsCiclicRoutes(V,M),
    constraintsDifferent(P,S,N,M),
    constraintsCoherence(P,S),
    constraintsPath(V,P,S,M),
    constraintsCapacity(Q,R,P,S,M),
    constraintsMaxCapacity(Q,V,Qv,M),
    constraintsCapacityInRoute(V,Qv,R,N,M),
%    constraintsTime(T,P,S,M),
%    constraintsSymmetriesVisits(V,M),
%    constraintsHamiltonianPath(P,S,N,M),
    constraintsDistance(D,P,S,M).
	
% Distance
	
constraintsDist([V,Q,_,D,R,Qv,P,S],[N,M],MDL):-
    constraintsCiclicRoutes(V,M),
	constraintsDifferent(P,S,N,M),
	constraintsCoherence(P,S),
	constraintsPath(V,P,S,M),
	constraintsCapacity(Q,R,P,S,M),
	constraintsMaxCapacity(Q,V,Qv,M),
	constraintsCapacityInRoute(V,Qv,R,N,M),
%	constraintsTime(T,P,S,M),
%	constraintsSymmetriesVisits(V,M),
%	constraintsHamiltonianPath(P,S,N,M).
    constraintsDistance(D,P,S,M),
	(foreach(Di,D),param(MDL) do 
		Di #=< MDL).	

		
% PickUp&Delivery		
		
constraintsPickDelivery([V,Q,T,D,R,Qv,P,S,O,Pairs],[N,M]):-
	constraintsCiclicRoutes(V,M),
    constraintsDifferent(P,S,N,M),
    constraintsCoherence(P,S),
    constraintsPath(V,P,S,M),
    constraintsCapacity(Q,R,P,S,M),
    constraintsMaxCapacity(Q,V,Qv,M),
    constraintsCapacityInRoute(V,Qv,R,N,M),	
	constraintPickUp(_,M,P,S,O),
	constraintPickUp2(Pairs,O,V),
	constraintTimePickUp(M,T,Pairs).
%	constraintsHardTimeWindowsAtTheBeginningOfVisit(T,[N,M]),
%    constraintsTime(T,P,S,M).
%    constraintsSymmetriesVisits(V,M),
%    constraintsHamiltonianPath(P,S,N,M),
%    constraintsDistance(D,P,S,M).
		

% STW's predicate

constraintsSTW([V,Q,T,D,R,Qv,P,S],[N,M],DeltaA,DeltaB,DeltaDep):-	
    constraintsCiclicRoutes(V,M),
    constraintsDifferent(P,S,N,M),
    constraintsCoherence(P,S),
    constraintsPath(V,P,S,M),
    constraintsCapacity(Q,R,P,S,M),
    constraintsMaxCapacity(Q,V,Qv,M),
    constraintsCapacityInRoute(V,Qv,R,N,M),
    constraintsTime(T,P,S,M),
    constraintsSoftTimeWindows(T,[N,M],DeltaA,DeltaB,DeltaDep),
    constraintsSymmetriesVisits(V,M),
    constraintsHamiltonianPath(P,S,N,M),
    constraintsDistance(D,P,S,M).
    
    
% HTW's predicate
    
constraintsHTW([V,Q,T,D,R,Qv,P,S],[N,M]):-
    constraintsCiclicRoutes(V,M),
    constraintsDifferent(P,S,N,M),
    constraintsCoherence(P,S),
    constraintsPath(V,P,S,M),
    constraintsCapacity(Q,R,P,S,M),
    constraintsMaxCapacity(Q,V,Qv,M),
    constraintsCapacityInRoute(V,Qv,R,N,M),
    constraintsTime(T,P,S,M),
%	constraintsTimeAtTheBeginningOfVisit(T,P,S,M),
	constraintsHardTimeWindowsAtTheBeginningOfVisit(T,[N,M]),
%	constraintsHardTimeWindows(T,[N,M]),
    constraintsSymmetriesVisits(V,M),
    constraintsHamiltonianPath(P,S,N,M),
    constraintsDistance(D,P,S,M).

%-------------------------------------------------------------------------------

objective([_,_,T,_,_,_,_,_],[_,M],DeltaA,DeltaB,DeltaDep,Cost):-
    costFunction(T,M,DeltaA,DeltaB,DeltaDep,Cost).

objective([_,_,_,D,_,_,_,_],[_,M],Cost):-
    costFunction(D,M,Cost).
    
objectiveDistance([_,_,_,D,_,_,_,_],[_,M],DeltaA,DeltaB,DeltaDep,Cost,CostDistance):-
    costDistance(D,M,CostDistance,DeltaA,DeltaB,DeltaDep,Cost).

%-------------------------------------------------------------------------------

optimise([V,Q,T,_,_,_,P,S],[_,_],Cost,B):-
    viewable_create(p_s, [P,S]),
    viewable_create(v_q_t, [V,Q,T]),
    nearestNeighbourHeuristic(S,Distances),
    flatten([V,Distances,P],VARSflat),
    bb_min((search(VARSflat,0,smallest,indomain_min,complete,[backtrack(B)]),indomain(Cost)),Cost,bb_options{strategy:continue}).
