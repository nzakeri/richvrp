customers([-23,
-20,
-9,
-10,
20,
-5,
20,
10,
20,
-20,
-10,
20,
10,
10,
-10,
20,
-9,
-10,
40,
10,
10,
-10,
-10,
-20,
20,
30,
20,
-10,
-20,
10,
-30,
10,
10,
-10,
20,
-20,
-10,
-10,
-30,
30,
-20,
10,
20,
10,
10,
10,
10,
-40,
10,
-14,
-18,
-23,
-27,
-8,
16,
-13,
23,
20,
-13,
-19,
9,
3,
6,
-6,
-6,
16,
9,
-10,
27,
23,
-3,
8,
5,
8,
-15,
31,
-35,
27,
-27,
13,
-9,
-20,
-31,
18,
2,
-20,
13,
19,
-2,
6,
13,
14,
-13,
23,
6,
-6,
35,
9,
15,
-20,
-10,
-8,
-20,
-16,
-23,
-16,
-10,
-10]).

coords([[25,85],
[22,75],
[22,85],
[20,80],
[20,85],
[18,75],
[15,75],
[15,80],
[10,35],
[10,40],
[8,40],
[8,45],
[5,35],
[5,45],
[2,40],
[0,40],
[0,45],
[44,5],
[42,10],
[42,15],
[40,5],
[40,15],
[38,5],
[38,15],
[35,5],
[95,30],
[95,35],
[92,30],
[90,35],
[88,30],
[88,35],
[87,30],
[85,25],
[85,35],
[67,85],
[65,85],
[65,82],
[62,80],
[60,80],
[60,85],
[58,75],
[55,80],
[55,85],
[55,82],
[20,82],
[18,80],
[2,45],
[42,5],
[42,12],
[72,35],
[55,20],
[25,30],
[20,50],
[55,60],
[30,60],
[50,35],
[30,25],
[15,10],
[10,20],
[15,60],
[45,65],
[65,35],
[65,20],
[45,30],
[35,40],
[41,37],
[64,42],
[40,60],
[31,52],
[35,69],
[65,55],
[63,65],
[2,60],
[20,20],
[5,5],
[60,12],
[23,3],
[8,56],
[6,68],
[47,47],
[49,58],
[27,43],
[37,31],
[57,29],
[63,23],
[21,24],
[12,24],
[24,58],
[67,5],
[37,47],
[49,42],
[53,43],
[61,52],
[57,48],
[56,37],
[55,54],
[4,18],
[26,52],
[26,35],
[31,67],
[20,82],
[20,20],
[15,10],
[30,60],
[57,48],
[41,37],
[87,30],
[5,35],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50],
[40,50]]).

vehicles([200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200,
200]).

service_times([10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
0,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
0,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
0,
10,
10,
10,
10,
10,
10,
10,
10,
10,
0,
10,
10,
0,
10,
10,
10,
10,
10,
10,
10,
0,
10,
10,
10,
10,
10,
10,
10,
0,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
0,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10,
10]).

time_windows([[0,191],
[0,199],
[0,190],
[141,171],
[0,189],
[0,196],
[0,194],
[91,121],
[91,121],
[0,198],
[59,89],
[0,197],
[142,172],
[0,194],
[58,88],
[0,188],
[0,189],
[0,184],
[0,189],
[0,194],
[0,185],
[0,195],
[65,95],
[0,194],
[154,184],
[0,171],
[0,172],
[0,174],
[0,177],
[0,178],
[0,179],
[0,178],
[0,178],
[0,182],
[0,185],
[0,186],
[0,189],
[75,105],
[0,193],
[0,189],
[0,199],
[0,196],
[0,191],
[64,94],
[0,192],
[0,192],
[0,191],
[0,184],
[0,191],
[0,194],
[0,196],
[0,205],
[91,121],
[0,211],
[140,170],
[0,211],
[0,203],
[152,182],
[0,187],
[0,203],
[0,214],
[52,82],
[0,190],
[0,209],
[0,218],
[0,216],
[70,100],
[0,220],
[41,71],
[0,210],
[0,204],
[0,202],
[0,190],
[141,171],
[0,172],
[75,105],
[150,180],
[0,197],
[89,119],
[0,222],
[0,217],
[0,215],
[0,210],
[96,126],
[0,194],
[0,197],
[90,120],
[0,212],
[0,177],
[0,225],
[0,217],
[14,44],
[0,208],
[0,212],
[0,209],
[0,214],
[0,181],
[0,215],
[77,107],
[180,210],
[0,192],
[141,171],
[152,182],
[140,170],
[0,212],
[0,216],
[0,178],
[142,172],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240],
[0,240]]).

pairs([[5,100],
[7,2],
[8,68],
[9,86],
[12,82],
[13,108],
[14,15],
[16,10],
[19,48],
[20,22],
[21,18],
[25,24],
[26,31],
[27,29],
[30,34],
[32,107],
[33,28],
[35,41],
[40,39],
[42,37],
[43,36],
[44,38],
[45,101],
[46,4],
[47,11],
[49,23],
[55,104],
[57,52],
[58,103],
[61,3],
[62,71],
[63,64],
[66,106],
[67,81],
[69,53],
[70,1],
[72,54],
[73,6],
[74,102],
[76,83],
[78,79],
[80,56],
[84,51],
[85,89],
[87,59],
[88,60],
[90,65],
[91,93],
[92,50],
[94,105],
[95,96],
[97,77],
[98,17],
[99,75]]).