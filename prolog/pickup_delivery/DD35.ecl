customers([9,
5,
16,
20,
10,
20,
6,
18,
7,
9,
8,
18,
12,
12,
15,
7,
14,
14,
19,
7,
7,
6,
11,
7,
19,
11,
16,
8,
13,
12,
5,
15,
8,
11,
16,
-9,
-5,
-16,
-20,
-10,
-20,
-6,
-18,
-7,
-9,
-8,
-18,
-12,
-12,
-15,
-7,
-14,
-14,
-19,
-7,
-7,
-6,
-11,
-7,
-19,
-11,
-16,
-8,
-13,
-12,
-5,
-15,
-8,
-11,
-16]).

coords([[45.479,47.35],
[14.637,2.649],
[31.784,39.682],
[22.594,49.458],
[1.279,7.844],
[49.591,5.129],
[14.9,8.658],
[21.227,46.868],
[0.401,2.882],
[0.992,42.377],
[8.671,23.026],
[32.845,31.582],
[1.221,19.986],
[30.561,28.302],
[36.089,46.41],
[38.427,21.803],
[31.439,5.571],
[47.912,10.565],
[30.745,9.007],
[45.764,8.559],
[1.486,35.445],
[4.525,31.446],
[7.55,44.519],
[2.7,35.55],
[44.995,10.117],
[46.221,16.131],
[23.69,14.918],
[17.508,19.095],
[36.781,37.726],
[2.119,0.96],
[31.146,45.793],
[22.081,43.214],
[5.049,22.902],
[12.182,49.069],
[46.369,46.347],
[5.293,33.895],
[39.612,41.714],
[37.15,7.767],
[20.41,42.96],
[39.731,3.207],
[13.064,46.942],
[11.596,17.549],
[35.094,10.91],
[15.455,49.859],
[9.693,8.836],
[12.492,13.799],
[24.334,41.503],
[13.111,48.089],
[24.422,33.443],
[25.272,28.466],
[42.778,11.453],
[39.423,13.02],
[32.147,17.898],
[10.796,9.047],
[30.328,42.175],
[47.221,23.289],
[40.113,10.096],
[1.045,18.115],
[49.688,11.707],
[4.499,3.554],
[48.159,31.667],
[25.973,5.136],
[43.619,12.027],
[14.434,22.33],
[21.136,12.236],
[20.912,47.276],
[26.269,36.998],
[48.913,24.144],
[47.721,49.908],
[22.17,47.329],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0],
[25.0,25.0]]).

vehicles([20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20,
20]).

service_times([0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0]).

time_windows([[200,320],
[40,160],
[448,568],
[13,133],
[559,679],
[207,327],
[114,234],
[364,484],
[29,149],
[112,232],
[235,355],
[409,529],
[146,266],
[434,554],
[107,227],
[367,487],
[559,679],
[402,522],
[386,506],
[480,600],
[464,584],
[138,258],
[283,403],
[326,446],
[211,331],
[289,409],
[212,332],
[435,555],
[532,652],
[550,670],
[203,323],
[347,467],
[385,505],
[284,404],
[242,362],
[242,362],
[86,206],
[480,600],
[19,139],
[597,717],
[262,382],
[123,243],
[402,522],
[78,198],
[146,266],
[244,364],
[422,542],
[176,296],
[442,562],
[127,247],
[378,498],
[569,689],
[419,539],
[405,525],
[516,636],
[511,631],
[179,299],
[310,430],
[378,498],
[252,372],
[304,424],
[222,342],
[462,582],
[559,679],
[572,692],
[213,333],
[354,474],
[428,548],
[319,439],
[266,386],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000],
[0,1000]]).

pairs([[1,36],
[2,37],
[3,38],
[4,39],
[5,40],
[6,41],
[7,42],
[8,43],
[9,44],
[10,45],
[11,46],
[12,47],
[13,48],
[14,49],
[15,50],
[16,51],
[17,52],
[18,53],
[19,54],
[20,55],
[21,56],
[22,57],
[23,58],
[24,59],
[25,60],
[26,61],
[27,62],
[28,63],
[29,64],
[30,65],
[31,66],
[32,67],
[33,68],
[34,69],
[35,70]]).